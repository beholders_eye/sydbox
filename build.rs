//
// syd: application sandbox
// build.rs: Helper file for build-time information
//
// Copyright (c) 2021 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{path::Path, process::Command};

fn main() {
    // We don't want to build libgit2 library just to get the git version.
    let root = Path::new(env!("CARGO_MANIFEST_DIR"));
    let root = root.join(".git");
    let mut head = String::new();

    if root.exists() {
        // Try to get the description
        if let Ok(output) = Command::new("git").arg("describe").output() {
            head = String::from_utf8_lossy(&output.stdout).trim().to_string();
        }

        // If description is empty, try to get the short HEAD
        if head.is_empty() {
            if let Ok(output) = Command::new("git")
                .args(["rev-parse", "--short", "HEAD"])
                .output()
            {
                head = String::from_utf8_lossy(&output.stdout).trim().to_string();
            }
        }

        // Check for any changes
        if let Ok(output) = Command::new("git")
            .args(["diff-index", "-m", "--name-only", "HEAD"])
            .output()
        {
            let changes = String::from_utf8_lossy(&output.stdout);
            if !changes.is_empty() {
                head = format!("{}-dirty", head);
            }
        }
    }
    println!("cargo:rustc-env=SYD_GITHEAD={}", head);
}
