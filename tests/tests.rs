//
// syd: application sandbox
// tests/tests.rs: Integration tests
//
// Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
// Copyright (c) 2023 Johannes Nixdorf <mixi@exherbo.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{env, process::Command};

const SYD_EXE: &'static str = env!("CARGO_BIN_EXE_syd");
const SYD_TEST_EXE: &'static str = env!("CARGO_BIN_EXE_syd-test");
const SYD_TEST_DO_EXE: &'static str = env!("CARGO_BIN_EXE_syd-test-do");

#[test]
fn syd_test() {
    env::set_var("CARGO_BIN_EXE_syd", SYD_EXE);
    env::set_var("CARGO_BIN_EXE_syd-test-do", SYD_TEST_DO_EXE);
    let status = Command::new(SYD_TEST_EXE)
        .status()
        .expect("execute syd-test");
    assert!(
        status.success(),
        "{} tests failed!",
        status.code().unwrap_or(-1)
    );
}
