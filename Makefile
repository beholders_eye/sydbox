# syd's Makefile
# Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
# SPDX-License-Identifier: GPL-3.0-or-later

# User variables
# Target, e.g: --target=aarch64-unknown-linux-musl
TARGET=

# Common tools
RM= rm
INSTALL= install
PREFIX= /usr/local
BINDIR= bin
CARGO= env RUST_BACKTRACE=1 cargo
PKG_CONFIG= pkg-config

# Environment variables necessary to link libseccomp statically.
export LIBSECCOMP_LINK_TYPE= static
export LIBSECCOMP_LIB_PATH= $(shell $(PKG_CONFIG) --variable=libdir libseccomp || echo /usr/lib)

# Rust flags
RUSTFLAGS_STATIC= -Clink-arg=-static -Ctarget-feature=+crt-static
RUSTFLAGS_NATIVE= $(RUSTFLAGS_STATIC) -Ctarget-cpu=native
RUSTFLAGS_RELEASE= $(RUSTFLAGS_STATIC)

# Cargo flags
CARGOFLAGS= -j$(shell nproc) -v

all: build

build:
	$(CARGO) +nightly fmt
	$(CARGO) build $(CARGOFLAGS)
	$(CARGO) test
	$(CARGO) +nightly clippy $(CARGOFLAGS)
	$(CARGO) deny check
clean:
	$(CARGO) clean
install: native
	$(INSTALL) -d $(DESTDIR)$(PREFIX)/$(BINDIR)/
	$(INSTALL) -pm 0755 target/release/syd $(DESTDIR)$(PREFIX)/$(BINDIR)/
	$(INSTALL) -pm 0755 target/release/syd-test $(DESTDIR)$(PREFIX)/$(BINDIR)/
	$(INSTALL) -pm 0755 target/release/syd-test-do $(DESTDIR)$(PREFIX)/$(BINDIR)/
uninstall:
	$(RM) -f \
		$(DESTDIR)$(PREFIX)/$(BINDIR)/syd
native:
	export RUSTFLAGS="$(RUSTFLAGS_NATIVE)"
	$(CARGO) build --release $(CARGOFLAGS)
release:
	export RUSTFLAGS="$(RUSTFLAGS_RELEASE)"
	@echo Using libseccomp library from $(LIBSECCOMP_LIB_PATH)
	$(CARGO) build --release $(CARGOFLAGS) $(TARGET)
	$(CARGO) test --release $(CARGOFLAGS)
test:
	$(CARGO) test

# Use LLVM sanitizers
sanitize_address:
	env RUSTFLAGS="-Zsanitizer=address" $(CARGO) +nightly build $(CARGOFLAGS)
sanitize_leak:
	env RUSTFLAGS="-Zsanitizer=leak" $(CARGO) +nightly build $(CARGOFLAGS)
sanitize_memory:
	env RUSTFLAGS="-Zsanitizer=memory" $(CARGO) +nightly build $(CARGOFLAGS)
sanitize_thread:
	env RUSTFLAGS="-Zsanitizer=thread" $(CARGO) +nightly build $(CARGOFLAGS)

bloat:
	$(CARGO) bloat --crates -n 100 --bin syd --profile release
cov:
	$(CARGO) llvm-cov --open
deny:
	$(CARGO) deny check
msrv:
	$(CARGO) msrv --bisect
watch:
	$(CARGO) watch
who:
	@git log --all --format='%cN <%cE>' | sort -u

.PHONY: all bloat build clean cov deny msrv native release install uninstall test watch who
.PHONY: sanitize_address sanitize_leak sanitize_memory sanitize_thread
