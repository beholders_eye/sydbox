[![](https://git.sr.ht/~alip/syd/tree/main/item/data/sydbox160.png)](https://git.sr.ht/~alip/syd)

# sydbox: The ☮ther SⒶndbøx

[![Shine On You Crazy Diamond!](https://img.shields.io/badge/Shine%20On%20You%20Crazy%20Diamond!-8A2BE2)](https://en.wikipedia.org/wiki/Syd_Barrett)
[![license](https://img.shields.io/crates/l/jja.svg)](https://git.sr.ht/~alip/syd/tree/main/item/COPYING)
[![msrv](https://img.shields.io/badge/rustc-1.70%2B-green?style=plastic)](https://blog.rust-lang.org/2023/06/01/Rust-1.70.0.html)
[![build status](https://builds.sr.ht/~alip/syd.svg)](https://builds.sr.ht/~alip/syd?)
[![maintenance-status](https://img.shields.io/badge/maintenance-actively--developed-brightgreen.svg)](https://git.sr.ht/~alip/syd)
[![dependency status](https://deps.rs/repo/sourcehut/~alip/syd/status.svg)](https://deps.rs/repo/sourcehut/~alip/syd)
[![repology](https://repology.org/badge/latest-versions/sydbox.svg)](https://repology.org/project/sydbox/versions)

[![SydB☮x](https://git.sr.ht/~alip/syd/blob/main/data/syd.png)](https://todo.sr.ht/~alip/syd)
[![GNU](https://web.archive.org/web/20221222061733if_/https://dev.exherbo.org/~alip/images/gnu.png)](https://www.kernel.org/category/about.html)
[![Linux](https://chesswob.org/jja/tux.png)](https://www.gnu.org/philosophy/philosophy.html)
[![Exherbo](https://web.archive.org/web/20230518155203if_/https://dev.exherbo.org/~alip/images/zebrapig.png)](https://www.exherbolinux.org/docs/gettingstarted.html)
[![musl libc](https://www.chesswob.org/jja/musl-inside.png)](https://www.musl-libc.org/)
[![libseccomp](https://web.archive.org/web/20221222061720if_/https://dev.exherbo.org/~alip/images/libseccomp.png)](https://github.com/seccomp/libseccomp)
[![Paludis](http://paludis.exherbolinux.org/paludis_270.png)](https://paludis.exherbolinux.org)

# Introduction

 SydB☮x has been the default sandbox of [`Exherbo`
`GNU/Linux`](https://exherbolinux.org) distribution for over a decade. We use it
to provide a restricted environment under which package builds run with
controlled access to file system and network resources.
[`Exherbo`](https://exherbolinux.org) package description format, currently
[`exheres-0`](https://exherbolinux.org/docs/eapi/exheres-for-smarties.html),
uses a shell function called `esandbox` to interact with `sydbox`.  See the
[Sandboxing section of Exheres for
Smarties](https://exherbolinux.org/docs/eapi/exheres-for-smarties.html#sandboxing)
for more information.

## Quick Start

- [`sydbox-0`](https://git.sr.ht/~alip/syd/tree/sydbox-0) is a `ptrace` based
  sandbox.
- [`sydbox-1`](https://git.sr.ht/~alip/syd/tree/sydbox-1) is a `ptrace+seccomp`
  based sandbox.
- [`sydbox-2`](https://git.sr.ht/~alip/syd/tree/sydbox-1) is a
  `seccomp+seccomp-unotify` based sandbox.
- `sydbox-3` is a rewrite of `sydbox-2` in Rust and it's what you are looking
  at.

This codebase has a history of a bit over 10 years and up to this point we have
used [`C11`](https://en.wikipedia.org/wiki/C11_(C_standard_revision)) as our
implementation language for various reasons. With `sydbox-3` we are moving
forwards one step and writing the sandbox from scratch using the `Rust` programming
language with the only `!Rust` dependency being `libseccomp`.  Although we
inherit many ideas and design decisions from the old codebase, we also don't shy
away from radically changing the internal implementation making it much simpler,
idiomatic, and less prone to bugs. This version also **takes advantage of
multithreading and handles system calls using a thread pool whose size is equal
to the number of CPUs on the running machine**. Moreover, **access violations are logged
into [syslog](https://en.wikipedia.org/wiki/Syslog)**, so you may use a command
like `journalctl SYSLOG_IDENTIFIER=syd` (or shortly `syd log`) to view the
sandbox logs. This version also comes with a new sandboxing type called [Stat
Sandboxing](#stat-sandboxing) which can be used to **hide files and directories** from the
sandboxed process. We use [clippy](https://github.com/rust-lang/rust-clippy) for
static analysis with a strict configuration. In the name of code simplicity, we
set the value `9` rather than the default `25` for the [cylomatic, "cognitive",
complexity](https://rust-lang.github.io/rust-clippy/master/index.html#/cognitive_complexity)
lint, and the value `80` rather than the default `250` for the [type
complexity](https://rust-lang.github.io/rust-clippy/master/index.html#type_complexity)
lint. Moreover, in the name of better error handling, we disallow the usage of
[Option::unwrap](https://doc.rust-lang.org/std/option/enum.Option.html#method.unwrap),
[Option::expect](https://doc.rust-lang.org/std/option/enum.Option.html#method.expect),
[Result::unwrap](https://doc.rust-lang.org/std/result/enum.Result.html#method.unwrap), and
[Result::expect](https://doc.rust-lang.org/std/result/enum.Result.html#method.expect).
The program may easily be built statically, with a size about `1.1Mb` for the
[x86-64](https://builds.sr.ht/~alip/syd/commits/main/linux-x86-64.yml) build
(gets down to ~350Kb after [UPX](https://upx.github.io/) compression), and
`1.0Mb` for the
[aarch64](https://builds.sr.ht/~alip/syd/commits/main/linux-arm64.yml) build.
After each push, the CI builds binaries with static linking using a
[musl](https://www.musl-libc.org/) target and keeps them available for download
for 90 days. To download, browse to one of the URLs given below depending on
your machine architecture, choose a succeeding build, and download the
artifact `syd` on the left part of the build page. You may also download the
`syd-debug` artifact for a non-stripped release build.

- **x86-64**
  - Host: `Linux build 6.1.55-0-lts #1-Alpine SMP PREEMPT_DYNAMIC Sun, 24 Sep 2023 23:14:02 +0000 x86_64 Linux`
  - List: https://builds.sr.ht/~alip/syd/commits/main/linux-x86-64.yml
- **aarch64**
  - Host: `Linux build 5.10.0-8-arm64 #1 SMP Debian 5.10.46-4 (2021-08-03) aarch64 GNU/Linux`
  - List: https://builds.sr.ht/~alip/syd/commits/main/linux-arm64.yml

To install from source, use `cargo install --locked syd`. To follow the latest
developments, checkout the git repository at <https://git.sr.ht/~alip/syd> and
run `cargo build --release` on it. Make sure to have
[libseccomp](https://github.com/seccomp/libseccomp) development files installed.
The command to install [libseccomp](https://github.com/seccomp/libseccomp)
library for some Linux distributions are given below.

- **Alpine**: `apk add libseccomp-dev libseccomp-static`
- **Debian**: `apt install libseccomp-dev`
- **Gentoo**: `emerge libseccomp`
- **Ubuntu**: `apt install libseccomp-dev`

Note, on [**Exherbo**](https://exherbolinux.org) SydB☮x is [already
packaged](https://gitlab.exherbo.org/exherbo/arbor/-/tree/master/packages/sys-apps/sydbox)
but it is masked with the **testing** keyword as the
[Paludis](https://paludis.exherbolinux.org) support for [**SydB☮x API 3** is not
merged yet](https://gitlab.exherbo.org/paludis/paludis/-/merge_requests/86). To
use SydB☮x on [**Exherbo**](https://exherbolinux.org) unmask `sys-apps/sydbox
testing`, run `cave resolve sydbox` as usual and follow the instructions in the
post install message.

SydB☮x exposes the functionality of the following crates in their sandbox API:

1. [glob-match](https://docs.rs/glob-match): Pattern matching with Unix shell
   style patterns, see
   [Wikipedia:Glob](https://en.wikipedia.org/wiki/Glob_(programming)) for more information.
2. [ipnetwork](https://docs.rs/ipnetwork): Pattern matching with Classless
   Inter-Domain Routing, see [Wikipedia:Cidr](https://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing)
   for more information.

This manual page gives only brief information on [Pattern
Matching](#pattern-matching), and [Address Matching](#address-matching). The
user is recommended to read the documentation of the respective crates to get
more information on the details of [pattern matching](#pattern-matching), and
[address matching](#address-matching).

# Manual

## NAME

syd - seccomp-bpf and seccomp-notify based application sandbox

## SYNOPSIS

- **syd \[-hv\] \[\--lock\] \[\-\-root] \[\-m *command*\...\] \[\-c *path*\...\]**
**\[\-p *name*\...\] \[\-\-\] {command *\[arg\...\]*}**
- **syd -e, --export bpf|pfc**
- **syd -t, --test**
- **syd exec {command *\[arg\...\]*}**
- **syd log**

## DESCRIPTION

SydB☮x is a **seccomp**(2) based sandboxing utility for modern Linux\[\>=5.6\]
machines to sandbox unwanted process access to filesystem and network resources.
SydB☮x requires *no root access* and *no ptrace* rights. All you need is a
recent Linux kernel and libseccomp which is available on many different
architectures, including **x86**, **x86\_64**, **x32**, **arm**, **aarch64**,
**mips**, **mips64**... This makes it very easy for a regular user to use. This is
the motto of SydB☮x: *bring easy, simple, flexible and powerful access restriction
to the Linux user!*

The basic idea of SydB☮x is to run a command under certain restrictions. These
restrictions define which system calls the command is permitted to run and which
argument values are permitted for the given system call. The restrictions may be
applied via two ways.  *seccomp-bpf* can be used to apply simple Secure Computing
user filters to run sandboxing fully on kernel space, and *seccomp-notify*
functionality can be used to run sandboxing on kernel space and fallback to user
space to dereference pointer arguments of system calls (**See
[Security](#security) about `TOCTOU` et. al**), which are one of
**[pathname](https://en.wikipedia.org/wiki/Path_(computing))**, **[UNIX socket
address](https://en.wikipedia.org/wiki/Unix_domain_socket)**,
**[IPv4](https://en.wikipedia.org/wiki/IPv4)** or
**[IPv6](https://en.wikipedia.org/wiki/IPv6)** network address, and make dynamic
decisions using [Unix shell style patterns](https://docs.rs/glob-match) such as
`allowlist/write+/home/sydbox/***`, or `allowlist/write+/run/user/*/pulse` for
**[pathnames](https://en.wikipedia.org/wiki/Path_(computing))**, and using
**[CIDR](https://docs.rs/ipnetwork)** notation such as
`allowlist/net/connect+127.0.0.1/8@9050`, or
`allowlist/net/connect+::1/8@9050` for
**[IPv4](https://en.wikipedia.org/wiki/IPv4)** and
**[IPv6](https://en.wikipedia.org/wiki/IPv6)** addresses and perform an action
which is by default denying the system call with an appropriate error, which is
usually **permission denied**, aka `EPERM`. For default disallowed system calls,
such as `ptrace` or `process_vm_writev` (**See [Security](#security) about
`TOCTOU` et. al**) SydB☮x returns `EACCES` and for invalid architectures SydB☮x
returns `ENOSYS`.

To be able to use SydB☮x, you need a recent Linux kernel with the system calls
**pidfd_getfd**, **pidfd_send_signal**. The Secure Computing facility of the
Linux kernel should support the **SECCOMP_USER_NOTIF_FLAG_CONTINUE** operation.
It is recommended to have the **CONFIG_CROSS_MEMORY_ATTACH** kernel option
enabled, if this option is not enabled, sydbox will fallback to reading/writing
from `/proc/$pid/mem`. Linux-5.11 or later is recommended.

## OPTIONS

The following options are understood:

**-h**, **\--help**

> Show usage and exit

**-v**, **\--version**

> Show version and exit

**-m** *command*, **\--magic**=*command*

> Run a sandbox command during init, may be repeated. See the section
> called [CONFIGURATION](#configuration) for more information.

**-c** *path*, **\--config**=*path*

> Run a configuration file during init, may be repeated. See the section
> called [CONFIGURATION](#configuration) for more information.

**-p** *name*, **\--profile**=*name*

> Use a sandbox profile during init, may be repeated. See the section
> called [CONFIGURATION](#configuration) for more information.

**-l**, **\--lock**

> Lock sandbox commands after initialization. By default, sydbox may be
> configured during runtime from inside the sandbox via Paludis esandbox
> command or via **pandora**. Locking prevents this and makes sydbox
> sandbox tamper-free making it a true jail replacement.
> (**See [Security](#security) about `TOCTOU` et. al**)

**-r**, **\--root**

> In **fakeroot** mode, the system will return a user/group id of `0`, mimicking
> the root user. This allows users to execute commands with apparent root
> privileges, without actual superuser rights. It's useful for tasks like package
> building where root-like environment is needed, but not actual root
> permissions. You may also use the environment variable `SYD_FAKEROOT` to this
> effect.

**-e** *mode*, **\--export**=*mode*

> Export secure computing rules with the given format to standard output and
> exit. Mode must be one of **bpf** or **pfc**. **bpf**, aka **Berkeley Packet
> Filter** is a binary, machine readable format whereas **pfc**, aka
> **Pseudo Filter Code** is a textual, human readable format.

**-t**, **\--test**

> Run integration tests and exit.
> Requires `syd-test` and `syd-test-do` programs to be in `PATH`.

- **syd exec** may be used to construct a sandbox command to execute a process
  outside the sandbox. See the description of [**cmd/exec**](#cmdexec) command
  for more information.
- **syd log** may be used to access sandbox logs using
  [`journalctl`](https://www.man7.org/linux/man-pages/man1/journalctl.1.html).

### Profiles
1. **paludis**: Used by the [Paludis](http://paludis.exherbolinux.org/) package mangler.
2. **noipv4**: Disables
   [IPv4](https://en.wikipedia.org/wiki/Internet_Protocol_version_4) connectivity.
3. **noipv6**: Disables [IPv6](https://en.wikipedia.org/wiki/IPv6) connectivity.
4. **user**: Allows user-specific directories, and connections, and
   parses the file ~/.user.syd-3 if it exists.

When invoked without arguments, the current shell is executed under sandbox with
the **user** profile.

### Environment Variables

- **SYD\_FAKEROOT**: Setting this has the same effect as `--root`.
- **SYD\_LOG**: Set log level. See the ["Enabling
  Logging"](https://docs.rs/env_logger/latest/env_logger/#enabling-logging)
  section of **env-logger** crate documentation for more information.
- **SYD\_NO\_SYSLOG**: Disable logging to **syslog**. By default logs of
  severity `Warn` and higher are logged to **syslog**.
- **SYD\_NO\_CROSS\_MEMORY\_ATTACH**: Disable cross memory attach and fallback
  to `/proc/pid/mem`.

### Exit Codes

Sydb☮x exits with the same exit code as the sandbox process itself. If the
sandbox process exits with a signal, Sydb☮x exits with 14 which stands for
**EFAULT**. In case there was an error in spawning or waiting for the sandbox
process, Sydb☮x exits with **errno** indicating the error condition. E.g. `syd
true` returns 0, `syd false` return 1, and `syd -- syd true` returns 16 which
stands for **EBUSY** which stands for "Device or resource busy" indicating there
is already a secure computing filter loaded. tl;dr Sydb☮x won't run under
Sydb☮x, similarly many process inspection tools such as `ltrace`, `strace`, or
`gdb` won't work under Sydb☮x. Thus the sandbox process can either be traced by
attaching from outside the sandbox or running the tracer in follow fork mode,
e.g. `strace -f syd true`.

## SANDBOXING

There are five sandboxing types:

1. [Read sandboxing](#read-sandboxing)
2. [Stat sandboxing](#stat-sandboxing)
3. [Write sandboxing](#write-sandboxing)
4. [Exec sandboxing](#exec-sandboxing)
5. [Network sandboxing](#network-sandboxing)

Sandboxing may be on and off.

- **off**: Sandboxing is off, none of the relevant system calls are checked and
  all access is allowed.
- **on**: Sandboxing defaults to deny, allowlists and denylists can be used to
  refine access rights.

In addition, there are filters for every sandboxing to prevent Sydb☮x
from reporting an access violation. Note, access is still denied in such
cases.

### Read Sandboxing

This sandboxing checks certain system calls for filesystem read access.
If a system call tries to read a path, this attempt is reported and the
system call is denied. See the section called [Write Sandboxing](#write-sandboxing) for
more information on how to customize this behaviour.

List of filtered system calls are: **access**, **faccessat**, **faccessat2**,
**open**, **openat**, **openat2**, **listxattr**, **flistxattr**, and
**llistxattr**.

### Stat Sandboxing

This sandboxing checks certain system calls for filesystem statistics access.
This can be one of listing a directory, changing into a directory, or using a
**stat** system call to query file metadata. This sandboxing type may be used to
effectively **hide files and directories** from the sandbox process.

List of filtered system calls are: **chdir**, **fchdir**, **getdents**,
**getdents64**, **stat**, **fstat**, **lstat**, **statx**, **newfstatat**.

### Write Sandboxing

This sandboxing checks certain system calls for filesystem write access. If a system
call tries to write, modify or change attributes of a path, this attempt is reported
in system log and the system call is denied. There are two ways to customize this
behaviour. Sydb☮x may be configured to "allowlist" some path patterns. If the path
argument of the system call which is subject to be modified matches a pattern in the
list of allowlisted path patterns, this attempt is not denied. If, however it
matches a pattern in the list of "denylist" path patterns the attempt is denied
(**last matching pattern wins**).  Additionally, Sydb☮x may be configured to
"filter" some path patterns. In this case a match will prevent Sydb☮x from reporting
a warning about the access violation, the system call is still denied though.

List of filtered system calls are: **access**, **faccessat**,
**faccessat2**, **chmod**, **fchmodat**, **chown**, **chown32**,
**lchown**, **lchown32**, **fchownat**, **open**, **openat**,
**openat2**, **creat**, **mkdir**, **mkdirat**, **mknod**,
**mknodat**, **rmdir**, **truncate**, **truncate64**, **mount**,
**umount**, **umount2**, **utime**, **utimes**, **utimensat**,
**futimesat**, **unlink**, **unlinkat**, **link**, **linkat**,
**rename**, **renameat**, **renameat2**, **symlink**, **symlinkat**,
**setxattr**, **fsetxattr**, **lsetxattr**, **removexattr**,
**fremovexattr** and **lremovexattr**.

### Exec Sandboxing

This sandboxing denies **execve**, and **execveat** calls in case
the path argument does not match one of the allowlisted patterns. Note,
all **exec** family functions are sandboxed because these functions
are just wrappers of either one of **execve** or **execveat**
system calls. See the section called [Write Sandboxing](#write-sandboxing) for
more information on how to customize this behaviour.

### Network Sandboxing

This sandboxing exposes a way to prevent unwanted network calls. The
filtered system calls are: **bind**, **connect**, **sendto**,
**recvmsg**, and **sendmsg**. To increase usability, these system
calls are filtered in two groups: *bind* and *connect*. **bind**
belongs to the first group, whereas the other system calls belong to the
*connect* group. See the section called [Write Sandboxing](#write-sandboxing) for
more information on how to customize this behaviour.

## Further Restrictions

There are other ways to further restrict access which are listed below.

- `exec/kill`: Kill the exec process in case it matches a path pattern.

## CONFIGURATION

Sydb☮x is configured through sandbox commands. There are two ways to supply
sandbox commands:

1. Sydb☮x may be configured using a configuration file. The path to the
   configuration file is speficied using the **-c** command line switch. More
   than one configuration file may be specified this way. Single commands may
   also be passed via *-m* command line switch. Some default configuration sets
   may be applied using the *-p* command line switch. The available sets are
   `paludis`, `noipv4`, `noipv6`, and `user`. See [Profiles](#profiles) for more
   information. More than one profile may be specified.  For multiple matching
   rules (e.g. two rules matching the same path), **the last matching rule
   wins**.
2. Sydb☮x may be configured using "magic" **stat** calls during runtime.  This
   is achieved by calling **stat()** system call on the special path `/dev/syd`
   followed by the sandbox command. Note that runtime configuration is only
   possible if the sandbox lock is *unset*. The system call **stat()** was
   chosen because it is practical to invoke using builtin shell commands like:

`test -e /dev/syd/sandbox/read:on`

which enables read sandboxing for a shell running under Sydb☮x. It is
also possible to query certain values using the return value of the
**stat** call:

```
test -e /dev/syd/sandbox/read? &&\
  echo "read sandboxing on" ||\
  echo "read sandboxing off"
```


Note, some of these shell builtins may actually call other system calls such as
**fstat**, **lstat**, **newfstatat**, or **statx**. Sydb☮x supports the same
interface through all these system calls transparently.

### Command Types

Every sandbox command accepts an argument of a certain type. The available types
are listed below:

- **void**: This command accepts no argument.
- **boolean**: A boolean type may have one of the two values, *true* or *false*.
In addition you can use the short forms *t* or *f* and you can also use *1* or *0*.
- **integer**: This type represents the basic integer type.
- **string**: This type represents the basic string type.
- **string-array**: This type represents a list of strings. Other types arent
  allowed within this type.
- **command**: This is a special type which is used to make sydbox execute certain
functions. It is meant to be used as a basic interprocess communication to
workaround some tracing limitations.

### Specifying Sandbox Commands

As mentioned in the section called [CONFIGURATION](#configuration), Sydb☮x may
be configured using sandbox commands. Format of the sandbox commands is simple:
`${PREFIX}/section/of/option${OPERATION_CHARACTER}value` where *`${PREFIX}`* is
/dev/syd by default (may be altered at compile-time using *MAGIC\_PREFIX*
definition in `config.rs`). This prefix is only required for **stat()** call,
not for **-m** command line switch.

*`${OPERATION_CHARACTER}`* determines the operation of the sandbox
command. Possible values are listed below:

- :
This term is used to set a value. Value must be either a boolean, an
integer or a string.
- ?
This term is used to query a value. Boolean values and certain other
values may be queried.
- \+
This term is used to append to a string array.
- \-
This is used to remove an element from a string array.
- !
This is used to execute a special sydbox command.

### Configuration File Format

Configuration file format of sydbox is simple. It is just a way to supply many
commands in a convenient way. All empty lines and lines starting with the
sign **\#** are ignored. All the other lines are treated as if they were supplied to
Sydb☮x via the **-m** command line switch.

### Configuration File Naming

Configuration file naming of sydbox follows a naming scheme which makes it possible
to extract command API version from the file name. A sydbox configuration file
must have the extension "syd-" followed by the API version (e.g. "syd-3" for API
version 3).

Current command API of sydbox version is `3`.

### Commands

Sydb☮x recognizes the following commands:

#### dump

- type: **void**
- default: *none*
- query: *no*

This command causes Sydb☮x to output sandbox information on standard error.

#### lock

- type: **string**
- default: *exec*
- query: *no*

A string specifying the state of the sandbox lock. Possible values are *on*, *off*
and *exec*. If the sandbox lock is *on* no sandbox commands are allowed. If *exec* is
specified, the sandbox lock is set to *on* for all processes except the initial
process, aka Sydb☮x exec child.

#### sandbox/exec

- type: **string**
- default: *off*
- query: *yes*

A string specifying how **exec** calls should be sandboxed.
See the section called [Exec Sandboxing](#exec-sandboxing) for more information.

#### sandbox/read

- type: **string**
- default: *off*
- query: *yes*

A string specifying how read sandboxing should be done. See the
section called [Read Sandboxing](#read-sandboxing) for more information.

#### sandbox/stat

- type: **string**
- default: *off*
- query: *yes*

A string specifying how stat sandboxing should be done. See the
section called [Stat Sandboxing](#stat-sandboxing) for more information.

#### sandbox/write

- type: **string**
- default: *on*
- query: *yes*

A string specifying how write sandboxing should be done. See the
section called [Write Sandboxing](#write-sandboxing) for more information.

#### sandbox/net

- type: **string**
- default: *on*
- query: *yes*

A string specifying how network sandboxing should be done. See the
section called [Network Sandboxing](#network-sandboxing) for more information.

#### trace/allow_successful_bind

- type: **boolean**
- default: *true*
- query: *no*

A boolean specifying whether the socket address arguments of successful **bind**
calls should be allowlisted for **connect**, **sendto**, **recvmsg**, and
**sendmsg** system calls. **Note**, these socket addresses are allowlisted
globally and not per-process for usability reasons. Thus, for example, a process
which forks to call **bind** will have its socket address allowlisted for their
parent as well.

#### trace/allow_unsupported_socket_families

- type: **boolean**
- default: *true*
- query: *no*

A boolean specifying whether unknown socket families should be allowed
access when network sandboxing is on.

#### trace/memory\_access

- type: **integer**
- default: *0*
- query: *no*

Mode on using cross memory attach or **/proc/pid/mem**. Cross memory
attach requires a Linux kernel with the
**CONFIG\_CROSS\_MEMORY\_ATTACH** option enabled. Default mode is
**0**.

- 0: Use cross memory attach if available, use /proc otherwise.
- 1: Use `/proc/pid/mem` unconditionally. You may also use the environment
  variable `SYD_NO_CROSS_MEMORY_ATTACH` to this effect:

```
⇒  strace -q -eprocess_vm_readv -fc -- syd -m trace/memory_access:1 true; echo $?
[pid 1100565] ????( <detached ...>
0
⇒  strace -q -eprocess_vm_readv -fc -- syd -m trace/memory_access:0 true; echo $?
% time     seconds  usecs/call     calls    errors syscall
------ ----------- ----------- --------- --------- -----------------
100,00    0,000155           6        24           process_vm_readv
------ ----------- ----------- --------- --------- -----------------
100,00    0,000155           6        24           total
0
⇒
```

#### exec/kill

- type: **string-array**
- default: [empty array]
- query: *no*

This setting specifies a list of path patterns. If one of these
patterns matches the resolved path of an **exec** system call,
the process in question is killed. See the section called [PATTERN
MATCHING](#pattern-matching) for more information on glob patterns.

#### filter/exec

- type: **string-array**
- default: [empty array]
- query: *no*

Specifies a list of path patterns to filter for **exec** sandboxing. See the
section called [Exec Sandboxing](#exec-sandboxing) and the section called
[PATTERN MATCHING](#pattern-matching).

#### filter/read

- type: **string-array**
- default: [empty array]
- query: *no*

Specifies a list of path patterns to filter for **read** sandboxing. See the
section called [Read Sandboxing](#read-sandboxing) and the section called
[PATTERN MATCHING](#pattern-matching).

#### filter/stat

- type: **string-array**
- default: [empty array]
- query: *no*

Specifies a list of path patterns to filter for **stat** sandboxing. See the
section called [Stat Sandboxing](#stat-sandboxing) and the section called
[PATTERN MATCHING](#pattern-matching).

#### filter/write

- type: **string-array**
- default: [empty array]
- query: *no*

Specifies a list of path patterns to filter for **write** sandboxing. See
the section called [Write Sandboxing](#write-sandboxing) and the section called [PATTERN
MATCHING](#pattern-matching).

#### filter/net

- type: **string-array**
- default: [empty array]
- query: *no*

Specifies a list of network addresses to filter for **network**
sandboxing. See the section called [Network Sandboxing](#network-sandboxing) and the
section called [ADDRESS MATCHING](#address-matching).

#### allowlist/exec

- type: **string-array**
- default: [empty array]
- query: *no*

Specifies a list of path patterns to allowlist for **exec** sandboxing. See the
section called [Exec Sandboxing](#exec-sandboxing) and the section called
[PATTERN MATCHING](#pattern-matching).

#### allowlist/read

- type: **string-array**
- default: [empty array]
- query: *no*

Specifies a list of path patterns to allowlist for **read** sandboxing.
See the section called [Read Sandboxing](#read-sandboxing) and the section called
[PATTERN MATCHING](#pattern-matching).

#### allowlist/stat

- type: **string-array**
- default: [empty array]
- query: *no*

Specifies a list of path patterns to allowlist for **stat** sandboxing.
See the section called [Stat Sandboxing](#stat-sandboxing) and the section called
[PATTERN MATCHING](#pattern-matching).

#### allowlist/write

- type: **string-array**
- default: [empty array]
- query: *no*

Specifies a list of path patterns to allowlist for **write** sandboxing.
See the section called [Write Sandboxing](#write-sandboxing) and the section called
[PATTERN MATCHING](#pattern-matching).

#### allowlist/net/bind

- type: **string-array**
- default: [empty array]
- query: *no*

Specifies a list of network addresses to allowlist for **bind network** sandboxing.
See the section called [Network Sandboxing](#network-sandboxing) and the section called "ADDRESS
MATCHING".

#### allowlist/net/connect

- type: **string-array**
- default: [empty array]
- query: *no*

Specifies a list of network addresses to allowlist for **connect network** sandboxing.
See the section called [Network Sandboxing](#network-sandboxing) and the section called "ADDRESS
MATCHING".

#### denylist/exec

- type: **string-array**
- default: [empty array]
- query: *no*

Specifies a list of path patterns to denylist for **exec** sandboxing. See the section
called [Exec Sandboxing](#exec-sandboxing) and the section called [PATTERN MATCHING](#pattern-matching).

#### denylist/read

- type: **string-array**
- default: [empty array]
- query: *no*

Specifies a list of path patterns to denylist for **read** sandboxing. See
the section called [Read Sandboxing](#read-sandboxing) and the section called [PATTERN
MATCHING](#pattern-matching).

#### denylist/stat

- type: **string-array**
- default: [empty array]
- query: *no*

Specifies a list of path patterns to denylist for **stat** andboxing. See
the section called [Stat Sandboxing](#stat-sandboxing) and the section called [PATTERN
MATCHING](#pattern-matching).

#### denylist/write

- type: **string-array**
- default: [empty array]
- query: *no*

Specifies a list of path patterns to denylist for **write** sandboxing.
See the section called [Write Sandboxing](#write-sandboxing) and the section called
[PATTERN MATCHING](#pattern-matching).

#### denylist/net/bind

- type: **string-array**
- default: [empty array]
- query: *no*

Specifies a list of network addresses to denylist for **bind network** sandboxing. See
the section called [Network Sandboxing](#network-sandboxing) and the section called [ADDRESS MATCHING](#address-matching).

#### denylist/net/connect

- type: **string-array**
- default: [empty array]
- query: *no*

Specifies a list of network addresses to denylist for **connect network** sandboxing.
See the section called [Network Sandboxing](#network-sandboxing) and the section called [ADDRESS
MATCHING](#address-matching).

#### cmd/exec

- type: **command**
- default: none
- query: *no*

Makes SydB☮x execute an external command without sandboxing. The program name
and arguments must be separated with the **US** (unit separator, octal: 037)
character. To ease usage, the **syd exec** subcommand is provided to construct a
sandbox command of this type:

```
⇒  syd -- bash -c 'test -e $(syd exec echo hello world)'
{"caps":2,"cmd":"bash -c test -e $(target/debug/syd exec echo hello world)","ctx":"access","id":"syd","l":2,"path":"/dev/tty","pid":771840,"sys":"openat","t":1696548946}
{"caps":2,"cmd":"bash -c test -e $(target/debug/syd exec echo hello world)","ctx":"access","id":"syd","l":2,"path":"/dev/pts/8","pid":771840,"sys":"openat","t":1696548946}
{"abs":false,"caps":8,"cmd":"target/debug/syd exec echo hello world","ctx":"access","id":"syd","l":2,"pid":771842,"sys":"connect","t":1696548946,"unix":"/dev/log"}
{"cmd":"target/debug/syd exec echo hello world","ctx":"syd/exec","id":"syd","l":2,"path":"/dev/syd/cmd/exec!echo\u001fhello\u001fworld","pid":771842,"t":1696548946}
{"cmd":"target/debug/syd -- bash -c test -e $(target/debug/syd exec echo hello world)","ctx":"cmd/exec","id":"syd","l":2,"pid":771847,"t":1696548946}
hello world
```

### PATTERN MATCHING

Sydb☮x uses shell-style pattern matching for allowlists and filters. The
matching code uses the [glob-match](https://docs.rs/glob-match) crate. Check
their documentation on more information about patterns. Note, patterns are case
sensitive and Sydb☮x also applies the **triple star** extension to patterns,
i.e. `/dev/***` matches both `/dev` and any file recursively under `/dev`.
Note also, Sydb☮x gets patterns from multiple sources: a configuration file, a
profile, the *-m* command line switch, or a *stat* call with `/dev/syd` prefix.
There is no precedence between different sources, and **the last matching
pattern decides the outcome.**

### ADDRESS MATCHING

Sydb☮x has a simple address scheme to match network addresses. The addresses can
either be a [glob-match](https://docs.rs/glob-match) pattern to match UNIX and
abstract UNIX socket addresses, or an [IP CIDR](https://docs.rs/ipnetwork)
followed by a port range to match IPv4 and IPv6 addresses. Port range can either
be a single port or a range in format `port1-port2`. The address and the port
range must be splitted by the character `@`. In addition there are some aliases,
you may use instead of specifying an address:

- **LOOPBACK**: Expanded to *127.0.0.0/8*
- **LOOPBACK6**: Expanded to *::1/8*
- **LOCAL**: Expanded to four addresses as defined in RFC1918:
  - *127.0.0.0/8*
  - *10.0.0.0/8*
  - *172.16.0.0/12*
  - *192.168.0.0/16*
- **LOCAL6**: Expanded to four addresses:
  - *::1*
  - *fe80::/7*
  - *fc00::/7*
  - *fec0::/7*

## EXAMPLES

Below are examples of invocation and configuration of Sydb☮x.

### Invocation Examples

Below are some invocation examples:

Deny all reads and writes, allow read access to /dev/zero and write access to
/dev/full. The executable dd is not static in this case thus allow access to
/lib64 where it will load its shared libraries from as well. Note, on the
system of the author the `dd` binary links only to libraries under `/usr/lib`,
use `ldd` to check the linked libraries on your system. Note also the quoting
to escape shell expansion.

```
⇒  syd -m sandbox/read:on -m 'allowlist/read+/usr/lib/**' -m allowlist/read+/dev/zero -m allowlist/write+/dev/full -- dd if=/dev/zero of=/dev/full count=1
dd: writing to '/dev/full': No space left on device
1+0 records in
0+0 records out
0 bytes copied, 0,00168969 s, 0,0 kB/s
```

Kill common bittorrent applications. Note 14 stands for **EFAULT** which means
the sandbox process was killed by a signal.

```
⇒  syd -m 'exec/kill+/usr/bin/[kr]torrent' -- bash; echo $?
$ rtorrent
14
⇒  syd -m 'exec/kill+/usr/bin/[kr]torrent' -- bash; echo $?
$ ktorrent
14
⇒ 
```

Hide some files and directories.

```
⇒  syd -puser bash
$ ls /
bin boot cdrom data dev etc home lib lib32 lib64 libx32 lost+found media mnt nix opt proc root run sbin snap srv sys tmp usr var
$ test -e /dev/syd/allowlist/stat-/boot && echo ok
$ test -e /dev/syd/allowlist/stat-/tmp && echo ok
ok
$ ls /
bin cdrom data dev etc home lib lib32 lib64 libx32 lost+found media mnt nix opt proc root run sbin snap srv sys usr var
$ cd /tmp
bash: cd: /tmp: Operation not permitted
$ test -e /dev/syd/allowlist/stat+/tmp && echo ok
ok
cd /tmp
$ pwd
/tmp
$
```

## BUGS

```
Hey you, out there beyond the wall,
Breaking bottles in the hall,
Can you help me?
```

Report bugs to ___SydB☮x___'s bug tracker at <https://todo.sr.ht/~alip/syd/>:
1. Always **be polite**, respectful, and kind:
   <https://css-tricks.com/open-source-etiquette-guidebook/>
2. Keep your final change as **small and neat** as possible:
   <https://tirania.org/blog/archive/2010/Dec-31.html>
3. Attaching poems with the bug report encourages consideration tremendously.

## SECURITY

This is a tricky area. The main advantage SydB☮x brings to the table is that
it requires **no elevated privileges: no root access or `ptrace` capabilities**
are needed. This makes SydB☮x very easy to set up and use. Moreover, SydB☮x
allows the user to **configure the sandbox dynamically from within the
sandbox**, and lock it as necessary afterwards. This comes at a cost though.
SydB☮x makes use of `SECCOMP_USER_NOTIF_FLAG_CONTINUE` to resume system calls
after dereferencing pointer arguments, and hence the sandbox is vulnerable to
[Time-of-check to
time-of-use](https://en.wikipedia.org/wiki/Time-of-check_to_time-of-use)
attacks. This is something we accept and live with. That said SydB☮x takes
some mild precautions to make
[`TOCTOU`](https://en.wikipedia.org/wiki/Time-of-check_to_time-of-use) attacks
less likely such as disallowing system calls which can access remote process
memory such as `ptrace` and `process_vm_writev`, and disallowing write access to
`/proc/${pid}/mem`.  This makes the attack vectors much harder to realize.

**Note**, as of 2023.10.07 I am happy to pay a one time award of 100€ to anyone
who develops a POC exploit utilizing `TOCTOU` to break out of SydB☮x. The POC
must preferably be written in the Rust programming language but other languages
are fine too.

## COPYRIGHT

- **SPDX-License-Identifier:** *GPL-3.0-or-later*
- Copyright © 2010, 2011, 2012, 2013, 2014, 2015, 2018, 2020, 2021, 2023 Ali Polatel <alip@chesswob.org>


ChangeLog
=========

# 3.0.0

- **Milestone**: Paludis builds under SydB☮x with recommended tests using this
  [MR](https://gitlab.exherbo.org/paludis/paludis/-/merge_requests/86).
- Sandbox command lock now defaults to **exec** rather than **off** for added
  security.
- `allowlist/successful_bind` was broken by a recent change. This is now fixed.
- The `trace/memory_access` command is fixed, `strace -c` confirms the results

# 3.0.0-beta.15

- Test suite now properly recognizes that it is running under SydB☮x and skips
  the integration tests.
- SydB☮x now properly exits with the exit code of the sandbox process and exit
  codes for error conditions are documented in `--help`.
- Fix an issue with triple star extension in path glob matches.

# 3.0.0-beta.14

- Fix an issue with /proc/pid/cmdline reader.
- `symlink` and `symlinkat` system call interceptors no longer check the target
  for access.
- Skip running integration tests when running under SydB☮x.
- `lock:exec` no longer waits for the initial **exec** call to lock the sandbox
  for all processes except the SydB☮x exec child.

# 3.0.0-beta.13

- Drop the `built` crate dependency.
- Drop the `procfs` crate dependency.
- Use the `built` crate without the `git2` feature.
- Don't use `snmalloc` as the global allocator anymore. This fixes issues with
  static linking on Gentoo.

# 3.0.0-beta.12

- Fix an issue of **stat** sandboxing with path hiding.
- The environment variable **SYD\_NO\_CROSS\_MEMORY\_ATTACH** may be set to
  disable using cross memory attach and fallback to `/proc/pid/mem`.
- The environment variable **SYD\_NO\_SYSLOG** may be set to disable logging to **syslog**.
- Canonicalize UNIX socket addresses before sandbox access check.
- Add common system directories to the allowlist in **user** profile to make
  usage more practical.
- Add `--export` argument to export secure computing rules in binary **Berkeley
  Packet Filter** format and textual **Pseudo Filter Code** formats.
- System call hooks now use system call name and arguments to determine whether
  remote path canonicalization should resolve symbolic links.
- bump MSRV from `1.69` to `1.70`.
- `error` and `warn` level logs are not written to standard error unless
  standard error is a terminal. Since logs of these levels also go to **syslog**
  this is no loss for the user. This is merely to provide convenience when
  running terminal user interfaces under SydB☮x.
- `user` profile now enables `stat` sandboxing with the user home directory
  allowlisted.

# 3.0.0-beta.11

- Added `stat` sandboxing which can be used to hide files and directories from
  the sandboxed process.
- The sandbox command `denylist/network` has been renamed to `denylist/net`.
- The sandbox command `allowlist/network` has been renamed to `allowlist/net`.
- The sandbox command `filter/network` has been renamed to `filter/net`.
- The sandbox command `sandbox/network` has been renamed to `sandbox/net`.
- `user` profile now properly allowlists screen and tmux connections.

# 3.0.0-beta.10

- When debug mode is enabled with `SYD_LOG=debug`, SydB☮x now logs all system
  calls with seccomp action other than `Allow` to the kernel log. This is useful
  in tackling problems with build failures.
- System calls with bad architecture know return `ENOSYS` rather than SydB☮x
  killing the thread.
- Disallowed system calls are now denied with `EACCES` rather than `ENOSYS`.
- SydB☮x now sets seccomp system call priority of hotter system calls to a
  higher value to improve performance.
- Fix a potential panic with `/proc/self` -> `/proc/pid`  handling in remote
  paths.

# 3.0.0-beta.9

- Fix an issue with remote path canonicalization.

# 3.0.0-beta.8

- Consolidate error handling, making it faster and more robust.
- Various fixes and improvements for the remote path canonicalization code which
  makes it faster and more robust with regards to error handling.

# 3.0.0-beta.7

- SydB☮x now ignores the signals `SIGHUP`, `SIGTSTP`, `SIGTTOU`, and `SIGTTIN`
  for uninterrupted tracing.
- The **user** profile now sets the environment variable
  `GIT_CEILING_DIRECTORIES` to `HOME` to save the user from some useless and
  annoying access violations.

# 3.0.0-beta.6

- Make the **user** profile Exherbo friendly.

# 3.0.0-beta.5

- The `user` profile now has **read** and **exec** sandboxing enabled as well as
  **write** and **network** sandboxing.
- The **triple star** extension is applied to glob patterns, ie `/dev/***`
  matches both `/dev` and any file recursively under `/dev`.
- When run without arguments, the home directory of the current user is now
  looked up from `passwd(5)` data rather than using the `HOME`
  environment variable.
- The clause **last matching rule wins** was not honored at all times. This is
  now fixed.

# 3.0.0-beta.4

- The `user` profile now also parses the file `~/.user.syd-3` if it exists.
  Note, syd uses this profile when invoked without arguments. This provides an
  easy way to spawn a working shell under sandbox.
- Fix UDP network sandboxing which was broken due to invalid error handling for
  connection-mode sockets.
- Some glob patterns in sandbox profiles `paludis`, and `user` have been fixed.

# 3.0.0-beta.3

- Run tests as integration tests, drop the `test-bin` development dependency.

# 3.0.0-beta.2

- Added the new `user` sandbox profile which allows access to user-specific
  directories such as `HOME`, and connections such as `X`, `screen`, `tmux` etc.
  When invoked without arguments, `syd` now drops to a shell with this profile.
- Replace `regex` crate with the more lightweight and performant `regex-lite`
  crate.
- Implement the `cmd/exec` sandbox command and the `syd exec` subcommand.
- Switch from `glob` crate to the `glob-match` crate for matching glob patterns.
- Fallback to `/proc/$pid/mem` if cross memory attach is not enabled in the
  kernel. Use `SYD_PROC_MEM` environment variable or the sandbox command
  `trace/memory_access:1` to force `/proc` fallback.
- `exec/kill_if_match` has been renamed to `exec/kill` which is a **breaking
  change**.
- Set `panic = abort` in release builds for reduced binary size.
- Name the polling thread `syd-poll`.
- Better error handling, and cleaner code.
- Use `parking_lot` crate for `Mutex`, and `RwLock`.
- The default magic virtual device path has been updated from `/dev/sydbox` to
  `/dev/syd` saving three letters on each typing!! This is a **breaking
  change**.
- The `core/` prefix has been removed from the configuration items
  `core/sandbox`, e.g use `sandbox/exec:on` rather than `core/sandbox/exec:on`.
  `allowlist/successful_bind` has been renamed to `trace/allow_successful_bind`,
  and `allowlist/unsupported_socket_families` has been renamed to
  `trace/allow_unsupported_socket_families`. Moreover the config item
  `core/trace/magic_lock` has been renamed to simply `lock`. This is a
  **breaking change**.
- The prefixes `unix:`, `unix-abstract:`, `inet:`, `inet6:` are no longer used
  in network addresses. Instead the pattern is treated as a UNIX shell style
  pattern if it starts with `/`, and as an IP address otherwise. There is no
  distinction between unix sockets and abstract unix sockets anymore. This is a
  **breaking change**. Check the `data/` subdirectory for a `sydbox.bash` for
  use with `Paludis`.
- Fix a bug with remote path canonicalization.
- Access violations are logged to syslog now. Use, e.g. `journalctl
  SYSLOG_IDENTIFIER=syd` to view them.

# 3.0.0-alpha.2

- When run without arguments, `sydbox` now drops into user's current running
  shell allowlisting the `HOME` directory.
- Document the CLI option `-p`, `--profile` and add `noipv4` and `noipv6`
  profiles in addition the `paludis` profile. These profiles may be stacked by
  specifying more than one `-p` arguments.
- Use a Seccomp `BPF` filter rather than a `Notify` filter for fakeroot mode.
- Improve logging to achieve consistency. We have a very simple Logger which logs
  to standard error in format `JSON` lines. There are some common keys `id` is
  always `syd`, `l` gives the `Log::Level` as an integer whereby the lower the
  value of the integer the more severe is the log condition. `t` gives a UNIX
  time stamp in seconds, and `ctx` has short context on the log entry. Errors are
  represented with the `err` key, and system call names are given with the `sys`
  key.
- The `--profile <profile-name>` and `--config @<profile-name>` is now
  supported. `Paludis` uses the former so it is important for compatibility.
  The profile file is **no longer** installed under `${sharedir}/sydbox` where
  `{sharedir}` is usually `/usr/share` and is kept as a static array in the
  program itself. In the future when `sydbox-3` has an exheres we can improve on
  this but for now this gets us going.
- The `setuid` system call is now allowed in the sandbox.
- Use `snmalloc` as the global allocator for improved performance.

# 3.0.0-alpha.1

- **New**: Added `core/allowlist/successful_bind`.
  - Utilizes `getsockname` hook, `pidfd_getfd`, and `process_vm_writev` for complete emulation.
  - Features a `TTL` of 3 mins for tracking addresses to manage zero port arguments in `bind()` system calls.

- **Improved**: Refined read, write, network/{bind,connect} sandboxing.
  - Simpler implementation, yet compatible with `Paludis` via `esandbox`.
  - No per-process sandboxing or process tree tracking; uses `/proc/$pid/cwd` when required.
  - Single set of sandbox rules with configurations pushed upfront.
  - **API Change**: Replaced `allow`, `deny` modes with simpler `on/off` toggle.
  - `core/sandbox/network` can be set to `bind` or `connect` for selective sandboxing.
  - Rule matching favors the latest rule for configuration stacking.
  - Streamlined `core/trace/magic_lock:exec` due to lack of parent/child tracking.

- **New**: Introduced `seccomp` process supervision.
  - Implemented primarily in `syd::hook` and `syd::remote`.
  - Derived from the `greenhook` crate, but with a deny-by-default `seccomp` policy.
  - Allowlisted system calls maintained in `syd::config` (currently immutable by users).
  - Notable system calls like `ptrace`, `process_vm_writev`, and `io-uring` are disabled to counteract `TOCTOU` vulnerabilities.

<!-- vim: set spell spelllang=en tw=80 : -->
