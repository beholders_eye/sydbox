//
// syd: application sandbox
// src/main.rs: Main entry point
//
// Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

//! syd: application sandbox
//! Main entry point.

// We like clean and simple code with documentation.
#![deny(missing_docs)]
#![deny(clippy::allow_attributes_without_reason)]
#![deny(clippy::arithmetic_side_effects)]
#![deny(clippy::as_ptr_cast_mut)]
#![deny(clippy::as_underscore)]
#![deny(clippy::assertions_on_result_states)]
#![deny(clippy::borrow_as_ptr)]
#![deny(clippy::branches_sharing_code)]
#![deny(clippy::case_sensitive_file_extension_comparisons)]
#![deny(clippy::cast_lossless)]
#![deny(clippy::cast_possible_truncation)]
#![deny(clippy::cast_possible_wrap)]
#![deny(clippy::cast_precision_loss)]
#![deny(clippy::cast_ptr_alignment)]
#![deny(clippy::cast_sign_loss)]
#![deny(clippy::checked_conversions)]
#![deny(clippy::clear_with_drain)]
#![deny(clippy::clone_on_ref_ptr)]
#![deny(clippy::cloned_instead_of_copied)]
#![deny(clippy::cognitive_complexity)]
#![deny(clippy::collection_is_never_read)]
#![deny(clippy::copy_iterator)]
#![deny(clippy::create_dir)]
#![deny(clippy::dbg_macro)]
#![deny(clippy::debug_assert_with_mut_call)]
#![deny(clippy::decimal_literal_representation)]
#![deny(clippy::default_trait_access)]
#![deny(clippy::default_union_representation)]
#![deny(clippy::derive_partial_eq_without_eq)]
#![deny(clippy::doc_link_with_quotes)]
#![deny(clippy::doc_markdown)]
#![deny(clippy::explicit_into_iter_loop)]
#![deny(clippy::explicit_iter_loop)]
#![deny(clippy::fallible_impl_from)]
#![deny(clippy::missing_safety_doc)]
#![deny(clippy::undocumented_unsafe_blocks)]

use std::{
    env,
    os::unix::process::CommandExt,
    process::{exit, Command},
    str::FromStr,
};

use anyhow::{bail, Context, Error};
use getargs::{Opt, Options};
use once_cell::sync::Lazy;
use syd::{
    hook::{ExportMode, Supervisor},
    sandbox, warn,
};

static SYD_VERSION: Lazy<&'static str> = Lazy::new(|| {
    if env!("SYD_GITHEAD").is_empty() {
        env!("CARGO_PKG_VERSION")
    } else {
        env!("SYD_GITHEAD")
    }
});

fn help() {
    println!(
        "sydbox-{} -- seccomp-bpf and seccomp-notify based application sandbox
Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
SPDX-License-Identifier: GPL-3.0-or-later
Usage:
  syd [-hvlr] [-m command...] [-c path...] [-p name...] [--] {{command [arg...]}}
  syd -e, --export bpf|pfc
  syd -t, --test
  syd exec {{command [arg...]}}
  syd log
-h          -- Show usage and exit
-v          -- Show version and exit
-l          -- Lock magic commands
-r          -- Return 0 as user/group id, aka \"fakeroot\" mode
-m command  -- Run a magic command during init, may be repeated
-c path     -- Path to the configuration file to use, may be repeated
-p name     -- Name of the sandbox profile to use, may be repeated
-e mode     -- Export seccomp rules in given format to standard output and exit
-t          -- Run integration tests and exit
               Requires syd-test & syd-test-do programs to be in PATH.

* syd exec may be used to construct a sandbox command to execute a
process outside the sandbox. See the description of cmd/exec
command in the README for more information.
* syd log may be used to access sandbox logs using journalctl.

# Profiles

1. paludis: Used by the Paludis package mangler.
2. noipv4: Disables IPv4 connectivity.
3. noipv6: Disables Ipv6 connectivity.
4. user: Allows user-specific directories and connections, and
         parses the file ~/.user.syd-3 if it exists.

When invoked without arguments, the current shell is executed
under sandbox with the user profile.

# Environment
SYD_LOG: Set log level. See the Enabling Logging¹ section of
 env-logger crate documentation for more information.
SYD_NO_SYSLOG: Disable logging to syslog(3). By default logs of
 severity Warn and higher are logged to syslog(3).
SYD_NO_CROSS_MEMORY_ATTACH: Disable cross memory attach, and
 fallback to /proc/$pid/mem.

# Exit Codes

Sydb☮x exits with the same exit code as the sandbox process
itself. If the sandbox process exits with a signal, Sydb☮x exits
with 14 which stands for EFAULT.  In case there was an error in
spawning or waiting for the sandbox process, Sydb☮x exits with
errno indicating the error condition. E.g. syd true\" returns 0,
\"syd false\" return 1, and \"syd -- syd true\" returns 16 which
stands for EBUSY which stands for \"Device or resource busy\"
indicating there is already a secure computing filter
loaded. tl;dr Sydb☮x won't run under Sydb☮x, similarly many
process inspection tools such as \"ltrace\", \"strace\", or
\"gdb\" won't work under Sydb☮x. Thus the sandbox process can
either be traced by attaching from outside the sandbox or
running the tracer in follow fork mode, e.g.
\"strace -f syd true\".

¹: https://docs.rs/env_logger/latest/env_logger/#enabling-logging",
        *SYD_VERSION
    );
}

#[allow(clippy::cognitive_complexity)]
fn main() -> anyhow::Result<()> {
    // Step 1: Initialize environment.
    Supervisor::init_env()?;

    // Step 2: Parse CLI arguments.
    let args = argv::iter().skip(1).map(|os| {
        // Let's not allocate, shall we?
        #[allow(clippy::disallowed_methods)]
        os.to_str()
            .expect("argument couldn't be converted to UTF-8")
    });

    let mut root = std::env::var("SYD_FAKEROOT").is_ok();
    let mut export_mode: Option<ExportMode> = None;
    let mut sandbox = sandbox::Sandbox::new();

    let mut options = Options::new(args);
    while let Some(option) = options.next_opt().context("calling Options::next")? {
        match option {
            Opt::Short('h') | Opt::Long("help") => {
                help();
                return Ok(());
            }
            Opt::Short('v') | Opt::Long("version") => {
                println!("sydbox {}", *SYD_VERSION);
                return Ok(());
            }
            Opt::Short('e') | Opt::Long("export") => {
                export_mode = Some(ExportMode::from_str(
                    options.value().context("--export requires an argument!")?,
                )?);
            }
            Opt::Short('t') | Opt::Long("test") => {
                Command::new("syd-test").exec();
            }
            Opt::Short('l') | Opt::Long("lock") => {
                sandbox.lock();
            }
            Opt::Short('r') | Opt::Long("root") => {
                root = true;
            }
            Opt::Short('m') | Opt::Long("magic") => {
                let cmd = options.value().context("--magic requires an argument!")?;
                sandbox
                    .config(cmd)
                    .context(format!("Failed to execute magic command `{cmd}'."))?;
            }
            Opt::Short('c') | Opt::Long("config") => {
                let path = options.value().context("--config requires an argument!")?;
                sandbox
                    .parse_config_file(path)
                    .context(format!("Failed to parse configuration file `{path}'."))?;
            }
            Opt::Short('p') | Opt::Long("profile") => {
                let profile = options.value().context("--profile requires an argument!")?;
                sandbox.parse_profile(profile).context(format!(
                    "Failed to parse configuration profile `{profile}'."
                ))?;
            }
            Opt::Short(c) => {
                bail!("Invalid option `-{c}'!");
            }
            Opt::Long(c) => {
                bail!("Invalid option `--{c}'!");
            }
        }
    }
    if root {
        sandbox.set_root(true);
    }

    // Step 3: Initialize sandbox supervisor.
    let nproc = num_cpus::get();
    let mut supervisor = Supervisor::new(sandbox, nproc, export_mode).context(format!(
        "Error creating sandbox with {} threads and export mode {:?}.",
        nproc, export_mode
    ))?;
    supervisor.init();

    // Step 4: Prepare the command to execute.
    let mut argv = options.positionals();
    let argv0 = match (export_mode, argv.next()) {
        (Some(_), _) => "true".to_string(),
        (_, Some("log")) => {
            Command::new("journalctl")
                .args(["SYSLOG_IDENTIFIER=syd"])
                .exec();
            exit(1);
        }
        (_, Some("exec")) => {
            // Split the arguments using the ASCII Unit Separator character
            let args: Vec<_> = argv.collect();
            let args = args.join("\x1F");

            // Format it using /dev/syd/cmd/exec!<concatenated-path>
            let path = format!("/dev/syd/cmd/exec!{args}");
            print!("{path}");
            warn!("ctx": "syd/exec", "pid": nix::unistd::getpid().as_raw(), "path": path);
            exit(0);
        }
        (_, Some(argv0)) => argv0.to_string(),
        (_, None) => {
            let shell = env::var("SHELL").unwrap_or("/bin/sh".to_string());
            #[allow(clippy::disallowed_methods)]
            Command::new(env::current_exe().unwrap())
                .args(["-puser", "--", &shell, "-"])
                .exec();
            exit(1);
        }
    };
    let mut command = Command::new(argv0);
    let command = command.args(argv);

    // Step 5: Spawn the program under sandbox and return the same error code.
    let (pid, thread_handle, pool) = match supervisor.spawn(command) {
        Ok((pid, thread_handle, pool)) => (pid, thread_handle, pool),
        Err(errno) => {
            let error = Error::new(errno).context("Failed to spawn command under sandbox.");
            eprintln!("{error:?}");
            exit(errno as i32);
        }
    };
    exit(match Supervisor::wait(pid, thread_handle, pool) {
        Ok(status) => status.code().unwrap_or(nix::libc::EFAULT),
        Err(errno) => {
            let error = Error::new(errno).context("Failed to wait for sandboxed process.");
            eprintln!("{error:?}");
            errno as i32
        }
    });
}
