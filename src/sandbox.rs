//
// syd: application sandbox
// src/sandbox.rs: Sandbox configuration
//
// Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{
    clone::Clone,
    collections::hash_map::RandomState,
    env, fmt,
    fs::File,
    io::{self, BufRead, BufReader},
    net::IpAddr,
    ops::{Deref, DerefMut},
    path::Path,
    process::Command,
    str::FromStr,
    time::Duration,
};

use anyhow::{bail, Context};
use bitflags::bitflags;
use glob_match::glob_match;
use ipnetwork::IpNetwork;
use libseccomp::ScmpNotifReq;
use nix::{
    errno::Errno,
    fcntl::OFlag,
    unistd::{AccessFlags, Pid},
};
use once_cell::sync::Lazy;
use parking_lot::{RwLockReadGuard, RwLockWriteGuard};
use regex_lite::{Captures, Regex};
use ttl_cache::TtlCache;

use crate::{config::*, error, hook::RemoteProcess, warn};

static RE_RULE: Lazy<Regex> = Lazy::new(|| {
    #[allow(clippy::disallowed_methods)]
    Regex::new(
        r"(?x)
        \A
        (
            # We either have exec/kill or an action with a capability.
            exec/kill |
            (?P<act>
                allowlist |
                denylist |
                filter
            )/
            (?P<cap>
                read |
                stat |
                write |
                exec |
                net/bind |
                net/connect
            )
        )
        (?P<mod>\+|\-)
        (?P<pat>.*)
        \z
    ",
    )
    .expect("Invalid sandbox rule regex, please file a bug!")
});
static RE_LOOPBACK: Lazy<Regex> = Lazy::new(|| {
    #[allow(clippy::disallowed_methods)]
    Regex::new(
        r"(?x)
        \A
        (
            allowlist |
            denylist |
            filter
        )
        /net/
        (
            bind |
            connect
        )
        [+-]
        LOOPBACK
        @
        [0-9]+
        (-[0-9]+)?
        \z
    ",
    )
    .expect("Invalid LOOPBACK regex, please file a bug!")
});

static RE_LOOPBACK6: Lazy<Regex> = Lazy::new(|| {
    #[allow(clippy::disallowed_methods)]
    Regex::new(
        r"(?x)
        \A
        (
            allowlist |
            denylist |
            filter
        )
        /net/
        (bind|connect)
        [+-]
        LOOPBACK6
        @
        [0-9]+
        (-[0-9]+)?
        \z",
    )
    .expect("Invalid LOOPBACK6 regex, please file a bug!")
});

static RE_LOCAL: Lazy<Regex> = Lazy::new(|| {
    #[allow(clippy::disallowed_methods)]
    Regex::new(
        r"(?x)
        \A
        (
            allowlist |
            denylist |
            filter
        )
        /net/
        (
            bind |
            connect
        )
        [+-]
        LOCAL
        @
        [0-9]+
        (-[0-9]+)?
        \z",
    )
    .expect("Invalid LOCAL regex, please file a bug!")
});

static RE_LOCAL6: Lazy<Regex> = Lazy::new(|| {
    #[allow(clippy::disallowed_methods)]
    Regex::new(
        r"(?x)
        \A
        (
            allowlist |
            denylist |
            filter
        )
        /net/
        (
            bind |
            connect
        )
        [+-]
        LOCAL6
        @
        [0-9]+
        (-[0-9]+)?
        \z
    ",
    )
    .expect("Invalid LOCAL6 regex, please file a bug!")
});

bitflags! {
    /// Sandboxing capabilities
    pub struct Capability: u16 {
        /// Read capability
        const CAP_READ = 1;
        /// List capability
        const CAP_STAT = 2;
        /// Write capability
        const CAP_WRITE = 4;
        /// Execute capability
        const CAP_EXEC = 8;
        /// Network connect capability
        const CAP_CONNECT = 16;
        /// Network bind capability
        const CAP_BIND = 32;
    }
}

impl fmt::Display for Capability {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut capabilities = Vec::new();

        if self.contains(Capability::CAP_READ) {
            capabilities.push("Read");
        }
        if self.contains(Capability::CAP_STAT) {
            capabilities.push("Stat");
        }
        if self.contains(Capability::CAP_WRITE) {
            capabilities.push("Write");
        }
        if self.contains(Capability::CAP_EXEC) {
            capabilities.push("Execute");
        }
        if self.contains(Capability::CAP_CONNECT) {
            capabilities.push("Connect");
        }
        if self.contains(Capability::CAP_BIND) {
            capabilities.push("Bind");
        }

        write!(f, "{}", capabilities.join(", "))
    }
}

impl From<&Captures<'_>> for Capability {
    fn from(captures: &Captures) -> Self {
        if let Some(cap) = captures.name("cap") {
            match cap.as_str() {
                "read" => Capability::CAP_READ,
                "stat" => Capability::CAP_STAT,
                "write" => Capability::CAP_WRITE,
                "exec" => Capability::CAP_EXEC,
                "net/bind" => Capability::CAP_BIND,
                "net/connect" => Capability::CAP_CONNECT,
                _ => unreachable!(),
            }
        } else {
            Capability::CAP_EXEC // See the regex.
        }
    }
}

impl TryFrom<(&RemoteProcess, &ScmpNotifReq, &str)> for Capability {
    type Error = Errno;

    // Find out capabilities of the system call using the system call name and seccomp request.
    fn try_from(value: (&RemoteProcess, &ScmpNotifReq, &str)) -> Result<Self, Errno> {
        let (proc, req, syscall_name) = value;
        match syscall_name {
            name if Capability::exec(name) => Ok(Self::CAP_EXEC),
            name if Capability::stat(name) => Ok(Self::CAP_STAT),
            "access" | "faccessat" | "faccessat2" => Ok(Capability::access(syscall_name, req)),
            "open" | "openat" | "openat2" => Capability::open(syscall_name, req, proc),
            _ => Ok(Self::CAP_WRITE),
        }
    }
}

impl Capability {
    fn open(syscall_name: &str, req: &ScmpNotifReq, proc: &RemoteProcess) -> Result<Self, Errno> {
        Ok(match syscall_name {
            "open" | "openat" => {
                let flidx = if syscall_name == "open" { 1 } else { 2 };
                #[allow(clippy::cast_possible_truncation)]
                let flags = OFlag::from_bits_truncate(req.data.args[flidx] as nix::libc::c_int);
                match flags & OFlag::O_ACCMODE {
                    OFlag::O_RDONLY => Self::CAP_READ,
                    _ => Self::CAP_WRITE,
                }
            }
            "openat2" => {
                #[allow(clippy::cast_possible_truncation)]
                let rohow =
                    proc.remote_ohow(req.data.args[2] as usize, req.data.args[3] as usize)?;
                #[allow(clippy::cast_possible_truncation)]
                let flags = OFlag::from_bits_truncate(rohow.flags as nix::libc::c_int);
                match flags & OFlag::O_ACCMODE {
                    OFlag::O_RDONLY => Self::CAP_READ,
                    _ => Self::CAP_WRITE,
                }
            }
            _ => unreachable!(),
        })
    }

    fn access(syscall_name: &str, req: &ScmpNotifReq) -> Self {
        let flidx = if syscall_name == "access" { 1 } else { 2 };
        #[allow(clippy::cast_possible_truncation)]
        let flags = AccessFlags::from_bits_truncate(req.data.args[flidx] as nix::libc::c_int);
        let mut caps = Self::empty();
        if flags.contains(AccessFlags::F_OK) || flags.contains(AccessFlags::R_OK) {
            caps |= Self::CAP_READ;
        }
        if flags.contains(AccessFlags::W_OK) {
            caps |= Self::CAP_WRITE;
        }
        if flags.contains(AccessFlags::X_OK) {
            caps |= Self::CAP_EXEC;
        }
        caps
    }

    fn exec(syscall_name: &str) -> bool {
        matches!(syscall_name, "execve" | "execveat")
    }

    fn stat(syscall_name: &str) -> bool {
        matches!(
            syscall_name,
            "chdir"
                | "fchdir"
                | "getdents"
                | "getdents64"
                | "stat"
                | "statx"
                | "fstat"
                | "lstat"
                | "newfstatat"
                | "getxattr"
                | "listxattr"
                | "flistxattr"
                | "llistxattr"
        )
    }
}

bitflags! {
    /// Sandboxing options
    pub struct Flag: u16 {
        /// Allow successful bind calls for subsequent connect calls
        const FL_ALLOW_SUCCESSFUL_BIND = 1;
        /// Allow socket families which are unsupported
        const FL_ALLOW_UNSUPPORTED_SOCKET_FAMILIES = 2;
    }
}

impl fmt::Display for Flag {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut flags = Vec::new();

        if self.contains(Flag::FL_ALLOW_SUCCESSFUL_BIND) {
            flags.push("Allow Successful Bind");
        }
        if self.contains(Flag::FL_ALLOW_UNSUPPORTED_SOCKET_FAMILIES) {
            flags.push("Allow Unsupported Socket Families");
        }

        write!(f, "{}", flags.join(", "))
    }
}

struct TtlSet<T: std::hash::Hash + Eq + Clone> {
    cache: TtlCache<T, (), RandomState>,
    duration: Duration,
}

impl<T: std::hash::Hash + Eq + Clone> TtlSet<T> {
    fn new(duration: Duration) -> Self {
        Self {
            cache: TtlCache::new(BIND0_CACHE_SIZE),
            duration,
        }
    }

    fn get(&mut self, key: T) -> bool {
        self.cache.get(&key).is_some()
    }

    fn insert(&mut self, key: T) {
        self.cache.insert(key, (), self.duration);
    }

    fn remove(&mut self, key: T) {
        self.cache.remove(&key);
    }
}

impl<T: std::hash::Hash + Eq + Clone> Default for TtlSet<T> {
    fn default() -> Self {
        // Default to 3 minutes TTL
        Self::new(Duration::from_secs(180))
    }
}

impl<T: std::hash::Hash + Eq + Clone + fmt::Debug> fmt::Debug for TtlSet<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut tmp = self.cache.clone();
        let keys = tmp.iter().map(|(k, _v)| k).collect::<Vec<_>>();

        f.debug_struct("TtlSet")
            .field("keys", &keys)
            .field("duration", &self.duration)
            .finish()
    }
}

/// Represents a network address pattern
#[derive(Debug, Eq, PartialEq)]
pub struct AddressPattern {
    addr: IpNetwork,
    port: [u16; 2],
}

impl fmt::Display for AddressPattern {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.port[0] == self.port[1] {
            write!(f, "{}:{}", self.addr, self.port[0])
        } else {
            write!(f, "{}:{}-{}", self.addr, self.port[0], self.port[1])
        }
    }
}

/// Represents a rule pattern
#[derive(Debug, Eq, PartialEq)]
pub enum Pattern {
    /// Represents a Unix shell style pattern
    Glob(String),
    /// Represents a network address pattern
    Cidr(AddressPattern),
}

impl fmt::Display for Pattern {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Pattern::Glob(pattern) => write!(f, "Glob({pattern})"),
            Pattern::Cidr(address) => write!(f, "Cidr({address})"),
        }
    }
}

/// Represents a rule action
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum Action {
    /// Filter
    Filter,
    /// Allowlist
    Allow,
    /// Denylist
    Deny,
    /// Kill
    Kill,
}

impl fmt::Display for Action {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Action::Filter => write!(f, "Filter"),
            Action::Allow => write!(f, "Allow"),
            Action::Deny => write!(f, "Deny"),
            Action::Kill => write!(f, "Kill"),
        }
    }
}

impl From<&Captures<'_>> for Action {
    fn from(captures: &Captures) -> Self {
        if let Some(act) = captures.name("act") {
            match act.as_str() {
                "allowlist" => Action::Allow,
                "denylist" => Action::Deny,
                "filter" => Action::Filter,
                _ => unreachable!(),
            }
        } else {
            Action::Kill
        }
    }
}

/// Represents a sandboxing rule.
#[derive(Debug)]
pub struct Rule {
    act: Action,
    cap: Capability,
    pat: Pattern,
}

impl fmt::Display for Rule {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "Action: {}, Capability: {}, Pattern: {}",
            self.act, self.cap, self.pat
        )
    }
}

/// Represents the state of the magic command lock.
#[derive(Debug, Eq, PartialEq)]
pub enum LockState {
    /// Lock is unset, sandbox commands are allowed.
    Unset,
    /// Sandbox commands are only allowed to the sydbox execve child.
    Exec,
    /// Lock is set, sandbox commands are not allowed.
    Set,
}

type Bind = (Pid, IpAddr);

/// Sandbox
#[derive(Debug, Default)]
pub struct Sandbox {
    /// Sandbox options represented using a set of `Flag` flags.
    flags: Flag,

    /// Sandbox state represented using a set of `Capability` flags.
    state: Capability,

    /// State of the magic lock.
    lock: LockState,

    /// List of sandboxing rules.
    list: Vec<Rule>,

    /// List of socket addresses with port 0.
    bind: TtlSet<Bind>,

    /// Process ID of the sydbox execve child.
    cpid: nix::libc::pid_t,

    /// If `true` all id system calls return 0 in the sandbox.
    root: bool,
}

/// Sandbox guard to use it practically under a read/write lock.
#[derive(Debug)]
pub enum SandboxGuard<'a> {
    /// Sandbox locked for read
    Read(RwLockReadGuard<'a, Sandbox>),
    /// Sandbox locked for write
    Write(RwLockWriteGuard<'a, Sandbox>),
}

impl Default for Flag {
    fn default() -> Self {
        Self::FL_ALLOW_SUCCESSFUL_BIND | Self::FL_ALLOW_UNSUPPORTED_SOCKET_FAMILIES
    }
}

impl Default for Capability {
    fn default() -> Self {
        Self::CAP_WRITE | Self::CAP_BIND | Self::CAP_CONNECT
    }
}

impl Default for LockState {
    fn default() -> Self {
        Self::Exec
    }
}

impl FromStr for LockState {
    type Err = io::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "off" => Ok(Self::Unset),
            "on" => Ok(Self::Set),
            "exec" => Ok(Self::Exec),
            _ => Err(io::Error::from_raw_os_error(nix::libc::EINVAL)),
        }
    }
}

impl<'a> Deref for SandboxGuard<'a> {
    type Target = Sandbox;
    fn deref(&self) -> &Self::Target {
        match self {
            SandboxGuard::Read(guard) => guard,
            SandboxGuard::Write(guard) => guard,
        }
    }
}

impl<'a> DerefMut for SandboxGuard<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        match self {
            SandboxGuard::Write(guard) => guard,
            _ => panic!("Cannot mutate a read-locked Sandbox!"),
        }
    }
}

impl fmt::Display for Sandbox {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "Sandbox:")?;
        writeln!(f, "  Lock: {:?}", self.lock)?;
        writeln!(f, "  Capabilities: {}", self.state)?;
        writeln!(f, "  Options: {}", self.flags)?;
        writeln!(f, "  Root Mode: {}", self.root)?;
        writeln!(f, "  Process ID: {}", self.cpid)?;
        writeln!(
            f,
            "  Rules: (total {}, highest precedence first)",
            self.list.len()
        )?;
        for (idx, rule) in self.list.iter().rev().enumerate() {
            // rev() because last matching rule wins.
            let idx = idx.saturating_add(1);
            writeln!(f, "    {idx}. {rule}")?;
        }
        writeln!(f, "  Bind Addresses: {0:?}", self.bind)
    }
}

impl Sandbox {
    /// Parses a configuration file and applies its configuration to the sandbox.
    ///
    /// This function reads the given configuration file line by line. It skips lines that are either
    /// empty or start with a '#' (treated as comments). For each valid line, it applies its
    /// configuration to the provided sandbox.
    ///
    /// # Arguments
    ///
    /// * `path` - A reference to the path of the configuration file. This can be any type that
    ///            implements the `AsRef<Path>` trait.
    ///
    /// # Returns
    ///
    /// * A Result indicating the success or failure of the operation.
    ///
    /// # Errors
    ///
    /// This function will return an error if:
    /// * There's an issue in opening the configuration file.
    /// * There's an error reading a line from the file.
    /// * There's an issue in parsing and applying a configuration line to the sandbox.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use syd::sandbox::Sandbox;
    ///
    /// let path = "/path/to/config/file";
    /// let mut sandbox = Sandbox::new();
    /// sandbox
    ///     .parse_config_file(&path)
    ///     .expect("Failed to parse configuration file");
    /// ```
    pub fn parse_config_file<P: AsRef<Path>>(&mut self, path: P) -> anyhow::Result<()> {
        let file = File::open(path).context("Failed to open configuration file.")?;
        let reader = BufReader::new(file);

        for (idx, line) in reader.lines().enumerate() {
            let lcnt = idx.saturating_add(1);
            let line = line.context(format!("Failed to read line {lcnt}."))?;
            // Trim the line to remove any leading or trailing whitespace
            let line = line.trim();
            // Check if the line is empty or starts with '#'
            if line.is_empty() || line.starts_with('#') {
                continue;
            }
            self.config(line)
                .context(format!("Failed to parse line {lcnt}: `{line}'."))?;
        }

        Ok(())
    }

    /// Parses the given profile and applies its configuration to the sandbox.
    ///
    /// This function supports multiple predefined profiles such as "paludis", "noipv4", "noipv6", and "user".
    /// Each profile corresponds to a set of configuration lines which are applied to the sandbox.
    /// The "user" profile includes both static configurations and dynamic ones that depend on the
    /// environment and the existence of a user-specific configuration file.
    ///
    /// # Arguments
    ///
    /// * `name` - A string slice that holds the name of the profile to be parsed.
    /// * `sandbox` - A mutable reference to the sandbox where the profile configurations will be applied.
    ///
    /// # Returns
    ///
    /// * A Result indicating the success or failure of the operation.
    ///
    /// # Errors
    ///
    /// This function will return an error if:
    /// * The profile name is invalid.
    /// * There's an issue in parsing the configuration lines.
    /// * There's an issue in reading or parsing the user-specific configuration file for the "user" profile.
    pub fn parse_profile(&mut self, name: &str) -> anyhow::Result<()> {
        // Inner function to handle repetitive logic of applying configurations
        fn apply_config(
            sandbox: &mut Sandbox,
            profile: &[&str],
            size: usize,
        ) -> anyhow::Result<()> {
            for (idx, line) in profile.iter().take(size).enumerate() {
                let lcnt = idx.saturating_add(1);
                sandbox
                    .config(line)
                    .context(format!("Failed to parse line {lcnt}: `{line}'."))?;
            }
            Ok(())
        }

        match name {
            "paludis" => apply_config(self, &PROFILE_PALUDIS, PROFILE_PALUDIS_SIZE),
            "noipv4" => apply_config(self, &PROFILE_NOIPV4, PROFILE_NOIPV4_SIZE),
            "noipv6" => apply_config(self, &PROFILE_NOIPV6, PROFILE_NOIPV6_SIZE),
            "user" => {
                // Step 1: Apply static user configuration defined at compile-time.
                apply_config(self, &PROFILE_USER, PROFILE_USER_SIZE)?;

                // Step 2: Apply dynamic, user-specific configuration.
                let uid = nix::unistd::getuid();
                let name = crate::get_user_name(uid);
                let mut home = crate::get_user_home(&name);

                // Save the user from some annoying warnings.
                if env::var("GIT_CEILING_DIRECTORIES").is_err() {
                    env::set_var("GIT_CEILING_DIRECTORIES", &home);
                }

                if !home.ends_with('/') {
                    home.push('/');
                }

                // /home
                // We allow read(/home/user) but not write(/home/user),
                // read|write(/home/user/**) is ok, i.e. the user can
                // not delete their home directory under the sandbox
                // which is a nice and funny protection.
                self.config(&format!("allowlist/read+{home}***"))?;
                self.config(&format!("allowlist/stat+{home}***"))?;
                self.config(&format!("allowlist/write+{home}**"))?;
                self.config(&format!("allowlist/exec+{home}**"))?;
                self.config(&format!("allowlist/net/bind+{home}**"))?;
                self.config(&format!("allowlist/net/connect+{home}**"))?;
                // /run/user/uid
                self.config(&format!("allowlist/read+/run/user/{uid}/**"))?;
                self.config(&format!("allowlist/write+/run/user/{uid}/**"))?;

                // Step 3: Parse the user configuration file if it exists.
                let rc = Path::new(&home).join(".user.syd-3");
                if rc.is_file() {
                    self.parse_config_file(&rc)
                } else {
                    Ok(())
                }
            }
            _ => bail!("Invalid profile `{name}'"),
        }
    }

    /// Configures the sandbox using a specified command.
    ///
    /// This method provides a central point for configuring the sandbox. It interprets and
    /// processes a variety of commands to adjust the sandbox's state, manage its tracing,
    /// handle regex-based configurations, and more.
    ///
    /// # Arguments
    ///
    /// * `command` - A string slice that represents the command to be executed.
    ///
    /// # Returns
    ///
    /// * A `Result` that indicates the success or failure of the operation. In the event of a
    ///   failure, it returns an appropriate error from the `Errno` enum.
    ///
    /// # Commands
    ///
    /// - If the command is empty or matches the API version, it simply returns `Ok(())`.
    /// - If the command is "dump", it prints the current state of the sandbox and returns `Ok(())`.
    /// - If the command starts with "lock", it attempts to set the sandbox's lock state.
    /// - If the command matches one of the supported commands, it applies the command to the sandbox.
    ///   See the ["Configuration" section in the README.md file](https://crates.io/crates/syd#configuration)
    ///   for a list of supported commands.
    /// - If none of the above conditions are met, it returns an error indicating invalid input.
    ///
    /// # Examples
    ///
    /// ```
    /// use syd::sandbox::Sandbox;
    ///
    /// let mut sandbox = Sandbox::new();
    /// sandbox.config("dump").expect("Failed to dump the sandbox");
    /// sandbox
    ///     .config("lock:on")
    ///     .expect("Failed to lock the sandbox");
    /// ```
    pub fn config(&mut self, command: &str) -> Result<(), Errno> {
        if command.is_empty() || command == API_VERSION {
            Ok(())
        } else if command == "dump" {
            eprintln!("{self}");
            Ok(())
        } else if let Some(state) = command.strip_prefix("lock:") {
            self.lock = LockState::from_str(state).map_err(|_| Errno::EINVAL)?;
            Ok(())
        } else if let Some(command) = command.strip_prefix("cmd/") {
            self.handle_sandbox_command(command)
        } else if let Some(command) = command.strip_prefix("sandbox/") {
            self.handle_sandbox_config(command)
        } else if let Some(command) = command.strip_prefix("trace/") {
            self.handle_trace_config(command)
        } else if self.handle_regex_config(command)? {
            Ok(())
        } else if let Some(captures) = RE_RULE.captures(command) {
            self.handle_rule_config(&captures)
        } else {
            Err(Errno::EINVAL)
        }
    }

    fn handle_regex_config(&mut self, command: &str) -> Result<bool, Errno> {
        if RE_LOOPBACK.is_match(command) {
            let command = command.replace("LOOPBACK", "127.0.0.0/8");
            self.config(&command)?;
            Ok(true)
        } else if RE_LOOPBACK6.is_match(command) {
            let command = command.replace("LOOPBACK6", "::1/8");
            self.config(&command)?;
            Ok(true)
        } else if RE_LOCAL.is_match(command) {
            let command = command.replace("LOCAL", "127.0.0.0/8");
            self.config(&command)?;
            let command = command.replace("LOCAL", "10.0.0.0/8");
            self.config(&command)?;
            let command = command.replace("LOCAL", "172.16.0.0/12");
            self.config(&command)?;
            let command = command.replace("LOCAL", "192.168.0.0/16");
            self.config(&command)?;
            Ok(true)
        } else if RE_LOCAL6.is_match(command) {
            let command = command.replace("LOCAL6", "::1");
            self.config(&command)?;
            let command = command.replace("LOCAL6", "fe80::/7");
            self.config(&command)?;
            let command = command.replace("LOCAL6", "fc00::/7");
            self.config(&command)?;
            let command = command.replace("LOCAL6", "fec0::/7");
            self.config(&command)?;
            Ok(true)
        } else {
            Ok(false)
        }
    }

    fn handle_sandbox_config(&mut self, command: &str) -> Result<(), Errno> {
        let (action, cap) = match command.chars().last() {
            Some('?') => (&command[..command.len().saturating_sub(1)], Some("?")),
            _ => {
                let mut splits = command.splitn(2, ':');
                (splits.next().unwrap_or(""), splits.next())
            }
        };

        match (action, cap) {
            ("read", Some("on")) => self.state.insert(Capability::CAP_READ),
            ("read", Some("off")) => self.state.remove(Capability::CAP_READ),
            ("read", Some("?")) => {
                if self.state.contains(Capability::CAP_READ) {
                    return Ok(());
                } else {
                    return Err(Errno::ENOENT);
                }
            }

            ("stat", Some("on")) => self.state.insert(Capability::CAP_STAT),
            ("stat", Some("off")) => self.state.remove(Capability::CAP_STAT),
            ("stat", Some("?")) => {
                if self.state.contains(Capability::CAP_STAT) {
                    return Ok(());
                } else {
                    return Err(Errno::ENOENT);
                }
            }

            ("write", Some("on")) => self.state.insert(Capability::CAP_WRITE),
            ("write", Some("off")) => self.state.remove(Capability::CAP_WRITE),
            ("write", Some("?")) => {
                if self.state.contains(Capability::CAP_WRITE) {
                    return Ok(());
                } else {
                    return Err(Errno::ENOENT);
                }
            }

            ("exec", Some("on")) => self.state.insert(Capability::CAP_EXEC),
            ("exec", Some("off")) => self.state.remove(Capability::CAP_EXEC),
            ("exec", Some("?")) => {
                if self.state.contains(Capability::CAP_EXEC) {
                    return Ok(());
                } else {
                    return Err(Errno::ENOENT);
                }
            }

            ("net", Some("on")) => {
                self.state.insert(Capability::CAP_BIND);
                self.state.insert(Capability::CAP_CONNECT);
            }
            ("net", Some("off")) => {
                self.state.remove(Capability::CAP_BIND);
                self.state.remove(Capability::CAP_CONNECT);
            }
            ("net", Some("bind")) => self.state.insert(Capability::CAP_BIND),
            ("net", Some("connect")) => self.state.insert(Capability::CAP_CONNECT),
            ("net", Some("?")) => {
                if !self.state.contains(Capability::CAP_BIND)
                    && !self.state.contains(Capability::CAP_CONNECT)
                {
                    return Err(Errno::ENOENT);
                } else {
                    return Ok(());
                }
            }

            _ => return Err(Errno::EINVAL),
        }
        Ok(())
    }

    fn handle_sandbox_command(&mut self, command: &str) -> Result<(), Errno> {
        if let Some(command) = command.strip_prefix("exec!") {
            // Splitting the command using the Unit Separator character
            let parts: Vec<&str> = command.split('\x1F').collect();

            // Paranoid checks: Ensure the command and its arguments are not empty
            if parts.is_empty() || parts[0].is_empty() {
                error!("ctx": "cmd/exec", "err": "Empty or invalid command provided.");
                return Err(Errno::EINVAL);
            }

            let program = parts[0];
            let args = &parts[1..];

            // Safely spawning a child process
            match Command::new(program).args(args).spawn() {
                Ok(child) => {
                    warn!("ctx": "cmd/exec", "cmd": program, "pid": child.id());
                    Ok(())
                }
                Err(error) => {
                    error!("ctx": "cmd/exec", "cmd": program, "args": format!("{args:?}"), "err": format!("Failed to execute command: {error}"));
                    Err(Errno::from_i32(
                        error.raw_os_error().unwrap_or(nix::libc::EINVAL),
                    ))
                }
            }
        } else {
            Err(Errno::ENOENT)
        }
    }

    fn handle_trace_config(&mut self, command: &str) -> Result<(), Errno> {
        match command {
            "allow_successful_bind:t"
            | "allow_successful_bind:1"
            | "allow_successful_bind:true" => {
                self.flags.insert(Flag::FL_ALLOW_SUCCESSFUL_BIND);
            }
            "allow_successful_bind:f"
            | "allow_successful_bind:0"
            | "allow_successful_bind:false" => {
                self.flags.remove(Flag::FL_ALLOW_SUCCESSFUL_BIND);
            }
            "allow_unsupported_socket_families:t"
            | "allow_unsupported_socket_families:1"
            | "allow_unsupported_socket_families:true" => {
                self.flags
                    .insert(Flag::FL_ALLOW_UNSUPPORTED_SOCKET_FAMILIES);
            }
            "allow_unsupported_socket_families:f"
            | "allow_unsupported_socket_families:0"
            | "allow_unsupported_socket_families:false" => {
                self.flags
                    .remove(Flag::FL_ALLOW_UNSUPPORTED_SOCKET_FAMILIES);
            }
            "memory_access:0" => {
                env::remove_var(NO_CROSS_MEMORY_ATTACH_ENVIRON);
            }
            "memory_access:1" => {
                env::set_var(NO_CROSS_MEMORY_ATTACH_ENVIRON, "1");
            }
            _ => {
                return Err(Errno::EINVAL);
            }
        }
        Ok(())
    }

    fn handle_rule_config(&mut self, captures: &Captures) -> Result<(), Errno> {
        let act = Action::from(captures);
        let cap = Capability::from(captures);
        let add = &captures["mod"] == "+";
        let pat = &captures["pat"];
        let ip =
            !pat.starts_with('/') && cap.intersects(Capability::CAP_BIND | Capability::CAP_CONNECT);

        if add {
            if ip {
                self.rule_add_cidr(act, cap, pat.as_ref())
            } else {
                self.rule_add_glob(act, cap, pat.as_ref())
            }
        } else if ip {
            self.rule_del_cidr(act, cap, pat.as_ref())
        } else {
            self.rule_del_glob(act, cap, pat.as_ref())
        }
    }

    /// Remove CIDR with port range, removes all instances for predictability.
    fn rule_del_cidr(&mut self, act: Action, cap: Capability, pat: &str) -> Result<(), Errno> {
        let mut split = pat.splitn(2, '@');
        if let (Some(addr), Some(port)) = (split.next(), split.next()) {
            let mut split = port.splitn(2, '-');
            if let Some(port0) = split.next() {
                if let Ok(port0) = port0.parse::<u16>() {
                    let port1 = split.next().unwrap_or("0");
                    if let Ok(port1) = port1.parse::<u16>() {
                        if let Ok(addr) = IpNetwork::from_str(addr) {
                            self.list.retain(|rule| {
                                if act != rule.act || !rule.cap.contains(cap) {
                                    return true;
                                }
                                if let Pattern::Cidr(network) = &rule.pat {
                                    if port0 == network.port[0]
                                        && port1 == network.port[1]
                                        && addr == network.addr
                                    {
                                        // negate due to retain.
                                        return false;
                                    }
                                }
                                true
                            });
                            return Ok(());
                        }
                    }
                }
            }
        }
        Err(Errno::ENOENT)
    }

    /// Add CIDR with port range
    /// The rule is either a Unix shell style pattern, or
    /// a network address, one of the following formats:
    ///
    /// 1. PATTERN
    /// 2. IP/NETMASK@$PORT
    ///
    /// PATTERN must start with a slash, `/`.
    /// /NETMASK may be omitted.
    /// PORT is a single integer or two in format port1-port2
    pub fn rule_add_cidr(&mut self, act: Action, cap: Capability, pat: &str) -> Result<(), Errno> {
        let mut split = pat.splitn(2, '@');
        if let (Some(addr), Some(port)) = (split.next(), split.next()) {
            let mut split = port.splitn(2, '-');
            if let Some(port0_str) = split.next() {
                if let Ok(port0) = port0_str.parse::<u16>() {
                    let port1 = split.next().unwrap_or(port0_str);
                    if let Ok(port1) = port1.parse::<u16>() {
                        if let Ok(addr) = IpNetwork::from_str(addr) {
                            self.list.push(Rule {
                                act,
                                cap,
                                pat: Pattern::Cidr(AddressPattern {
                                    addr,
                                    port: [port0, port1],
                                }),
                            });
                            return Ok(());
                        }
                    }
                }
            }
        }
        Err(Errno::EINVAL)
    }

    /// Remove Unix shell style pattern, removes all instances for predictability.
    pub fn rule_del_glob(&mut self, act: Action, cap: Capability, pat: &str) -> Result<(), Errno> {
        // Expand foo/*** to [foo, foo/**]
        if let Some(pat) = pat.strip_suffix("/***") {
            let pat_doublestar = format!("{pat}/**");
            self.rule_del_glob(act, cap, if pat.is_empty() { "/" } else { pat })?;
            self.rule_del_glob(act, cap, &pat_doublestar)?;
            return Ok(());
        }

        self.list.retain(|rule| {
            if act != rule.act || !rule.cap.contains(cap) {
                return true;
            }
            if let Pattern::Glob(pattern) = &rule.pat {
                // negate due to retain.
                return pattern != pat;
            }
            true
        });
        Ok(())
    }

    /// Add Unix shell style pattern
    pub fn rule_add_glob(&mut self, act: Action, cap: Capability, pat: &str) -> Result<(), Errno> {
        // Expand foo/*** to [foo, foo/**]
        if let Some(pat) = pat.strip_suffix("/***") {
            let pat_doublestar = format!("{pat}/**");
            self.rule_add_glob(act, cap, if pat.is_empty() { "/" } else { pat })?;
            self.rule_add_glob(act, cap, &pat_doublestar)?;
            return Ok(());
        }

        self.list.push(Rule {
            act,
            cap,
            pat: Pattern::Glob(pat.to_owned()),
        });
        Ok(())
    }

    /// Check IPv{4,6} address for access.
    pub fn check_ip(&self, cap: Capability, addr: IpAddr, port: u16) -> Action {
        for rule in self.list.iter().rev() {
            // rev() because last matching rule wins.
            if cap != rule.cap {
                continue;
            }
            if rule.act != Action::Allow && rule.act != Action::Deny {
                continue;
            }
            if let Pattern::Cidr(ref pattern) = rule.pat {
                if port >= pattern.port[0]
                    && (pattern.port[1] == pattern.port[0] || port <= pattern.port[1])
                    && pattern.addr.contains(addr)
                {
                    if rule.act == Action::Allow {
                        return Action::Allow;
                    } else if self.filter_ip(cap, &addr, port) {
                        // Check filter to determine whether violation is to be reported.
                        return Action::Filter;
                    } else {
                        return Action::Deny;
                    }
                }
            }
        }

        // If no specific rule is found, return based on capability being enabled or not.
        if self.enabled(cap) {
            Action::Deny
        } else {
            Action::Allow
        }
    }

    /// Check UNIX socket for access.
    pub fn check_unix(&self, cap: Capability, path: &str) -> Action {
        // First, see if there's a matching allow or deny rule for the path.
        if let Some(action) = self.match_action(cap, path) {
            if action == Action::Allow {
                return Action::Allow;
            }

            // If the action is Deny, then we must check if it's filtered.
            if self.filter_path(cap, path) {
                return Action::Filter;
            }
            return Action::Deny;
        }

        // If no specific rule is found, return based on capability being enabled or not.
        if self.enabled(cap) {
            Action::Deny
        } else {
            Action::Allow
        }
    }

    /// Check path for access.
    pub fn check_path(&self, cap: Capability, path: &str) -> Action {
        // First, see if there's a matching allow or deny rule for the path.
        if let Some(action) = self.match_action(cap, path) {
            if action == Action::Allow {
                return Action::Allow;
            }

            // If the action is Deny, then we must check if it's filtered.
            if self.filter_path(cap, path) {
                return Action::Filter;
            }
            return Action::Deny;
        }

        // If no specific rule is found, return based on capability being enabled or not.
        if self.enabled(cap) {
            Action::Deny
        } else {
            Action::Allow
        }
    }

    /// Check exec for kill.
    pub fn check_exec(&self, path: &str) -> Action {
        for rule in self.list.iter().rev() {
            // rev() because last matching rule wins.
            if rule.cap != Capability::CAP_EXEC || rule.act != Action::Kill {
                continue;
            }

            if let Pattern::Glob(ref pattern) = rule.pat {
                if glob_match(pattern, path) {
                    return Action::Kill;
                }
            }
        }
        Action::Allow
    }

    /// Find a matching action (Allow or Deny) for the given path.
    fn match_action(&self, cap: Capability, path: &str) -> Option<Action> {
        self.list
            .iter()
            .rev() // rev() because last matching rule wins.
            .filter(|rule| {
                rule.cap == cap && (rule.act == Action::Allow || rule.act == Action::Deny)
            })
            .find_map(|rule| {
                if let Pattern::Glob(ref pattern) = rule.pat {
                    if glob_match(pattern, path) {
                        return Some(rule.act);
                    }
                }
                None
            })
    }

    /// Check if the ip address with the given port is filtered.
    fn filter_ip(&self, cap: Capability, addr: &IpAddr, port: u16) -> bool {
        self.list
            .iter()
            .rev() // rev() because last matching rule wins.
            .filter(|filter| filter.act == Action::Filter && filter.cap == cap)
            .any(|filter| {
                if let Pattern::Cidr(ref pattern) = filter.pat {
                    port >= pattern.port[0] && (pattern.port[1] != pattern.port[0] && port <= pattern.port[1]) && pattern.addr.contains(*addr)
                } else {
                    false
                }
            })
    }

    /// Check if the path is filtered.
    fn filter_path(&self, cap: Capability, path: &str) -> bool {
        self.list
            .iter()
            .rev() // rev() because last matching rule wins.
            .filter(|filter| filter.act == Action::Filter && filter.cap == cap)
            .any(|filter| {
                if let Pattern::Glob(ref pattern) = filter.pat {
                    glob_match(pattern, path)
                } else {
                    false
                }
            })
    }

    /// Check if there are any patterns in `exec/kill` list.
    pub fn has_exec_kill(&self) -> bool {
        for rule in &self.list {
            if rule.cap != Capability::CAP_EXEC || rule.act != Action::Kill {
                continue;
            }
            return true;
        }
        false
    }

    /// Get the process ID of the sydbox execve child.
    pub fn get_child_pid(&mut self) -> Pid {
        Pid::from_raw(self.cpid)
    }

    /// Set the process ID of the sydbox execve child.
    pub fn set_child_pid(&mut self, pid: Pid) {
        self.cpid = pid.as_raw();
    }

    /// Get root
    pub fn get_root(&self) -> bool {
        self.root
    }

    /// If root is set, all id system calls return 0 in the sandbox.
    pub fn set_root(&mut self, on: bool) {
        self.root = on
    }

    /// Return true if the sandboxing is enabled for the given capability.
    pub fn enabled(&self, cap: Capability) -> bool {
        self.state.contains(cap)
    }

    /// Lock sandbox.
    pub fn lock(&mut self) {
        self.lock = LockState::Set
    }

    /// Returns true if the sandbox is locked.
    pub fn locked(&self) -> bool {
        self.lock == LockState::Set
    }

    /// Returns true if the sandbox is locked for the given process ID.
    pub fn locked_for_pid(&self, pid: nix::libc::pid_t) -> bool {
        match self.lock {
            LockState::Set => true,
            LockState::Exec => self.cpid != 0 && pid != self.cpid,
            _ => false,
        }
    }

    /// Returns true if successful bind addresses should be allowed for subsequent connect calls.
    pub fn allow_successful_bind(&self) -> bool {
        self.flags.contains(Flag::FL_ALLOW_SUCCESSFUL_BIND)
    }

    /// Returns true if unsupported socket families should be allowed.
    pub fn allow_unsupported_socket_families(&self) -> bool {
        self.flags
            .contains(Flag::FL_ALLOW_UNSUPPORTED_SOCKET_FAMILIES)
    }

    /// Inserts an IP to the list of addresses with zero port for bind.
    pub fn insert_bind0(&mut self, tgid: Pid, ip: IpAddr) {
        self.bind.insert((tgid, ip))
    }

    /// Removes an IP from the list of addresses with zero port for bind.
    pub fn remove_bind0(&mut self, tgid: Pid, ip: IpAddr) {
        self.bind.remove((tgid, ip))
    }

    /// Checks if the list of addresses with zero port for bind include the given IP.
    pub fn contains_bind0(&mut self, tgid: Pid, ip: IpAddr) -> bool {
        self.bind.get((tgid, ip))
    }

    /// Returns a new sandbox in default state.
    pub fn new() -> Self {
        Sandbox::default()
    }
}

#[cfg(test)]
mod tests {
    use std::io::Result as IOResult;

    use super::*;

    #[test]
    fn sandbox_config_api() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        sandbox.config("")?;
        sandbox.config("3")?;
        assert!(sandbox.config("2").is_err(), "{sandbox}");
        assert!(sandbox.config("1").is_err(), "{sandbox}");
        assert!(sandbox.config("0").is_err(), "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_sandbox_read() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert!(!sandbox.state.contains(Capability::CAP_READ));
        assert!(sandbox.config("sandbox/read?").is_err());
        sandbox.config("sandbox/read:on")?;
        assert!(sandbox.state.contains(Capability::CAP_READ));
        assert!(sandbox.config("sandbox/read?").is_ok());
        sandbox.config("sandbox/read:off")?;
        assert!(!sandbox.state.contains(Capability::CAP_READ));
        assert!(sandbox.config("sandbox/read?").is_err());

        Ok(())
    }

    #[test]
    fn sandbox_config_sandbox_stat() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert!(!sandbox.state.contains(Capability::CAP_STAT));
        assert!(sandbox.config("sandbox/stat?").is_err());
        sandbox.config("sandbox/stat:on")?;
        assert!(sandbox.state.contains(Capability::CAP_STAT));
        assert!(sandbox.config("sandbox/stat?").is_ok());
        sandbox.config("sandbox/stat:off")?;
        assert!(!sandbox.state.contains(Capability::CAP_STAT));
        assert!(sandbox.config("sandbox/stat?").is_err());

        Ok(())
    }

    #[test]
    fn sandbox_config_sandbox_write() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert!(sandbox.state.contains(Capability::CAP_WRITE));
        assert!(sandbox.config("sandbox/write?").is_ok());
        sandbox.config("sandbox/write:off")?;
        assert!(!sandbox.state.contains(Capability::CAP_WRITE));
        assert!(sandbox.config("sandbox/write?").is_err());
        sandbox.config("sandbox/write:on")?;
        assert!(sandbox.state.contains(Capability::CAP_WRITE));
        assert!(sandbox.config("sandbox/write?").is_ok());
        Ok(())
    }

    #[test]
    fn sandbox_config_sandbox_exec() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert!(!sandbox.state.contains(Capability::CAP_EXEC));
        assert!(sandbox.config("sandbox/exec?").is_err());
        sandbox.config("sandbox/exec:on")?;
        assert!(sandbox.state.contains(Capability::CAP_EXEC));
        assert!(sandbox.config("sandbox/exec?").is_ok());
        sandbox.config("sandbox/exec:off")?;
        assert!(!sandbox.state.contains(Capability::CAP_EXEC));
        assert!(sandbox.config("sandbox/exec?").is_err());
        Ok(())
    }

    #[test]
    fn sandbox_config_sandbox_network() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert!(sandbox.state.contains(Capability::CAP_BIND));
        assert!(sandbox.state.contains(Capability::CAP_CONNECT));
        assert!(sandbox.config("sandbox/net?").is_ok());
        sandbox.config("sandbox/net:off")?;
        assert!(!sandbox.state.contains(Capability::CAP_BIND));
        assert!(!sandbox.state.contains(Capability::CAP_CONNECT));
        assert!(sandbox.config("sandbox/net?").is_err());
        sandbox.config("sandbox/net:on")?;
        assert!(sandbox.state.contains(Capability::CAP_BIND));
        assert!(sandbox.state.contains(Capability::CAP_CONNECT));
        assert!(sandbox.config("sandbox/net?").is_ok());

        sandbox.config("sandbox/net:off")?;
        sandbox.config("sandbox/net:bind")?;
        assert!(sandbox.state.contains(Capability::CAP_BIND));
        assert!(!sandbox.state.contains(Capability::CAP_CONNECT));
        assert!(sandbox.config("sandbox/net?").is_ok());

        sandbox.config("sandbox/net:off")?;
        sandbox.config("sandbox/net:connect")?;
        assert!(sandbox.state.contains(Capability::CAP_CONNECT));
        assert!(!sandbox.state.contains(Capability::CAP_BIND));
        assert!(sandbox.config("sandbox/net?").is_ok());

        Ok(())
    }

    #[test]
    fn sandbox_config_lock() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert!(sandbox.lock == LockState::Exec);
        sandbox.config("lock:off")?;
        assert!(sandbox.lock == LockState::Unset);
        sandbox.config("lock:exec")?;
        assert!(sandbox.lock == LockState::Exec);
        sandbox.config("lock:on")?;
        assert!(sandbox.lock == LockState::Set);
        Ok(())
    }

    #[test]
    fn sandbox_config_trace_allow() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert!(sandbox.flags.contains(Flag::FL_ALLOW_SUCCESSFUL_BIND));
        sandbox.config("trace/allow_successful_bind:false")?;
        assert!(!sandbox.flags.contains(Flag::FL_ALLOW_SUCCESSFUL_BIND));
        sandbox.config("trace/allow_successful_bind:true")?;
        assert!(sandbox.flags.contains(Flag::FL_ALLOW_SUCCESSFUL_BIND));

        assert!(sandbox
            .flags
            .contains(Flag::FL_ALLOW_UNSUPPORTED_SOCKET_FAMILIES));
        sandbox.config("trace/allow_unsupported_socket_families:false")?;
        assert!(!sandbox
            .flags
            .contains(Flag::FL_ALLOW_UNSUPPORTED_SOCKET_FAMILIES));
        sandbox.config("trace/allow_unsupported_socket_families:true")?;
        assert!(sandbox
            .flags
            .contains(Flag::FL_ALLOW_UNSUPPORTED_SOCKET_FAMILIES));

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_01() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("allowlist/read+/usr/**")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("allowlist/read-/usr/**")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_02() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("allowlist/write+/usr/**")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("allowlist/write-/usr/**")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_03() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("allowlist/exec+/usr/**")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("allowlist/exec-/usr/**")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_04() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("allowlist/net/bind+/usr/**")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("allowlist/net/bind-/usr/**")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_05() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("allowlist/net/connect+/usr/**")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("allowlist/net/connect-/usr/**")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_06() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("allowlist/net/bind+/usr/**")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("allowlist/net/bind-/usr/**")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_07() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("allowlist/net/connect+/usr/**")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("allowlist/net/connect-/usr/**")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_08() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("allowlist/net/bind+127.0.0.0/8@1024-65535")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("allowlist/net/bind-127.0.0.0/8@1024-65535")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_09() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("allowlist/net/connect+127.0.0.0/8@1024-65535")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("allowlist/net/connect-127.0.0.0/8@1024-65535")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_10() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("allowlist/net/bind+::1/8@1024-65535")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("allowlist/net/bind-::1/8@1024-65535")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_11() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("allowlist/net/connect+::1/8@1024-65535")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("allowlist/net/connect-::1/8@1024-65535")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_12() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("denylist/read+/usr/**")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("denylist/read-/usr/**")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_13() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("denylist/write+/usr/**")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("denylist/write-/usr/**")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_14() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("denylist/exec+/usr/**")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("denylist/exec-/usr/**")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_15() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("denylist/net/bind+/usr/**")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("denylist/net/bind-/usr/**")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_16() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("denylist/net/connect+/usr/**")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("denylist/net/connect-/usr/**")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_17() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("denylist/net/bind+/usr/**")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("denylist/net/bind-/usr/**")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_18() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("denylist/net/connect+/usr/**")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("denylist/net/connect-/usr/**")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_19() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("denylist/net/bind+127.0.0.0/8@1024-65535")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("denylist/net/bind-127.0.0.0/8@1024-65535")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_20() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("denylist/net/connect+127.0.0.0/8@1024-65535")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("denylist/net/connect-127.0.0.0/8@1024-65535")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_21() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("denylist/net/bind+::1/8@1024-65535")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("denylist/net/bind-::1/8@1024-65535")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_22() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("denylist/net/connect+::1/8@1024-65535")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("denylist/net/connect-::1/8@1024-65535")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_23() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("filter/read+/usr/**")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("filter/read-/usr/**")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_24() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("filter/write+/usr/**")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("filter/write-/usr/**")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_25() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("filter/exec+/usr/**")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("filter/exec-/usr/**")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_26() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+/usr/**")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("filter/net/bind-/usr/**")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_27() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+/usr/**")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("filter/net/connect-/usr/**")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_28() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+/usr/**")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("filter/net/bind-/usr/**")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_29() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+/usr/**")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("filter/net/connect-/usr/**")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_30() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+127.0.0.0/8@1024-65535")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("filter/net/bind-127.0.0.0/8@1024-65535")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_31() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+127.0.0.0/8@1024-65535")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("filter/net/connect-127.0.0.0/8@1024-65535")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_32() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+::1/8@1024-65535")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("filter/net/bind-::1/8@1024-65535")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_33() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+::1/8@1024-65535")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("filter/net/connect-::1/8@1024-65535")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_34() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("exec/kill+/usr/**")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("exec/kill-/usr/**")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_35() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("allowlist/net/bind+LOOPBACK@0")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("allowlist/net/bind-LOOPBACK@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_36() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("allowlist/net/connect+LOOPBACK@0")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("allowlist/net/connect-LOOPBACK@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_37() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("denylist/net/bind+LOOPBACK@0")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("denylist/net/bind-LOOPBACK@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_38() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("denylist/net/connect+LOOPBACK@0")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("denylist/net/connect-LOOPBACK@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_39() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+LOOPBACK@0")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("filter/net/bind-LOOPBACK@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_40() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+LOOPBACK@0")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("filter/net/connect-LOOPBACK@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_41() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("allowlist/net/bind+LOOPBACK6@0")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("allowlist/net/bind-LOOPBACK6@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_42() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("allowlist/net/connect+LOOPBACK6@0")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("allowlist/net/connect-LOOPBACK6@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_43() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("denylist/net/bind+LOOPBACK6@0")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("denylist/net/bind-LOOPBACK6@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_44() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("denylist/net/connect+LOOPBACK6@0")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("denylist/net/connect-LOOPBACK6@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_45() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+LOOPBACK6@0")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("filter/net/bind-LOOPBACK6@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_46() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+LOOPBACK6@0")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("filter/net/connect-LOOPBACK6@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_47() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("allowlist/net/bind+LOCAL@0")?;
        assert_eq!(sandbox.list.len(), 4, "{sandbox}");
        sandbox.config("allowlist/net/bind-LOCAL@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_48() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("allowlist/net/connect+LOCAL@0")?;
        assert_eq!(sandbox.list.len(), 4, "{sandbox}");
        sandbox.config("allowlist/net/connect-LOCAL@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_49() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("denylist/net/bind+LOCAL@0")?;
        assert_eq!(sandbox.list.len(), 4, "{sandbox}");
        sandbox.config("denylist/net/bind-LOCAL@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_50() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("denylist/net/connect+LOCAL@0")?;
        assert_eq!(sandbox.list.len(), 4, "{sandbox}");
        sandbox.config("denylist/net/connect-LOCAL@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_51() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+LOCAL@0")?;
        assert_eq!(sandbox.list.len(), 4, "{sandbox}");
        sandbox.config("filter/net/bind-LOCAL@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_52() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+LOCAL@0")?;
        assert_eq!(sandbox.list.len(), 4, "{sandbox}");
        sandbox.config("filter/net/connect-LOCAL@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_53() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("allowlist/net/bind+LOCAL6@0")?;
        assert_eq!(sandbox.list.len(), 4, "{sandbox}");
        sandbox.config("allowlist/net/bind-LOCAL6@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_54() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("allowlist/net/connect+LOCAL6@0")?;
        assert_eq!(sandbox.list.len(), 4, "{sandbox}");
        sandbox.config("allowlist/net/connect-LOCAL6@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_55() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("denylist/net/bind+LOCAL6@0")?;
        assert_eq!(sandbox.list.len(), 4, "{sandbox}");
        sandbox.config("denylist/net/bind-LOCAL6@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_56() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("denylist/net/connect+LOCAL6@0")?;
        assert_eq!(sandbox.list.len(), 4, "{sandbox}");
        sandbox.config("denylist/net/connect-LOCAL6@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_57() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+LOCAL6@0")?;
        assert_eq!(sandbox.list.len(), 4, "{sandbox}");
        sandbox.config("filter/net/bind-LOCAL6@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_58() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+LOCAL6@0")?;
        assert_eq!(sandbox.list.len(), 4, "{sandbox}");
        sandbox.config("filter/net/connect-LOCAL6@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_59() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        for _ in 0..2 {
            sandbox.config("allowlist/net/bind+LOOPBACK@0")?;
        }
        assert_eq!(sandbox.list.len(), 2, "{sandbox}");
        sandbox.config("allowlist/net/bind-LOOPBACK@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_60() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        for _ in 0..3 {
            sandbox.config("allowlist/net/connect+LOOPBACK@0")?;
        }
        assert_eq!(sandbox.list.len(), 3, "{sandbox}");
        sandbox.config("allowlist/net/connect-LOOPBACK@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_61() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        for _ in 0..4 {
            sandbox.config("denylist/net/bind+LOOPBACK@0")?;
        }
        assert_eq!(sandbox.list.len(), 4, "{sandbox}");
        sandbox.config("denylist/net/bind-LOOPBACK@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_62() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        for _ in 0..5 {
            sandbox.config("denylist/net/connect+LOOPBACK@0")?;
        }
        assert_eq!(sandbox.list.len(), 5, "{sandbox}");
        sandbox.config("denylist/net/connect-LOOPBACK@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_63() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        for _ in 0..6 {
            sandbox.config("filter/net/bind+LOOPBACK@0")?;
        }
        assert_eq!(sandbox.list.len(), 6, "{sandbox}");
        sandbox.config("filter/net/bind-LOOPBACK@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_64() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        for _ in 0..7 {
            sandbox.config("filter/net/connect+LOOPBACK@0")?;
        }
        assert_eq!(sandbox.list.len(), 7, "{sandbox}");
        sandbox.config("filter/net/connect-LOOPBACK@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_65() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        for _ in 0..8 {
            sandbox.config("allowlist/net/bind+LOOPBACK6@0")?;
        }
        assert_eq!(sandbox.list.len(), 8, "{sandbox}");
        sandbox.config("allowlist/net/bind-LOOPBACK6@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_66() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        for _ in 0..9 {
            sandbox.config("allowlist/net/connect+LOOPBACK6@0")?;
        }
        assert_eq!(sandbox.list.len(), 9, "{sandbox}");
        sandbox.config("allowlist/net/connect-LOOPBACK6@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_67() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        for _ in 0..10 {
            sandbox.config("denylist/net/bind+LOOPBACK6@0")?;
        }
        assert_eq!(sandbox.list.len(), 10, "{sandbox}");
        sandbox.config("denylist/net/bind-LOOPBACK6@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_68() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        for _ in 0..11 {
            sandbox.config("denylist/net/connect+LOOPBACK6@0")?;
        }
        assert_eq!(sandbox.list.len(), 11, "{sandbox}");
        sandbox.config("denylist/net/connect-LOOPBACK6@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_69() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        for _ in 0..12 {
            sandbox.config("filter/net/bind+LOOPBACK6@0")?;
        }
        assert_eq!(sandbox.list.len(), 12, "{sandbox}");
        sandbox.config("filter/net/bind-LOOPBACK6@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_70() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        for _ in 0..13 {
            sandbox.config("filter/net/connect+LOOPBACK6@0")?;
        }
        assert_eq!(sandbox.list.len(), 13, "{sandbox}");
        sandbox.config("filter/net/connect-LOOPBACK6@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_71() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        for _ in 0..14 {
            sandbox.config("allowlist/net/bind+LOCAL@0")?;
        }
        assert_eq!(sandbox.list.len(), 4 * 14, "{sandbox}");
        sandbox.config("allowlist/net/bind-LOCAL@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_72() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        for _ in 0..15 {
            sandbox.config("allowlist/net/connect+LOCAL@0")?;
        }
        assert_eq!(sandbox.list.len(), 4 * 15, "{sandbox}");
        sandbox.config("allowlist/net/connect-LOCAL@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_73() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        for _ in 0..16 {
            sandbox.config("denylist/net/bind+LOCAL@0")?;
        }
        assert_eq!(sandbox.list.len(), 4 * 16, "{sandbox}");
        sandbox.config("denylist/net/bind-LOCAL@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_74() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        for _ in 0..17 {
            sandbox.config("denylist/net/connect+LOCAL@0")?;
        }
        assert_eq!(sandbox.list.len(), 4 * 17, "{sandbox}");
        sandbox.config("denylist/net/connect-LOCAL@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_75() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        for _ in 0..18 {
            sandbox.config("filter/net/bind+LOCAL@0")?;
        }
        assert_eq!(sandbox.list.len(), 4 * 18, "{sandbox}");
        sandbox.config("filter/net/bind-LOCAL@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_76() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        for _ in 0..19 {
            sandbox.config("filter/net/connect+LOCAL@0")?;
        }
        assert_eq!(sandbox.list.len(), 4 * 19, "{sandbox}");
        sandbox.config("filter/net/connect-LOCAL@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_77() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        for _ in 0..20 {
            sandbox.config("allowlist/net/bind+LOCAL6@0")?;
        }
        assert_eq!(sandbox.list.len(), 4 * 20, "{sandbox}");
        sandbox.config("allowlist/net/bind-LOCAL6@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_78() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        for _ in 0..21 {
            sandbox.config("allowlist/net/connect+LOCAL6@0")?;
        }
        assert_eq!(sandbox.list.len(), 4 * 21, "{sandbox}");
        sandbox.config("allowlist/net/connect-LOCAL6@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_79() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        for _ in 0..22 {
            sandbox.config("denylist/net/bind+LOCAL6@0")?;
        }
        assert_eq!(sandbox.list.len(), 4 * 22, "{sandbox}");
        sandbox.config("denylist/net/bind-LOCAL6@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_80() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        for _ in 0..23 {
            sandbox.config("denylist/net/connect+LOCAL6@0")?;
        }
        assert_eq!(sandbox.list.len(), 4 * 23, "{sandbox}");
        sandbox.config("denylist/net/connect-LOCAL6@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_81() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        for _ in 0..24 {
            sandbox.config("filter/net/bind+LOCAL6@0")?;
        }
        assert_eq!(sandbox.list.len(), 4 * 24, "{sandbox}");
        sandbox.config("filter/net/bind-LOCAL6@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_82() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        for _ in 0..25 {
            sandbox.config("filter/net/connect+LOCAL6@0")?;
        }
        assert_eq!(sandbox.list.len(), 4 * 25, "{sandbox}");
        sandbox.config("filter/net/connect-LOCAL6@0")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_83() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("allowlist/stat+/usr/**")?;
        assert_eq!(sandbox.list.len(), 1, "{sandbox}");
        sandbox.config("allowlist/stat-/usr/**")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_84() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("denylist/stat+/usr/**")?;
        sandbox.config("denylist/stat+/usr/**")?;
        assert_eq!(sandbox.list.len(), 2, "{sandbox}");
        sandbox.config("denylist/stat-/usr/**")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_85() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        sandbox.config("filter/stat+/usr/**")?;
        sandbox.config("filter/stat+/usr/**")?;
        sandbox.config("filter/stat+/usr/**")?;
        assert_eq!(sandbox.list.len(), 3, "{sandbox}");
        sandbox.config("filter/stat-/usr/**")?;
        assert_eq!(sandbox.list.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_glob_doublestar_does_not_match_basename() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        sandbox.config("sandbox/read:on")?;

        sandbox.config("allowlist/read+/dev/**")?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_READ, "/dev"),
            None,
            "/dev =~ /dev/**, {sandbox}"
        );

        Ok(())
    }

    #[test]
    fn sandbox_glob_doublestar_matches_basename_with_slash() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        sandbox.config("sandbox/read:on")?;

        sandbox.config("allowlist/read+/dev/**")?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_READ, "/dev/"),
            Some(Action::Allow),
            "/dev/ !~ /dev/**, {sandbox}"
        );

        Ok(())
    }

    #[test]
    fn sandbox_glob_doublestar_matches_pathname() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        sandbox.config("sandbox/read:on")?;

        sandbox.config("allowlist/read+/dev/**")?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_READ, "/dev/null"),
            Some(Action::Allow),
            "/dev/null !~ /dev/**, {sandbox}"
        );

        Ok(())
    }

    #[test]
    fn sandbox_glob_triplestar_matches_basename() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        sandbox.config("sandbox/read:on")?;

        sandbox.config("allowlist/read+/dev/***")?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_READ, "/dev"),
            Some(Action::Allow),
            "/dev =~ /dev/***, {sandbox}"
        );

        Ok(())
    }

    #[test]
    fn sandbox_glob_triplestar_matches_basename_with_slash() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        sandbox.config("sandbox/read:on")?;

        sandbox.config("allowlist/read+/dev/***")?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_READ, "/dev/"),
            Some(Action::Allow),
            "/dev/ !~ /dev/***, {sandbox}"
        );

        Ok(())
    }

    #[test]
    fn sandbox_glob_triplestar_matches_pathname() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        sandbox.config("sandbox/read:on")?;

        sandbox.config("allowlist/read+/dev/***")?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_READ, "/dev/null"),
            Some(Action::Allow),
            "/dev/null !~ /dev/***, {sandbox}"
        );

        Ok(())
    }
}
