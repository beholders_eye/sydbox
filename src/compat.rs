//
// syd: application sandbox
// src/compat.rs: Compatibility code for different libcs
//
// Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{
    ffi::{OsStr, OsString},
    os::{fd::RawFd, unix::ffi::OsStrExt},
};

use nix::errno::Errno;
use nonempty::NonEmpty;

/// This structure represents the Linux data structure `struct statx_timestamp`
#[repr(C)]
pub struct statx_timestamp {
    tv_sec: i64,
    tv_nsec: i32,
    reserved: i32,
}

/// This structure represents the Linux data structure `struct statx`
#[repr(C)]
pub struct statx {
    stx_mask: u32,       // What results were written [uncond]
    stx_blksize: u32,    // Preferred general I/O size [uncond]
    stx_attributes: u64, // Flags conveying information about the file [uncond]

    stx_nlink: u32, // Number of hard links
    stx_uid: u32,   // User ID of owner
    stx_gid: u32,   // Group ID of owner
    stx_mode: u16,  // File mode
    reserved0: [u16; 1],

    stx_ino: u64,             // Inode number
    stx_size: u64,            // File size
    stx_blocks: u64,          // Number of 512-byte blocks allocated
    stx_attributes_mask: u64, // Mask to show what's supported in stx_attributes

    stx_atime: statx_timestamp, // Last access time
    stx_btime: statx_timestamp, // File creation time
    stx_ctime: statx_timestamp, // Last attribute change time
    stx_mtime: statx_timestamp, // Last data modification time

    stx_rdev_major: u32, // Device ID of special file [if bdev/cdev]
    stx_rdev_minor: u32,
    stx_dev_major: u32, // ID of device containing file [uncond]
    stx_dev_minor: u32,

    stx_mnt_id: u64,
    stx_dio_mem_align: u32,    // Memory buffer alignment for direct I/O
    stx_dio_offset_align: u32, // File offset alignment for direct I/O

    reserved2: [u64; 12], // Spare space for future expansion
}

/// This struct represents a directory entry.
#[derive(Clone)]
pub struct DirEntry {
    /// The `dirent64` structure.
    pub dirent: Box<[u8]>,

    /// Size of the file name, in bytes.
    pub namelen: usize,
}

impl std::fmt::Debug for DirEntry {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_tuple("DirEntry").field(&self.name()).finish()
    }
}

impl DirEntry {
    /// Return the name of the directory entry.
    pub fn name(&self) -> OsString {
        // SAFETY: We trust self.dirent points to a valid `dirent64` structure.
        OsStr::from_bytes(unsafe { self.name_bytes() }).to_os_string()
    }

    unsafe fn name_bytes(&self) -> &[u8] {
        let dirent: *const nix::libc::dirent64 = Box::as_ref(&self.dirent).as_ptr().cast();
        let d_name: *const nix::libc::c_char = (*dirent).d_name.as_ptr();
        std::slice::from_raw_parts(d_name.cast(), self.namelen)
    }
}

/// Represents an optional list of directory entries as returned by `getdents64` system call.
type Directory = Option<NonEmpty<DirEntry>>;

/// Retrieve directory entries from an open directory file descriptor.
///
/// # Parameters
///
/// - `fd`: The open directory file descriptor.
///
/// # Returns
///
/// A `Directory` containing the directory entries.
/// The Option is None when there are no more entries.
///
/// # Safety
///
/// This function calls the `getdents64` system call directly which is an unsafe function.
/// Ensure that `fd` is a valid open directory file descriptor to avoid undefined behavior.
pub fn getdents(fd: RawFd, count: usize) -> Result<Directory, Errno> {
    let mut buffer = vec![0u8; count];

    let size = loop {
        // SAFETY: In libc, we trust.
        match unsafe { getdents64(fd, buffer.as_mut_ptr().cast(), buffer.len()) } {
            0 => {
                return Ok(None);
            }
            error if error < 0 => {
                #[allow(clippy::arithmetic_side_effects)]
                #[allow(clippy::cast_possible_truncation)]
                let error = -error as i32;
                if error == Errno::EINTR as i32 {
                    continue;
                }
                return Err(Errno::from_i32(error));
            }
            size =>
            {
                #[allow(clippy::cast_possible_truncation)]
                #[allow(clippy::cast_sign_loss)]
                break size as usize
            }
        }
    };

    // Parsing the buffer to extract DirEntry structures
    let mut entries = Vec::new();
    let mut offset = 0;
    while offset < size {
        // SAFETY: In libc, we trust.
        unsafe {
            let dirent: *const nix::libc::dirent64 = buffer.as_ptr().add(offset).cast();
            let d_reclen = (*dirent).d_reclen as usize;

            // Copy the bytes of the `dirent64` structure from the buffer to a
            // memory owned by `DirEntry`.
            let entry = std::slice::from_raw_parts(dirent.cast(), d_reclen)
                .to_owned()
                .into_boxed_slice();

            entries.push(DirEntry {
                dirent: entry,
                namelen: nix::libc::strlen((*dirent).d_name.as_ptr()),
            });

            offset = offset.saturating_add(d_reclen);
        }
    }

    if entries.is_empty() {
        Ok(None)
    } else {
        #[allow(clippy::disallowed_methods)]
        Ok(Some(NonEmpty::from_vec(entries).unwrap()))
    }
}

/// Wrapper for the `getdents64` syscall
#[inline(always)]
unsafe fn getdents64(
    fd: nix::libc::c_int,
    buf: *mut nix::libc::c_void,
    bytes: nix::libc::size_t,
) -> nix::libc::c_long {
    nix::libc::syscall(nix::libc::SYS_getdents64, fd, buf, bytes)
}
