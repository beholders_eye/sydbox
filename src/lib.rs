//
// syd: application sandbox
// src/lib.rs: Common utility functions
//
// Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

//! syd main module

// We like clean and simple code with documentation.
// Keep in sync with main.rs.
#![deny(missing_docs)]
#![deny(clippy::allow_attributes_without_reason)]
#![deny(clippy::arithmetic_side_effects)]
#![deny(clippy::as_ptr_cast_mut)]
#![deny(clippy::as_underscore)]
#![deny(clippy::assertions_on_result_states)]
#![deny(clippy::borrow_as_ptr)]
#![deny(clippy::branches_sharing_code)]
#![deny(clippy::case_sensitive_file_extension_comparisons)]
#![deny(clippy::cast_lossless)]
#![deny(clippy::cast_possible_truncation)]
#![deny(clippy::cast_possible_wrap)]
#![deny(clippy::cast_precision_loss)]
#![deny(clippy::cast_ptr_alignment)]
#![deny(clippy::cast_sign_loss)]
#![deny(clippy::checked_conversions)]
#![deny(clippy::clear_with_drain)]
#![deny(clippy::clone_on_ref_ptr)]
#![deny(clippy::cloned_instead_of_copied)]
#![deny(clippy::cognitive_complexity)]
#![deny(clippy::collection_is_never_read)]
#![deny(clippy::copy_iterator)]
#![deny(clippy::create_dir)]
#![deny(clippy::dbg_macro)]
#![deny(clippy::debug_assert_with_mut_call)]
#![deny(clippy::decimal_literal_representation)]
#![deny(clippy::default_trait_access)]
#![deny(clippy::default_union_representation)]
#![deny(clippy::derive_partial_eq_without_eq)]
#![deny(clippy::doc_link_with_quotes)]
#![deny(clippy::doc_markdown)]
#![deny(clippy::explicit_into_iter_loop)]
#![deny(clippy::explicit_iter_loop)]
#![deny(clippy::fallible_impl_from)]
#![deny(clippy::missing_safety_doc)]
#![deny(clippy::undocumented_unsafe_blocks)]

/// Compatibility code for different libcs
pub mod compat;
/// Static configuration, edit & recompile!
pub mod config;
/// Filesystem utilities
pub mod fs;
/// Secure computing hooks
#[allow(clippy::as_ptr_cast_mut)]
#[allow(clippy::cast_sign_loss)]
#[allow(clippy::undocumented_unsafe_blocks)]
pub mod hook;
/// Simple logging on standard error using JSON lines
pub mod log;
/// /proc utilities
pub mod proc;
/// Sandbox configuration
pub mod sandbox;

/* Utilities */
use nix::{
    errno::Errno,
    sys::signal::{sigaction, SaFlags, SigAction, SigHandler, Signal},
    unistd::{Pid, Uid, User},
};

/// Given a `Uid`, return the user name of the user.
/// On any error conditions, return "nobody".
pub fn get_user_name(uid: Uid) -> String {
    match User::from_uid(uid) {
        Ok(Some(user)) => user.name,
        _ => "nobody".to_string(),
    }
}

/// Given a username, return the home directory of the user.
/// On any error conditions, return "/var/empty".
pub fn get_user_home(username: &str) -> String {
    // Fetch user details.
    match User::from_name(username) {
        Ok(Some(user)) => user.dir.to_string_lossy().to_string(),
        _ => "/var/empty".to_string(),
    }
}

/// Sets the specified signal to be ignored.
///
/// This function utilizes the `sigaction` system call to set the specified signal's action
/// to `SIG_IGN`, effectively causing the process to ignore that signal.
///
/// # Arguments
///
/// * `signal` - The signal number (e.g., `SIGTSTP`, `SIGTTIN`, `SIGTTOU`).
///
/// # Returns
///
/// * `Result<(), Error>` - Returns `Ok(())` if successful, or an error if the operation fails.
///
/// # Example
///
/// ```no_run
/// use nix::sys::signal::SIGTSTP;
///
/// let result = syd::ignore_signal(SIGTSTP);
/// assert!(result.is_ok());
/// ```
pub fn ignore_signal(signal: Signal) -> Result<(), Errno> {
    let sig_action = SigAction::new(
        SigHandler::SigIgn, // Set to ignore
        SaFlags::empty(),
        nix::sys::signal::SigSet::empty(),
    );

    // SAFETY: The unsafe call to `sigaction` is used to set the signal's disposition
    // to "ignore". We're not invoking any handlers or performing any operations that
    // could lead to data races or other undefined behaviors. Hence, it's safe to call
    // in this context.
    unsafe {
        sigaction(signal, &sig_action)
            .map(|_| ())
            .map_err(|_| Errno::last())
    }
}

static PROC_SELF_LEN: usize = b"/proc/self".len();

/// Replaces occurrences of `/proc/self` in the given buffer with the actual path using the process ID.
///
/// When monitoring paths of processes, it's common to encounter `/proc/self`, which is a symbolic
/// link that points to the current process's PID directory in `/proc`. This function resolves
/// `/proc/self` in the buffer to its actual path using the provided process ID (`pid`).
///
/// # Parameters
/// - `buf`: The buffer containing the path string. This buffer will be modified in-place if
///          `/proc/self` is detected.
/// - `pid`: The process ID used to replace `/proc/self`.
///
/// # Returns
///
/// `true` if `/proc/self` was detected and replaced, `false` otherwise.
///
/// # Examples
///
/// ```rust
/// use nix::unistd::Pid;
/// use syd::proc_self;
///
/// let mut path_buf = [0u8; 128];
/// path_buf[..b"/proc/self".len()].copy_from_slice(b"/proc/self");
/// proc_self(Pid::from_raw(12345), &mut path_buf);
/// let path_buf = path_buf
///     .split(|&byte| byte == 0)
///     .next()
///     .unwrap_or(&path_buf);
/// let path = String::from_utf8_lossy(path_buf);
/// assert_eq!(path, "/proc/12345");
/// ```
pub fn proc_self(pid: Pid, buf: &mut [u8]) -> bool {
    if !buf.starts_with(b"/proc/self") {
        return false;
    }

    let proc_pid = format!("/proc/{}", pid);
    let proc_pid_len = proc_pid.len();

    // Ensure we don't go beyond the buffer length.
    let remaining_bytes = buf.len().saturating_sub(PROC_SELF_LEN);
    let max_replace_length = remaining_bytes.saturating_sub(proc_pid_len);

    // Compute the start and end positions separately to avoid arithmetic side effects.
    let start_pos = PROC_SELF_LEN;
    let end_pos = start_pos.saturating_add(max_replace_length);

    let remaining_path: Vec<u8> = buf[start_pos..end_pos].to_vec();

    // Clear the buffer and write the new path.
    buf.fill(0);
    let new_path = [proc_pid.as_bytes(), &remaining_path[..]].concat();
    buf[..new_path.len()].copy_from_slice(&new_path);

    true
}

/// Return system call priority by system call name.
#[inline(always)]
pub fn syscall_priority(name: &str) -> u8 {
    if name.starts_with("open") {
        255
    } else if name.contains("stat") {
        225
    } else if name.starts_with("getdents") {
        200
    } else if name.starts_with("access") {
        150
    } else if name.starts_with("execve") {
        100
    } else if name.starts_with("unlink") {
        75
    } else if name.starts_with("mkdir") {
        50
    } else {
        25
    }
}

#[cfg(test)]
mod tests {
    use nix::unistd::Pid;

    use super::*;

    #[test]
    fn test_proc_self_replace_pid() {
        let mut buf = [0u8; 4096];
        buf[..PROC_SELF_LEN].copy_from_slice(b"/proc/self");
        assert_eq!(proc_self(Pid::from_raw(1234), &mut buf), true);
        assert_eq!(proc_self(Pid::from_raw(0), &mut buf), false);
        let buf = buf.split(|&byte| byte == 0).next().unwrap_or(&buf);
        let path = String::from_utf8_lossy(buf);
        assert_eq!(path, "/proc/1234");
    }

    #[test]
    fn test_proc_self_replace_pid_shorter() {
        let mut buf = [0u8; 4096];
        buf[..PROC_SELF_LEN].copy_from_slice(b"/proc/self");
        assert_eq!(proc_self(Pid::from_raw(1), &mut buf), true);
        assert_eq!(proc_self(Pid::from_raw(1234), &mut buf), false);
        let buf = buf.split(|&byte| byte == 0).next().unwrap_or(&buf);
        let path = String::from_utf8_lossy(buf);
        assert_eq!(path, "/proc/1");
    }

    #[test]
    fn test_proc_self_replace_pid_longer() {
        let mut buf = [0u8; 4096];
        buf[..PROC_SELF_LEN].copy_from_slice(b"/proc/self");
        assert_eq!(proc_self(Pid::from_raw(1234567), &mut buf), true);
        assert_eq!(proc_self(Pid::from_raw(1), &mut buf), false);
        let buf = buf.split(|&byte| byte == 0).next().unwrap_or(&buf);
        let path = String::from_utf8_lossy(buf);
        assert_eq!(path, "/proc/1234567");
    }

    #[test]
    fn test_proc_self_replace_pid_with_path() {
        let mut buf = [0u8; 4096];
        buf[..PROC_SELF_LEN + "/mem".len()].copy_from_slice(b"/proc/self/mem");
        assert_eq!(proc_self(Pid::from_raw(1234), &mut buf), true);
        assert_eq!(proc_self(Pid::from_raw(1), &mut buf), false);
        let buf = buf.split(|&byte| byte == 0).next().unwrap_or(&buf);
        let path = String::from_utf8_lossy(buf);
        assert_eq!(path, "/proc/1234/mem");
    }

    #[test]
    fn test_proc_self_replace_pid_with_path_shorter() {
        let mut buf = [0u8; 4096];
        buf[..PROC_SELF_LEN + "/mem".len()].copy_from_slice(b"/proc/self/mem");
        assert_eq!(proc_self(Pid::from_raw(1), &mut buf), true);
        assert_eq!(proc_self(Pid::from_raw(1234), &mut buf), false);
        let buf = buf.split(|&byte| byte == 0).next().unwrap_or(&buf);
        let path = String::from_utf8_lossy(buf);
        assert_eq!(path, "/proc/1/mem");
    }

    #[test]
    fn test_proc_self_replace_pid_with_path_longer() {
        let mut buf = [0u8; 4096];
        buf[..PROC_SELF_LEN + "/mem".len()].copy_from_slice(b"/proc/self/mem");
        assert_eq!(proc_self(Pid::from_raw(1234567), &mut buf), true);
        assert_eq!(proc_self(Pid::from_raw(1), &mut buf), false);
        let buf = buf.split(|&byte| byte == 0).next().unwrap_or(&buf);
        let path = String::from_utf8_lossy(buf);
        assert_eq!(path, "/proc/1234567/mem");
    }
}
