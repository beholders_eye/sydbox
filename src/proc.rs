//
// syd: application sandbox
// src/proc.rs: /proc utilities
//
// Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{
    fs::File,
    io::{BufRead, BufReader, Read},
};

use nix::{errno::Errno, unistd::Pid};

/// Retrieves the thread group ID (TGID) for the specified process ID (PID).
///
/// This function reads the `/proc/<pid>/status` file and extracts the
/// TGID from the "Tgid:" line.
///
/// # Arguments
///
/// * `pid` - The process ID for which to retrieve the thread group ID.
///
/// # Returns
///
/// * `Ok(Pid)` - The thread group ID as a `Pid` type if found.
/// * `Err(Errno)` - An error, typically:
///     * `Errno::ENOENT` if the "Tgid:" line is not found in the status file.
///     * `Errno::EINVAL` if the TGID is found but cannot be parsed as an integer.
///
/// # Examples
///
/// ```rust
/// use nix::unistd::Pid;
/// use syd::proc::proc_tgid;
///
/// let tgid = proc_tgid(Pid::this());
/// assert!(tgid.is_ok());
/// ```
pub fn proc_tgid(pid: Pid) -> Result<Pid, Errno> {
    let file =
        BufReader::new(File::open(format!("/proc/{pid}/status")).map_err(|_| Errno::last())?);

    for line in file.lines().flatten() {
        if line.starts_with("Tgid:") {
            let tgid_str = line.trim_start_matches("Tgid:").trim();
            let tgid: i32 = tgid_str.parse().map_err(|_| Errno::EINVAL)?;
            return Ok(Pid::from_raw(tgid));
        }
    }

    Err(Errno::ENOENT)
}

/// Retrieves the command line of the specified process ID (PID)
/// concatenated as a single string.
///
/// This function reads the `/proc/<pid>/cmdline` file and concatenates
/// the arguments using spaces. The function takes care of replacing null
/// bytes (`'\0'`) with spaces to format the command line as a readable string.
///
/// # Arguments
///
/// * `pid` - The process ID for which to retrieve the command line.
///
/// # Returns
///
/// * `Ok(String)` - The command line of the process as a single string.
/// * `Err(Errno)` - An error, depending on the issue encountered while reading
///                  or processing the cmdline file.
///
/// # Examples
///
/// ```rust
/// use nix::unistd::Pid;
/// use syd::proc::proc_cmdline;
///
/// let cmdline = proc_cmdline(Pid::this());
/// assert!(cmdline.is_ok());
/// ```
pub fn proc_cmdline(pid: Pid) -> Result<String, Errno> {
    let mut file =
        BufReader::new(File::open(format!("/proc/{pid}/cmdline")).map_err(|_| Errno::last())?);
    let mut data = String::new();
    file.read_to_string(&mut data).map_err(|_| Errno::last())?;

    Ok(data.replace('\0', " ").trim().to_string())
}
