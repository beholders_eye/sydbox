//
// syd: application sandbox
// src/fs.rs: Filesystem utilities
//
// Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
// Based in part upon uutils coreutils package's src/lib/features/fs.rs which is:
//   (c) Joseph Crail <jbcrail@gmail.com>
//   (c) Jian Zeng <anonymousknight96 AT gmail.com>
// Tests base based in part upon gnulib packages' tests/test-canonicalize.c which is:
//   (c) Free Software Foundation, Inc.
//
// SPDX-License-Identifier: GPL-3.0-or-later

//! Set of functions to manage files and symlinks

use std::{
    collections::{HashSet, VecDeque},
    ffi::{OsStr, OsString},
    fs,
    fs::read_dir,
    hash::Hash,
    path::{Component, Path, PathBuf, MAIN_SEPARATOR},
};

use nix::{errno::Errno, NixPath};

/// Information to uniquely identify a file
struct FileInformation(nix::sys::stat::FileStat);

impl FileInformation {
    /// Get information for a given path.
    ///
    /// If `path` points to a symlink and `dereference` is true, information about
    /// the link's target will be returned.
    pub fn from_path(path: impl AsRef<Path>, dereference: bool) -> Result<Self, Errno> {
        let stat = if dereference {
            nix::sys::stat::stat(path.as_ref())
        } else {
            nix::sys::stat::lstat(path.as_ref())
        }
        .map_err(|_| Errno::last())?;
        Ok(Self(stat))
    }
}

impl PartialEq for FileInformation {
    fn eq(&self, other: &Self) -> bool {
        self.0.st_dev == other.0.st_dev && self.0.st_ino == other.0.st_ino
    }
}

impl Eq for FileInformation {}

impl Hash for FileInformation {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.0.st_dev.hash(state);
        self.0.st_ino.hash(state);
    }
}

/// Controls how symbolic links should be handled when canonicalizing a path.
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum MissingHandling {
    /// Return an error if any part of the path is missing.
    Normal,

    /// Resolve symbolic links, ignoring errors on the final component.
    Existing,

    /// Resolve symbolic links, ignoring errors on the non-final components.
    Missing,
}

/// Normalize a path by removing relative information
/// For example, convert 'bar/../foo/bar.txt' => 'foo/bar.txt'
/// copied from `<https://github.com/rust-lang/cargo/blob/2e4cfc2b7d43328b207879228a2ca7d427d188bb/src/cargo/util/paths.rs#L65-L90>`
/// both projects are MIT `<https://github.com/rust-lang/cargo/blob/master/LICENSE-MIT>`
/// for std impl progress see rfc `<https://github.com/rust-lang/rfcs/issues/2208>`
/// replace this once that lands
pub fn normalize_path(path: &Path) -> PathBuf {
    let mut components = path.components().peekable();
    let mut ret = if let Some(c @ Component::Prefix(..)) = components.peek().copied() {
        components.next();
        PathBuf::from(c.as_os_str())
    } else {
        PathBuf::new()
    };

    for component in components {
        match component {
            Component::Prefix(..) => unreachable!(),
            Component::RootDir => {
                ret.push(component.as_os_str());
            }
            Component::CurDir => {}
            Component::ParentDir => {
                ret.pop();
            }
            Component::Normal(c) => {
                ret.push(c);
            }
        }
    }
    ret
}

fn resolve_symlink<P: AsRef<Path>>(path: P) -> Result<Option<PathBuf>, Errno> {
    let result = if fs::symlink_metadata(&path)
        .map_err(|_| Errno::last())?
        .file_type()
        .is_symlink()
    {
        Some(fs::read_link(&path).map_err(|_| Errno::last())?)
    } else {
        None
    };
    Ok(result)
}

enum OwningComponent {
    Prefix(OsString),
    RootDir,
    CurDir,
    ParentDir,
    Normal(OsString),
}

impl OwningComponent {
    fn as_os_str(&self) -> &OsStr {
        match self {
            Self::Prefix(s) => s.as_os_str(),
            Self::RootDir => Component::RootDir.as_os_str(),
            Self::CurDir => Component::CurDir.as_os_str(),
            Self::ParentDir => Component::ParentDir.as_os_str(),
            Self::Normal(s) => s.as_os_str(),
        }
    }
}

impl<'a> From<Component<'a>> for OwningComponent {
    fn from(comp: Component<'a>) -> Self {
        match comp {
            Component::Prefix(_) => Self::Prefix(comp.as_os_str().to_os_string()),
            Component::RootDir => Self::RootDir,
            Component::CurDir => Self::CurDir,
            Component::ParentDir => Self::ParentDir,
            Component::Normal(s) => Self::Normal(s.to_os_string()),
        }
    }
}

/// Return the canonical, absolute form of a path
///
/// This function is a generalization of [`std::fs::canonicalize`] that
/// allows controlling how symbolic links are resolved and how to deal
/// with missing components. It returns the canonical, absolute form of
/// a path.
///
/// The `resolve_symlinks` is a boolean parameter which controls how symbolic links are resolved.
///
/// The `miss_mode` parameter controls how missing path elements are handled
///
/// * [`MissingHandling::Normal`] makes this function behave like
///   [`std::fs::canonicalize`], resolving symbolic links and returning
///   an error if the path does not exist.
/// * [`MissingHandling::Missing`] makes this function ignore non-final
///   components of the path that could not be resolved.
/// * [`MissingHandling::Existing`] makes this function return an error
///   if the final component of the path does not exist.
#[allow(clippy::cognitive_complexity)]
pub fn canonicalize<P: AsRef<Path>>(
    original: P,
    resolve_symlinks: bool,
    miss_mode: MissingHandling,
) -> Result<PathBuf, Errno> {
    const SYMLINKS_TO_LOOK_FOR_LOOPS: i32 = 20;
    let original = original.as_ref();
    let has_to_be_directory =
        (miss_mode == MissingHandling::Normal || miss_mode == MissingHandling::Existing) && {
            let path_str = original.to_string_lossy();
            path_str.ends_with(MAIN_SEPARATOR)
        };
    let original = if original.is_empty() {
        return Err(Errno::ENOENT);
    } else if original.is_absolute() {
        original.to_path_buf()
    } else {
        // relative path passed to canonicalize is not supported.
        return Err(Errno::EINVAL);
    };
    let path = normalize_path(&original);
    let mut parts: VecDeque<OwningComponent> = path.components().map(|part| part.into()).collect();
    let mut result = PathBuf::new();
    let mut followed_symlinks = 0;
    let mut visited_files = HashSet::new();
    while let Some(part) = parts.pop_front() {
        match part {
            OwningComponent::Prefix(s) => {
                result.push(s);
                continue;
            }
            OwningComponent::RootDir | OwningComponent::Normal(..) => {
                result.push(part.as_os_str());
            }
            OwningComponent::CurDir => {}
            OwningComponent::ParentDir => {
                result.pop();
            }
        }
        if !resolve_symlinks {
            continue;
        }
        match resolve_symlink(&result) {
            Ok(Some(link_path)) => {
                for link_part in link_path.components().rev() {
                    parts.push_front(link_part.into());
                }
                if followed_symlinks < SYMLINKS_TO_LOOK_FOR_LOOPS {
                    followed_symlinks = followed_symlinks.saturating_add(1);
                } else {
                    let file_info = FileInformation::from_path(
                        result
                            .parent()
                            .ok_or_else(|| Errno::ENOENT /* no parent directory */)?,
                        false,
                    )?;
                    let mut path_to_follow = PathBuf::new();
                    for part in &parts {
                        path_to_follow.push(part.as_os_str());
                    }
                    if !visited_files.insert((file_info, path_to_follow)) {
                        return Err(Errno::ELOOP);
                    }
                }
                result.pop();
            }
            Err(error) => {
                if miss_mode == MissingHandling::Existing
                    || (miss_mode == MissingHandling::Normal && !parts.is_empty())
                {
                    return Err(error);
                }
            }
            _ => {}
        }
    }
    // raise Not a directory if required
    match miss_mode {
        MissingHandling::Existing => {
            if has_to_be_directory {
                read_dir(&result).map_err(|_| Errno::last())?;
            }
        }
        MissingHandling::Normal => {
            if result.exists() {
                if has_to_be_directory {
                    read_dir(&result).map_err(|_| Errno::last())?;
                }
            } else if let Some(parent) = result.parent() {
                read_dir(parent).map_err(|_| Errno::last())?;
            }
        }
        MissingHandling::Missing => {}
    }
    Ok(result)
}

#[cfg(test)]
mod tests {
    use std::{fs::OpenOptions, os::unix::fs::OpenOptionsExt};

    use super::{MissingHandling::*, *};

    type TestResult = Result<(), Box<dyn std::error::Error>>;

    struct NormalizePathTestCase<'a> {
        path: &'a str,
        test: &'a str,
    }

    const NORMALIZE_PATH_TESTS: [NormalizePathTestCase; 6] = [
        NormalizePathTestCase {
            path: "./foo/bar.txt",
            test: "foo/bar.txt",
        },
        NormalizePathTestCase {
            path: "bar/../foo/bar.txt",
            test: "foo/bar.txt",
        },
        NormalizePathTestCase {
            path: "foo///bar.txt",
            test: "foo/bar.txt",
        },
        NormalizePathTestCase {
            path: "foo///bar",
            test: "foo/bar",
        },
        NormalizePathTestCase {
            path: "foo//./bar",
            test: "foo/bar",
        },
        NormalizePathTestCase {
            path: "/foo//./bar",
            test: "/foo/bar",
        },
    ];

    fn tempdir() -> Result<PathBuf, Box<dyn std::error::Error>> {
        let path = Path::new(".syd-test");
        std::fs::create_dir_all(path)?;
        let _ = OpenOptions::new()
            .write(true)
            .create(true)
            .mode(0o600)
            .open(path.join("test"))?;
        Ok(path.to_path_buf())
    }

    #[test]
    fn test_normalize_path() {
        for test in &NORMALIZE_PATH_TESTS {
            let path = Path::new(test.path);
            let normalized = normalize_path(path);
            assert_eq!(
                test.test
                    .replace('/', std::path::MAIN_SEPARATOR.to_string().as_str()),
                normalized.to_str().expect("Path is not valid utf-8!")
            );
        }
    }

    #[test]
    fn test_canonicalize_empty_path() -> TestResult {
        assert_eq!(canonicalize("", true, Normal), Err(Errno::ENOENT));
        assert_eq!(canonicalize("", true, Existing), Err(Errno::ENOENT));
        assert_eq!(canonicalize("", true, Missing), Err(Errno::ENOENT));
        assert_eq!(canonicalize("", false, Normal), Err(Errno::ENOENT));
        assert_eq!(canonicalize("", false, Existing), Err(Errno::ENOENT));
        assert_eq!(canonicalize("", false, Missing), Err(Errno::ENOENT));

        Ok(())
    }

    #[test]
    fn test_canonicalize_repetitive_slashes() -> TestResult {
        let result_test = canonicalize("/etc/passwd", true, Normal)?;
        let paths = vec![
            "/etc/passwd",
            "/etc//passwd",
            "/etc///passwd",
            "//etc/passwd",
            "//etc//passwd",
            "//etc///passwd",
            "///etc/passwd",
            "///etc//passwd",
            "///etc///passwd",
        ];
        for path in &paths {
            let result = canonicalize(path, true, Missing)?;
            assert_eq!(result, result_test);
        }

        Ok(())
    }

    #[test]
    fn test_canonicalize_dots_slashes() -> TestResult {
        let base = tempdir()?;

        let cwd = std::env::current_dir()?.display().to_string();
        let path = base.display().to_string();

        let result1 = canonicalize(format!("{cwd}/{path}//./..//{path}/test"), true, Normal)?
            .display()
            .to_string();
        let result2 = canonicalize(format!("{cwd}/{path}//./..//{path}/test"), true, Existing)?
            .display()
            .to_string();

        assert!(!result1.is_empty(), "result:{result1}");
        assert!(!result2.is_empty(), "result:{result2}");
        assert_eq!(result1, result2);

        Ok(())
    }

    #[test]
    fn test_canonicalize_non_directory_with_slash() -> TestResult {
        let cwd = std::env::current_dir()?.display().to_string();
        let path = tempdir()?.display().to_string();
        let test = format!("{cwd}/{path}/test/");

        assert_eq!(canonicalize(&test, true, Normal), Err(Errno::ENOTDIR));
        assert_eq!(canonicalize(&test, true, Existing), Err(Errno::ENOTDIR));
        assert!(canonicalize(&test, true, Missing).is_ok());
        assert_eq!(canonicalize(&test, false, Normal), Err(Errno::ENOTDIR));
        assert_eq!(canonicalize(&test, false, Existing), Err(Errno::ENOTDIR));
        assert!(canonicalize(&test, false, Missing).is_ok());

        Ok(())
    }

    /// FIXME: The asserts return success rather than failure.
    /// Bug or feature?
    #[test]
    #[ignore]
    fn test_canonicalize_missing_directory_returns_enoent() -> TestResult {
        assert_eq!(canonicalize("/zzz/..", true, Normal), Err(Errno::ENOENT));
        assert_eq!(canonicalize("/zzz/..", true, Existing), Err(Errno::ENOENT));
        assert_eq!(canonicalize("/zzz/..", false, Normal), Err(Errno::ENOENT));
        assert_eq!(canonicalize("/zzz/..", false, Existing), Err(Errno::ENOENT));

        Ok(())
    }
}
