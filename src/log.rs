//
// syd: application sandbox
// src/log.rs: Simple logging on standard error using JSON lines
//
// Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

/* Simple logging with JSON lines */
use std::{
    env,
    ffi::CString,
    io::{self, IsTerminal, Write},
    time::{SystemTime, UNIX_EPOCH},
};

use env_logger::filter::{Builder, Filter};
use log::{Level, SetLoggerError};
use nix::unistd::Pid;
use once_cell::sync::Lazy;
use parking_lot::Mutex;
use serde_json::{json, Map, Value};

use crate::{config::*, proc::proc_cmdline};

/// info! logging macro
#[macro_export]
macro_rules! info {
    ($($key:literal : $value:expr),+) => {
        #[allow(clippy::disallowed_methods)]
        $crate::log::log_with_data(log::Level::Info, serde_json::json!({$($key: $value),+}).as_object().unwrap().clone());
    }
}

/// error! logging macro
#[macro_export]
macro_rules! error {
    ($($key:literal : $value:expr),+) => {
        #[allow(clippy::disallowed_methods)]
        $crate::log::log_with_data(log::Level::Error, serde_json::json!({$($key: $value),+}).as_object().unwrap().clone());
    }
}

/// warn! logging macro
#[macro_export]
macro_rules! warn {
    ($($key:literal : $value:expr),+) => {
        #[allow(clippy::disallowed_methods)]
        $crate::log::log_with_data(log::Level::Warn, serde_json::json!({$($key: $value),+}).as_object().unwrap().clone());
    }
}

/// debug! logging macro
#[macro_export]
macro_rules! debug {
    ($($key:literal : $value:expr),+) => {
        #[cfg(debug_assertions)]
        #[allow(clippy::disallowed_methods)]
        $crate::log::log_with_data(log::Level::Debug, serde_json::json!({$($key: $value),+}).as_object().unwrap().clone());
    }
}

/// trace! logging macro
#[macro_export]
macro_rules! trace {
    ($($key:literal : $value:expr),+) => {
        #[cfg(debug_assertions)]
        #[allow(clippy::disallowed_methods)]
        $crate::log::log_with_data(log::Level::Trace, serde_json::json!({$($key: $value),+}).as_object().unwrap().clone());
    }
}

type Writer = Box<dyn Write + Send>;

/// Simple logging on standard error using JSON lines
pub struct JsonLinesLogger {
    filter: Filter,
    writer: Mutex<Writer>,
}

impl JsonLinesLogger {
    fn new() -> Self {
        let filters = env::var(LOG_ENVIRON).unwrap_or("warn".to_string());
        let mut builder = Builder::new();
        Self {
            filter: builder.parse(&filters).build(),
            writer: Mutex::new(Box::new(io::stderr())),
        }
    }

    /// Initialize logging
    pub fn init() -> Result<(), SetLoggerError> {
        let logger = Self::new();

        log::set_max_level(logger.filter.filter());
        log::set_boxed_logger(Box::new(logger))
    }
}

impl log::Log for JsonLinesLogger {
    fn enabled(&self, metadata: &log::Metadata) -> bool {
        self.filter.enabled(metadata)
    }

    fn log(&self, record: &log::Record) {
        if self.enabled(record.metadata()) {
            let mut writer = self.writer.lock();
            let _ = writeln!(writer, "{}", record.args());
        }
    }

    fn flush(&self) {}
}

fn level_to_int(level: log::Level) -> u8 {
    match level {
        Level::Error => 1,
        Level::Warn => 2,
        Level::Info => 3,
        Level::Debug => 4,
        Level::Trace => 5,
    }
}

/// Helper for logging using JSON lines.
#[allow(clippy::cognitive_complexity)]
pub fn log_with_data(level: Level, data: Map<String, Value>) {
    static NO_SYSLOG: Lazy<bool> = Lazy::new(|| std::env::var(NO_SYSLOG_ENVIRON).is_ok());
    #[allow(clippy::disallowed_methods)]
    let mut log_entry = json!({
        "id": "syd",
        "l" : level_to_int(level),
        "t": SystemTime::now().duration_since(UNIX_EPOCH).unwrap_or_default().as_secs(),
    })
    .as_object_mut()
    .unwrap()
    .clone();
    log_entry.extend(data);

    if level == Level::Warn {
        // Access violation
        #[allow(clippy::cast_possible_truncation)]
        #[allow(clippy::disallowed_methods)]
        let pid =
            Pid::from_raw(log_entry.get("pid").unwrap().as_i64().unwrap() as nix::libc::pid_t);
        let cmd = if pid.as_raw() == 0 {
            String::new()
        } else {
            proc_cmdline(pid).unwrap_or("?".to_string())
        };
        log_entry.insert("cmd".to_string(), Value::String(cmd));
    }

    let log_entry = Value::Object(log_entry).to_string();
    match level {
        Level::Error => {
            if !*NO_SYSLOG {
                syslog(&log_entry);
            } else if io::stderr().is_terminal() {
                log::error!("{log_entry}");
            }
        }
        Level::Warn => {
            if !*NO_SYSLOG {
                syslog(&log_entry);
            } else if io::stderr().is_terminal() {
                log::warn!("{log_entry}");
            }
        }
        Level::Info => log::info!("{log_entry}"),
        Level::Debug => log::debug!("{log_entry}"),
        Level::Trace => log::trace!("{log_entry}"),
    }
}

/// Logs a message to the system's syslog.
///
/// # Arguments
///
/// * `message` - The message string to be logged.
fn syslog(message: &str) {
    let c_msg = CString::new(message).unwrap_or_else(|_|
        // SAFETY: We ensure the string has no null bytes and the vector
        // we pass into the function is an actual byte vector.
        unsafe { CString::from_vec_unchecked(b"?".to_vec()) });

    // SAFETY: Use the syslog interface provided by libc.
    unsafe {
        nix::libc::openlog(std::ptr::null(), nix::libc::LOG_PID, nix::libc::LOG_USER);
        nix::libc::syslog(
            nix::libc::LOG_WARNING,
            "%s\0".as_ptr().cast(),
            c_msg.as_ptr(),
        );
        nix::libc::closelog();
    }
}
