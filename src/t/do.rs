//
// syd: application sandbox
// src/test-do.rs: Integration test cases
//
// Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#![allow(clippy::disallowed_methods)]

use std::{env, thread, time};

use nix::{
    libc::_exit,
    unistd::{fork, ForkResult},
};

fn main() {
    let command =
        env::var("SYD_DO").expect("No command specified via SYD_DO environment variable.");
    match command.as_str() {
        "fork" => do_fork(),
        "thread" => do_thread(),
        _ => panic!("Invalid command: {command}."),
    }
}

fn do_fork() -> ! {
    let args: Vec<String> = env::args().collect();
    if args.len() < 3 {
        panic!("Expected exit code and number of processes as arguments.");
    }
    let xcode: i32 = args[1].parse().expect("Failed to parse the exit code.");
    if xcode < 0 || xcode > u8::MAX as i32 {
        panic!("Invalid exit code.");
    }
    let nproc: i32 = args[2]
        .parse()
        .expect("Failed to parse the number of processes.");
    if !(0..=4096).contains(&nproc) {
        panic!("Invalid number for number of processes.");
    }

    for i in 0..nproc {
        match unsafe { fork() } {
            Ok(ForkResult::Parent { .. }) => {
                // Do nothing.
            }
            Ok(ForkResult::Child) => {
                thread::sleep(time::Duration::from_micros(4242 + i as u64));
                // SAFETY: In libc we trust.
                unsafe { _exit((i % 254) + 1) };
            }
            Err(error) => {
                eprintln!("Failed to fork: {error}.");
                // SAFETY: In libc we trust.
                unsafe { _exit(127) };
            }
        }
    }

    // SAFETY: In libc we trust.
    unsafe { _exit(xcode) };
}

fn do_thread() -> ! {
    let args: Vec<String> = env::args().collect();
    if args.len() < 3 {
        panic!("Expected exit code and number of processes as arguments.");
    }
    let xcode: i32 = args[1].parse().expect("Failed to parse the exit code.");
    if xcode < 0 || xcode > u8::MAX as i32 {
        panic!("Invalid exit code.");
    }
    let nproc: i32 = args[2]
        .parse()
        .expect("Failed to parse the number of processes.");
    if !(0..=4096).contains(&nproc) {
        panic!("Invalid number for number of processes.");
    }

    for _ in 0..nproc {
        // We don't join the threads deliberately here.
        let _ = thread::spawn(|| {
            thread::sleep(time::Duration::from_micros(4242));
        });
    }

    // SAFETY: In libc we trust.
    unsafe { _exit(xcode) };
}
