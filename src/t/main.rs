//
// syd: application sandbox
// src/test/main.rs: Run integration tests with TAP output
//
// Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::process::exit;

mod test;
mod util;
use test::*;
use util::*;

fn main() {
    // Print TAP plan.
    println!("# syd-test: Welcome to the Machine!");
    println!("1..{TEST_COUNT}");
    // We cannot run this under sandbox.
    if syd_enabled() {
        for (idx, (name, _)) in TESTS.iter().take(TEST_COUNT).enumerate() {
            let count = idx.saturating_add(1);
            println!("ok {count} - {name} # SKIP running under SydB☮x");
        }
        println!("# 0 tests passed.\n# 0 tests failed.\n# {TEST_COUNT} tests skipped.");
        return;
    }

    let mut fails = Vec::new();
    for (idx, (name, test)) in TESTS.iter().take(TEST_COUNT).enumerate() {
        let count = idx.saturating_add(1);
        match test() {
            Ok(_) => {
                println!("ok {count} - {name}");
            }
            Err(error) => {
                println!("not ok {count} - {name} - FAIL: {error}");
                fails.push(error);
            }
        }
    }

    let fail = fails.len();
    let succ = TEST_COUNT - fail;
    println!("# {succ} tests passed.");
    println!("# {fail} tests failed.");
    println!("# 0 tests skipped.");
    exit(fail.try_into().unwrap_or(127));
}
