//
// syd: application sandbox
// src/test/util.rs: Utilities for integration tests
//
// Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#![allow(clippy::disallowed_methods)]

use std::{env, error::Error, fmt, fs::canonicalize, process::Command};

use nix::errno::Errno;
use once_cell::sync::Lazy;

#[derive(Debug)]
pub struct TestError(pub String);
pub type TestResult = Result<(), TestError>;

impl fmt::Display for TestError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl<E: Error> From<E> for TestError {
    fn from(err: E) -> Self {
        TestError(err.to_string())
    }
}

#[macro_export]
macro_rules! assert {
    ($cond:expr) => {
        if !$cond {
            return Err(TestError(format!("Assertion failed: {}", stringify!($cond))));
        }
    };
    ($cond:expr, $($arg:tt)*) => {
        if !$cond {
            return Err(TestError(format!("Assertion failed: {}: {}", stringify!($cond), format_args!($($arg)*))));
        }
    };
}

#[macro_export]
macro_rules! assert_eq {
    ($left:expr, $right:expr) => {
        if $left != $right {
            return Err(TestError(format!("Assertion failed in {} ({}:{}): (left: `{}`, right: `{}`)", std::any::type_name::<fn()>(), file!(), line!(), $left, $right)));
        }
    };
    ($left:expr, $right:expr, $($arg:tt)*) => {
        if $left != $right {
            return Err(TestError(format!("Assertion failed in {} ({}:{}): (left: `{}`, right: `{}`): {}", std::any::type_name::<fn()>(), file!(), line!(), $left, $right, format_args!($($arg)*))));
        }
    };
}

#[macro_export]
macro_rules! assert_ne {
    ($left:expr, $right:expr) => {
        if $left == $right {
            return Err(TestError(format!("Assertion failed in {} ({}:{}): (left: `{}`, right: `{}`)", std::any::type_name::<fn()>(), file!(), line!(), $left, $right)));
        }
    };
    ($left:expr, $right:expr, $($arg:tt)*) => {
        if $left == $right {
            return Err(TestError(format!("Assertion failed in {} ({}:{}): (left: `{}`, right: `{}`): {}", std::any::type_name::<fn()>(), file!(), line!(), $left, $right, format_args!($($arg)*))));
        }
    };
}

/// Holds a `String` to run `syd`.
/// Honours CARGO_BIN_EXE_syd environment variable.
pub static SYD: Lazy<String> =
    Lazy::new(|| env::var("CARGO_BIN_EXE_syd").unwrap_or("syd".to_string()));

/// Holds a `String` to run `syd-test-do`.
/// Honours CARGO_BIN_EXE_syd-test-do environment variable.
pub static SYD_DO: Lazy<String> =
    Lazy::new(|| env::var("CARGO_BIN_EXE_syd-test-do").unwrap_or("syd-test-do".to_string()));

/// Returns a `Command` to run `syd`.
/// Honours CARGO_BIN_EXE_syd environment variable.
pub fn syd() -> Command {
    let mut cmd = Command::new(&*SYD);
    cmd.env("RUST_BACKTRACE", "1");
    cmd.env("SYD_LOG", "trace");
    cmd.env("SYD_NO_SYSLOG", "1");
    cmd
}

/// Resembles the `which` command, finds a program in PATH.
pub fn which(command: &str) -> Result<String, Errno> {
    let out = Command::new("which")
        .arg(command)
        .output()
        .expect("execute which")
        .stdout;
    if out.is_empty() {
        return Err(Errno::ENOENT);
    }

    let bin = String::from_utf8_lossy(&out);
    let bin = bin.trim();
    Ok(canonicalize(bin)
        .map_err(|_| Errno::last())?
        .to_string_lossy()
        .into_owned())
}

// Returns true if we are running under SydB☮x.
pub fn syd_enabled() -> bool {
    // This will not work if the sandbox is locked.
    // Path::new("/dev/syd").exists() || Path::new("/dev/sydbox").exists()
    syd()
        .arg("true")
        .status()
        .expect("execute syd")
        .code()
        .unwrap_or(0)
        == Errno::EBUSY as i32
}
