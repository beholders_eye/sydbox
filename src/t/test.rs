//
// syd: application sandbox
// src/test/test.rs: Integration tests
//
// Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#![allow(clippy::disallowed_methods)]

use std::env;

use crate::{assert, assert_eq, util::*};

/// Represents a test case.
pub type Test<'a> = (&'a str, fn() -> TestResult);

macro_rules! test_entry {
    ($func:expr) => {
        (stringify!($func), $func)
    };
}

/// List of integration tests.
pub const TESTS: [Test; TEST_COUNT] = [
    test_entry!(test_syd_true_returns_success),
    test_entry!(test_syd_true_returns_success_with_many_processes),
    test_entry!(test_syd_true_returns_success_with_many_threads),
    test_entry!(test_syd_false_returns_failure),
    test_entry!(test_syd_true_returns_failure_with_many_processes),
    test_entry!(test_syd_true_returns_failure_with_many_threads),
    test_entry!(test_syd_whoami_returns_root),
    test_entry!(test_syd_lock),
    test_entry!(test_syd_lock_exec),
    test_entry!(test_syd_read_sandbox_open_allow),
    test_entry!(test_syd_read_sandbox_open_deny),
    test_entry!(test_syd_stat_sandbox_chdir_allow),
    test_entry!(test_syd_stat_sandbox_chdir_hide),
    test_entry!(test_syd_stat_sandbox_stat_allow),
    test_entry!(test_syd_stat_sandbox_stat_hide),
    test_entry!(test_syd_stat_sandbox_getdents_allow),
    test_entry!(test_syd_stat_sandbox_getdents_hide),
    test_entry!(test_syd_write_sandbox_open_allow),
    test_entry!(test_syd_write_sandbox_open_deny),
    test_entry!(test_syd_exec_sandbox_open_allow),
    test_entry!(test_syd_exec_sandbox_open_deny),
];
/// Number of integration tests
pub const TEST_COUNT: usize = 21;

// Tests if `true` returns success under sandbox.
fn test_syd_true_returns_success() -> TestResult {
    let status = syd().arg("true").status().expect("execute syd");
    assert!(status.success());

    let status = syd().args(["--", "true"]).status().expect("execute syd");
    assert!(status.success());

    Ok(())
}

// Tests if `syd` returns success for a sandbox running many processes,
// in case the execve child returns success.
fn test_syd_true_returns_success_with_many_processes() -> TestResult {
    env::set_var("SYD_DO", "fork");
    let status = syd()
        .args(["--", &SYD_DO, "0", "8"])
        .status()
        .expect("execute syd");
    assert!(status.success());

    Ok(())
}

// Tests if `syd` returns success for a sandbox running many threads,
// in case the execve child returns success.
fn test_syd_true_returns_success_with_many_threads() -> TestResult {
    env::set_var("SYD_DO", "thread");
    let status = syd()
        .args(["--", &SYD_DO, "0", "8"])
        .status()
        .expect("execute syd");
    assert!(status.success());

    Ok(())
}

// Tests if `false` returns failure under sandbox.
fn test_syd_false_returns_failure() -> TestResult {
    let status = syd().arg("false").status().expect("execute syd");
    assert_ne!(status.code().unwrap_or(127), 0);

    let status = syd().args(["--", "false"]).status().expect("execute syd");
    assert_ne!(status.code().unwrap_or(127), 0);

    Ok(())
}

// Tests if `syd` returns failure for a sandbox running many processes,
// in case the execve child returns failure.
fn test_syd_true_returns_failure_with_many_processes() -> TestResult {
    env::set_var("SYD_DO", "fork");
    let status = syd()
        .args(["--", &SYD_DO, "7", "8"])
        .status()
        .expect("execute syd");
    assert!(!status.success());
    assert_eq!(status.code().unwrap_or(127), 7);

    Ok(())
}

// Tests if `syd` returns failure for a sandbox running many threads,
// in case the execve child returns failure.
fn test_syd_true_returns_failure_with_many_threads() -> TestResult {
    env::set_var("SYD_DO", "thread");
    let status = syd()
        .args(["--", &SYD_DO, "7", "8"])
        .status()
        .expect("execute syd");
    assert!(!status.success());
    assert_eq!(status.code().unwrap_or(127), 7);

    Ok(())
}

// Tests if `whoami` returns `root` with `syd --root`.
fn test_syd_whoami_returns_root() -> TestResult {
    let output = syd()
        .args(["-r", "--", "whoami"])
        .output()
        .expect("execute syd");
    assert!(output.stdout.starts_with(b"root"));

    Ok(())
}

// Tests if `syd --lock` disables access to `/dev/syd`.
fn test_syd_lock() -> TestResult {
    eprintln!("+ stat /dev/syd");
    let status = syd()
        .args(["--", "stat", "/dev/syd"])
        .status()
        .expect("execute syd");
    assert_eq!(status.code().unwrap_or(127), 0);

    eprintln!("+ stat /dev/syd");
    let status = syd()
        .args(["-l", "--", "stat", "/dev/syd"])
        .status()
        .expect("execute syd");
    assert_ne!(status.code().unwrap_or(127), 0);

    Ok(())
}

// Tests if `lock:exec` locks the sandbox for all except the exec child.
fn test_syd_lock_exec() -> TestResult {
    eprintln!("+ sh -c \"test -e /dev/syd\"");
    let status = syd()
        .args(["-mlock:exec", "--", "sh", "-c", "test -e /dev/syd"])
        .status()
        .expect("execute syd");
    assert_eq!(status.code().unwrap_or(127), 0);

    eprintln!("+ stat /dev/syd");
    let status = syd()
        .args(["-mlock:exec", "--", "sh", "-c", "stat", "/dev/syd"])
        .status()
        .expect("execute syd");
    assert_ne!(status.code().unwrap_or(127), 0);

    Ok(())
}

// Tests if read sandboxing for open works to allow.
fn test_syd_read_sandbox_open_allow() -> TestResult {
    eprintln!("+ cat /dev/null");
    let status = syd()
        .args([
            "-puser",
            "-mdenylist/read+/dev/***",
            "-mallowlist/read+/dev/null",
            "--",
            "cat",
            "/dev/null",
        ])
        .status()
        .expect("execute syd");
    assert_eq!(status.code().unwrap_or(127), 0);

    Ok(())
}

// Tests if read sandboxing for open works to deny.
fn test_syd_read_sandbox_open_deny() -> TestResult {
    eprintln!("+ cat /dev/null");
    let status = syd()
        .args([
            "-puser",
            "-mallowlist/read+/***",
            "-mdenylist/read+/dev/null",
            "--",
            "cat",
            "/dev/null",
        ])
        .status()
        .expect("execute syd");
    assert_ne!(status.code().unwrap_or(127), 0);

    Ok(())
}

// Tests if stat sandboxing for chdir works to allow.
fn test_syd_stat_sandbox_chdir_allow() -> TestResult {
    eprintln!("+ sh -c \"cd /dev\"");
    let status = syd()
        .args([
            "-puser",
            "-mdenylist/stat+/dev",
            "-mallowlist/stat+/dev",
            "--",
            "/bin/sh",
            "-c",
            "cd /dev",
        ])
        .status()
        .expect("execute syd");
    assert_eq!(status.code().unwrap_or(127), 0);

    Ok(())
}

// Tests if stat sandboxing for stat works to hide.
fn test_syd_stat_sandbox_chdir_hide() -> TestResult {
    eprintln!("+ cd /dev");
    let status = syd()
        .args([
            "-puser",
            "-mallowlist/stat+/***",
            "-mdenylist/stat+/dev",
            "--",
            "cd",
            "/dev",
        ])
        .status()
        .expect("execute syd");
    assert_ne!(status.code().unwrap_or(127), 0);

    Ok(())
}

// Tests if stat sandboxing for stat works to allow.
fn test_syd_stat_sandbox_stat_allow() -> TestResult {
    eprintln!("+ ls /dev/null");
    let status = syd()
        .args([
            "-puser",
            "-mdenylist/stat+/dev/null",
            "-mallowlist/stat+/dev/null",
            "--",
            "ls",
            "/dev/null",
        ])
        .status()
        .expect("execute syd");
    assert_eq!(status.code().unwrap_or(127), 0);

    Ok(())
}

// Tests if stat sandboxing for stat works to hide.
fn test_syd_stat_sandbox_stat_hide() -> TestResult {
    eprintln!("+ ls /dev/null");
    let status = syd()
        .args([
            "-puser",
            "-mallowlist/stat+/***",
            "-mdenylist/stat+/dev/null",
            "--",
            "ls",
            "/dev/null",
        ])
        .status()
        .expect("execute syd");
    assert_ne!(status.code().unwrap_or(127), 0);

    Ok(())
}

// Tests if stat sandboxing for getdents works to allow.
fn test_syd_stat_sandbox_getdents_allow() -> TestResult {
    eprintln!("+ ls /dev");
    let output = syd()
        .args([
            "-puser",
            "-mdenylist/stat+/dev/null",
            "-mallowlist/stat+/dev/null",
            "--",
            "ls",
            "/dev",
        ])
        .output()
        .expect("execute syd");
    assert!(
        output
            .stdout
            .windows(b"null".len())
            .any(|window| window == b"null"),
        "Stdout:\n{:?}",
        output.stdout
    );

    Ok(())
}

// Tests if stat sandboxing for getdents works to hide.
fn test_syd_stat_sandbox_getdents_hide() -> TestResult {
    eprintln!("+ ls /dev");
    let output = syd()
        .args([
            "-puser",
            "-mallowlist/stat+/***",
            "-mdenylist/stat+/dev/null",
            "--",
            "ls",
            "/dev",
        ])
        .output()
        .expect("execute syd");
    assert!(
        output
            .stdout
            .windows(b"null".len())
            .any(|window| window != b"null"),
        "Stdout:\n{:?}",
        output.stdout
    );

    Ok(())
}

// Tests if write sandboxing for open works to allow.
fn test_syd_write_sandbox_open_allow() -> TestResult {
    eprintln!("+ sh -c \"echo welcome to the machine >> /dev/null\"");
    let status = syd()
        .args([
            "-puser",
            "-mdenylist/write+/dev/***",
            "-mallowlist/write+/dev/null",
            "--",
            "sh",
            "-c",
            "echo welcome to the machine >> /dev/null",
        ])
        .status()
        .expect("execute syd");
    assert_eq!(status.code().unwrap_or(127), 0);

    Ok(())
}

// Tests if write sandboxing for open works to deny.
fn test_syd_write_sandbox_open_deny() -> TestResult {
    eprintln!("+ sh -c \"echo welcome to the machine >> /dev/null\"");
    let status = syd()
        .args([
            "-puser",
            "-mallowlist/write+/***",
            "-mdenylist/write+/dev/null",
            "--",
            "sh",
            "-c",
            "echo welcome to the machine >> /dev/null",
        ])
        .status()
        .expect("execute syd");
    assert_ne!(status.code().unwrap_or(127), 0);

    Ok(())
}

// Tests if exec sandboxing works to allow.
fn test_syd_exec_sandbox_open_allow() -> TestResult {
    let bin = which("true")?;
    eprintln!("+ {bin}");
    let status = syd()
        .args([
            "-puser",
            "-mdenylist/exec+/***",
            &format!("-mallowlist/exec+{bin}"),
            "--",
            &bin.to_string(),
        ])
        .status()
        .expect("execute syd");
    assert_eq!(status.code().unwrap_or(127), 0);

    Ok(())
}

// Tests if exec sandboxing works to deny.
fn test_syd_exec_sandbox_open_deny() -> TestResult {
    let bin = which("true")?;
    eprintln!("+ {bin}");
    let status = syd()
        .args([
            "-puser",
            "-mallowlist/exec+/***",
            &format!("-mdenylist/exec+{bin}"),
            "--",
            &bin.to_string(),
        ])
        .status()
        .expect("execute syd");
    assert_ne!(status.code().unwrap_or(127), 0);

    Ok(())
}
