//
// syd: application sandbox
// src/hook.rs: Secure computing hooks
//
// Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
// Based in part upon greenhook which is under public domain.
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{
    borrow::Cow,
    collections::{HashMap, HashSet},
    ffi::{CStr, CString},
    fs::{File, OpenOptions},
    io::{self, IoSlice, IoSliceMut, Read, Result as IOResult, Seek, SeekFrom, Write},
    mem::{self, size_of, MaybeUninit},
    net::{IpAddr, Ipv4Addr},
    os::{
        fd::{AsRawFd, RawFd},
        unix::{
            ffi::OsStrExt,
            process::{CommandExt, ExitStatusExt},
        },
    },
    path::Path,
    process::{Command, ExitStatus, Stdio},
    ptr,
    str::FromStr,
    sync::Arc,
    thread::{Builder, JoinHandle},
};

use anyhow::{bail, Context};
use libseccomp::{
    ScmpAction, ScmpFilterContext, ScmpNotifReq, ScmpNotifResp, ScmpNotifRespFlags, ScmpSyscall,
};
use log::{log_enabled, Level};
use nix::{
    cmsg_space,
    errno::Errno,
    fcntl::OFlag,
    libc::{
        self, c_uint, c_void, cmsghdr, ioctl, msghdr, CMSG_DATA, CMSG_FIRSTHDR, CMSG_LEN,
        CMSG_SPACE,
    },
    poll::{PollFd, PollFlags},
    sys::{
        signal::{kill, SIGHUP, SIGKILL, SIGTSTP, SIGTTIN, SIGTTOU},
        socket::{
            getsockname, recvmsg, socketpair, AddressFamily, ControlMessageOwned, MsgFlags,
            SockFlag, SockType, SockaddrLike, SockaddrStorage,
        },
        stat::{lstat, Mode},
        uio::{process_vm_readv, process_vm_writev, RemoteIoVec},
        utsname::uname,
    },
    unistd::{close, ForkResult, Pid},
};
use nonempty::{nonempty, NonEmpty};
use once_cell::sync::Lazy;
use parking_lot::RwLock;
use threadpool::ThreadPool;

use crate::{
    compat::getdents,
    config::*,
    debug, error, info,
    log::JsonLinesLogger,
    proc::proc_tgid,
    sandbox::{Action, Capability, Sandbox, SandboxGuard},
    trace, warn,
};

/*
 * Utilities
 */
/// Converts a reference to `T` into a slice of bytes.
///
/// # Safety
///
/// This function is unsafe because it may produce a slice that does not
/// respect the original alignment requirements of `T`. The caller must ensure
/// that the resultant slice of bytes is only used in ways that are consistent
/// with the memory layout of `T`.
///
/// It's also crucial to ensure that the lifetime of the returned slice does not
/// exceed the lifetime of the input reference, `p`.
unsafe fn any_as_u8_slice<T: Sized>(p: &T) -> &[u8] {
    ::core::slice::from_raw_parts((p as *const T) as *const u8, ::core::mem::size_of::<T>())
}

/// Converts a mutable reference to `T` into a mutable slice of bytes.
///
/// # Safety
///
/// This function is unsafe for the same reasons as `any_as_u8_slice`. It can
/// produce a slice that does not respect the original alignment requirements
/// of `T`. The caller must ensure the resultant slice of bytes is only used in
/// ways that are consistent with the memory layout of `T`.
///
/// Also, the caller should ensure that no other references to `p` exist when
/// this function is called, as this could lead to aliasing mutable references,
/// which is undefined behavior in Rust.
unsafe fn any_as_u8_mut_slice<T: Sized>(p: &mut T) -> &mut [u8] {
    ::core::slice::from_raw_parts_mut((p as *mut T) as *mut u8, ::core::mem::size_of::<T>())
}

/*
 * Macros
 */
macro_rules! validate_request_or_fail {
    ($request:expr) => {
        if !$request.is_valid() {
            return Ok($request.fail_syscall(nix::libc::EACCES));
        }
    };
}

macro_rules! syscall_handler {
    ($request:expr, $body:expr) => {{
        let inner = |request: &UNotifyEventRequest| -> Result<libseccomp::ScmpNotifResp, Errno> {
            let (req, proc) = request.prepare()?;
            validate_request_or_fail!(request);
            $body(req, &proc)
        };

        match inner($request) {
            Ok(result) => result,
            Err(error) => $request.fail_syscall(error as i32),
        }
    }};
}

macro_rules! remote_path_n {
    ($remote_process:expr, $request:expr, $n:expr) => {
        $remote_process.remote_path($remote_process.get_pid(), $request.data.args[$n] as usize)
    };
}

/// Seccomp sandbox profile export modes.
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum ExportMode {
    /// Berkeley Packet Filter (binary, machine readable)
    BerkeleyPacketFilter,
    /// Pseudo Filter Code (text, human readable)
    PseudoFiltercode,
}

impl FromStr for ExportMode {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "bpf" => Ok(Self::BerkeleyPacketFilter),
            "pfc" => Ok(Self::PseudoFiltercode),
            _ => bail!("Invalid export mode, expected one of bpf, or pfc!"),
        }
    }
}

/// `SyscallPathArgument` represents a system call path argument,
/// coupled with a directory file descriptor as necessary.
pub struct SyscallPathArgument {
    /// DirFd index in syscall args, if applicable.
    pub dirfd: Option<usize>,
    /// Path index in syscall args, if applicable.
    pub path: Option<usize>,
    /// If the path is a symlink, should be resolve it?
    pub follow: bool,
}

// SocketPair is used to copy fd from child to parent
// with sendmsg/recvmsg and SCM_RIGHTS
#[derive(Debug)]
struct SocketPair {
    // child fd
    sender: RawFd,
    // parent fd
    receiver: RawFd,
}

struct Sender {
    fd: RawFd,
}

struct Receiver {
    fd: RawFd,
}

impl SocketPair {
    pub(crate) fn init() -> Result<Self, io::Error> {
        let pairs = socketpair(
            AddressFamily::Unix,
            SockType::Stream,
            None,
            SockFlag::SOCK_CLOEXEC,
        )?;
        Ok(SocketPair {
            sender: pairs.0.as_raw_fd(),
            receiver: pairs.1.as_raw_fd(),
        })
    }

    pub(crate) fn channel(&self) -> (Sender, Receiver) {
        (Sender { fd: self.sender }, Receiver { fd: self.receiver })
    }
}

impl Drop for SocketPair {
    fn drop(&mut self) {
        let _ = close(self.sender);
        let _ = close(self.receiver);
    }
}

impl Sender {
    // nix::sys::socket::sendmsg allocates when cmsgs is not empty
    // which is not a good idea inside pre_exec
    // ref: nix's sendmsg implementation (MIT license)
    // (https://github.com/nix-rust/nix/blob/c6f9e2332efcf62c751d7a0174bb791e732b90a8/src/sys/socket/mod.rs#L1474)
    pub(crate) fn sendfd(&self, fd: RawFd) -> IOResult<()> {
        #[allow(clippy::cast_possible_truncation)]
        const FD_SIZE: c_uint = size_of::<RawFd>() as c_uint;
        const CAPACITY: u32 = unsafe { CMSG_SPACE(FD_SIZE) };
        const ALIGNMENT: usize = mem::align_of::<cmsghdr>();
        let buf = [0u8; ALIGNMENT + CAPACITY as usize];
        let (_prefix, aligned_buf, _suffix) = unsafe { buf.align_to::<cmsghdr>() };
        #[allow(clippy::as_ptr_cast_mut)]
        let cmsg_ptr = aligned_buf.as_ptr() as *mut c_void;
        let mut _binding = [0; 1];
        let mut _iov_buffer = [IoSliceMut::new(&mut _binding); 1];

        let mhdr = unsafe {
            // Musl's msghdr has private fields, so this is the only way to
            // initialize it.
            let mut mhdr = mem::MaybeUninit::<msghdr>::zeroed();
            let p = mhdr.as_mut_ptr();
            (*p).msg_name = ptr::null::<()>() as *mut _;
            (*p).msg_namelen = 0;
            // transmute iov into a mutable pointer.  sendmsg doesn't really mutate
            // the buffer, but the standard says that it takes a mutable pointer
            (*p).msg_iov = _iov_buffer.as_ref().as_ptr() as *mut _;
            (*p).msg_iovlen = 1;
            (*p).msg_control = cmsg_ptr;
            #[cfg(not(target_env = "musl"))]
            {
                (*p).msg_controllen = CAPACITY as usize;
            }
            #[cfg(target_env = "musl")]
            {
                (*p).msg_controllen = CAPACITY as u32;
            }
            (*p).msg_flags = 0;
            mhdr.assume_init()
        };

        let pmhdr: *mut cmsghdr = unsafe { CMSG_FIRSTHDR(&mhdr) };

        unsafe {
            (*pmhdr).cmsg_level = libc::SOL_SOCKET;
            (*pmhdr).cmsg_type = libc::SCM_RIGHTS;
            #[cfg(not(target_env = "musl"))]
            {
                (*pmhdr).cmsg_len = CMSG_LEN(FD_SIZE) as usize;
            }
            #[cfg(target_env = "musl")]
            {
                (*pmhdr).cmsg_len = CMSG_LEN(FD_SIZE) as u32;
            }
            ptr::copy_nonoverlapping(
                &[fd] as *const _ as *const u8,
                CMSG_DATA(pmhdr),
                FD_SIZE as usize,
            )
        }
        let ret = unsafe { libc::sendmsg(self.fd, &mhdr, 0) };

        if ret < 0 {
            Err(io::Error::last_os_error())
        } else {
            Ok(())
        }
    }
}

impl Receiver {
    pub(crate) fn recvfd(&self) -> IOResult<RawFd> {
        let mut cmsg_buffer = cmsg_space!(RawFd);
        let mut _binding = [0; 1];
        let mut _iov_buffer = [IoSliceMut::new(&mut _binding); 1];
        let res = recvmsg::<()>(
            self.fd,
            &mut _iov_buffer,
            Some(&mut cmsg_buffer),
            MsgFlags::empty(),
        )
        .map_err(|e| io::Error::from_raw_os_error(e as i32))?;
        for cmsg in res.cmsgs() {
            if let ControlMessageOwned::ScmRights(fds) = cmsg {
                return Ok(fds[0]);
            }
        }
        Err(io::Error::from_raw_os_error(libc::EINVAL))
    }
}

/// `UNotifyEventRequest` is the type of parameter that user's function
/// would get.
#[derive(Debug)]
pub struct UNotifyEventRequest {
    request: libseccomp::ScmpNotifReq,
    notify_fd: RawFd,
    sandbox: Arc<RwLock<Sandbox>>,
}

type RequestWithProcess<'a> = (&'a ScmpNotifReq, RemoteProcess);

impl UNotifyEventRequest {
    fn new(
        request: libseccomp::ScmpNotifReq,
        notify_fd: RawFd,
        sandbox: Arc<RwLock<Sandbox>>,
    ) -> Self {
        UNotifyEventRequest {
            request,
            notify_fd,
            sandbox,
        }
    }

    /// Given a `UNotifyEventRequest` return the seccomp request and a corresponding `RemoteProcess` instance.
    pub fn prepare(&self) -> Result<RequestWithProcess, Errno> {
        let req = self.get_request();
        #[allow(clippy::cast_possible_wrap)]
        let pid = Pid::from_raw(req.pid as i32);
        let proc = RemoteProcess::new(pid)?;
        Ok((req, proc))
    }

    /// Returns the unotify request (`libseccomp::ScmpNotifReq`) of
    /// this event.
    pub fn get_request(&self) -> &libseccomp::ScmpNotifReq {
        &self.request
    }

    /// Returns the internal `Sandbox` object locking it as necessary,
    /// and wrappped in a `SandboxGuard`.
    pub fn get_sandbox(&self, write: bool) -> SandboxGuard {
        if write {
            SandboxGuard::Write(self.sandbox.write())
        } else {
            SandboxGuard::Read(self.sandbox.read())
        }
    }

    /// Let the kernel continue the syscall.
    ///
    /// # Safety
    /// CAUTION! This method is unsafe because it may suffer TOCTOU attack.
    /// Please read `seccomp_unotify(2)` "NOTES/Design goals; use of `SECCOMP_USER_NOTIF_FLAG_CONTINUE`"
    /// before using this method.
    pub unsafe fn continue_syscall(&self) -> libseccomp::ScmpNotifResp {
        libseccomp::ScmpNotifResp::new(self.request.id, 0, 0, ScmpNotifRespFlags::CONTINUE.bits())
    }

    /// Returns error to supervised process.
    /// `err` parameter should be a number larger than 0.
    pub fn fail_syscall(&self, err: i32) -> libseccomp::ScmpNotifResp {
        debug_assert!(err > 0);
        #[allow(clippy::arithmetic_side_effects)]
        libseccomp::ScmpNotifResp::new(self.request.id, 0, -err, 0)
    }

    /// Returns value to supervised process.
    pub fn return_syscall(&self, val: i64) -> libseccomp::ScmpNotifResp {
        libseccomp::ScmpNotifResp::new(self.request.id, val, 0, 0)
    }

    /// Check if this event is still valid.
    /// In some cases this is necessary, please check `seccomp_unotify(2)` for more information.
    pub fn is_valid(&self) -> bool {
        libseccomp::notify_id_valid(self.notify_fd, self.request.id).is_ok()
    }

    /// Add a file descriptor to the supervised process.
    /// This could help avoid TOCTOU attack in some cases.
    pub fn add_fd(&self, src_fd: RawFd) -> Result<RawFd, io::Error> {
        let addfd: libseccomp_sys::seccomp_notif_addfd = libseccomp_sys::seccomp_notif_addfd {
            id: self.request.id,
            flags: 0,
            srcfd: src_fd as u32,
            newfd: 0,
            newfd_flags: 0,
        };
        #[cfg(not(target_env = "musl"))]
        const SECCOMP_IOCTL_NOTIF_ADDFD: u64 = 0x40182103;
        #[cfg(target_env = "musl")]
        const SECCOMP_IOCTL_NOTIF_ADDFD: i32 = 0x40182103;

        let new_fd = unsafe {
            ioctl(
                self.notify_fd,
                SECCOMP_IOCTL_NOTIF_ADDFD,
                std::ptr::addr_of!(addfd),
            )
        };
        if new_fd < 0 {
            Err(io::Error::last_os_error())
        } else {
            Ok(new_fd as RawFd)
        }
    }
}

/// By using `RemoteProcess`, you can get some information about the supervised process.
#[derive(Debug)]
pub struct RemoteProcess {
    pid: Pid,
    fd: RawFd,
}

impl RemoteProcess {
    /// Create a `RemoteProcess` object from a `Pid`.
    ///
    /// # Examples
    ///
    /// ```ignore
    /// let remote = RemoteProcess::new(Pid::from_raw(req.request.pid as i32)).unwrap();
    /// ```
    pub fn new(pid: Pid) -> Result<Self, Errno> {
        // get TGID of given pid (TID)
        let tgid = proc_tgid(pid)?;
        let fd = unsafe { libc::syscall(libc::SYS_pidfd_open, tgid, 0) };
        if fd < 0 {
            return Err(Errno::last());
        }

        Ok(RemoteProcess {
            pid: tgid,
            fd: fd as RawFd,
        })
    }

    /// Get file descriptor from remote process with `pidfd_getfd()`.
    /// This function requires Linux 5.6+.
    pub fn get_fd(&self, remote_fd: RawFd) -> Result<RawFd, Errno> {
        let local_fd = unsafe { libc::syscall(libc::SYS_pidfd_getfd, self.fd, remote_fd, 0) };
        if local_fd < 0 {
            Err(Errno::last())
        } else {
            Ok(local_fd as RawFd)
        }
    }

    /// Get process ID of the process
    pub fn get_pid(&self) -> Pid {
        self.pid
    }

    /// Read data from remote process's memory with `process_vm_readv()`.
    /// You should run `is_valid()` after this method to check if the remote process and corresponding syscall
    /// is still alive.
    ///
    /// # Examples
    /// ```ignore
    /// let mut buf = [0u8; 256];
    /// remote.read_mem(&mut buf, path as usize).unwrap();
    /// ```
    pub fn read_mem(&self, local_buffer: &mut [u8], remote_addr: usize) -> Result<usize, Errno> {
        static FORCE_PROC: Lazy<bool> =
            Lazy::new(|| std::env::var(NO_CROSS_MEMORY_ATTACH_ENVIRON).is_ok());
        if *FORCE_PROC {
            return self
                .read_mem_proc(local_buffer, remote_addr)
                .map_err(|_| Errno::last());
        }

        let len = local_buffer.len();
        match process_vm_readv(
            self.pid,
            &mut [IoSliceMut::new(local_buffer)],
            &[RemoteIoVec {
                len,
                base: remote_addr,
            }],
        ) {
            Ok(n) => Ok(n),
            Err(e) => match e as i32 {
                nix::libc::ENOSYS | nix::libc::EPERM => self
                    .read_mem_proc(local_buffer, remote_addr)
                    .map_err(|_| Errno::last()),
                _ => Err(Errno::last()),
            },
        }
    }

    /// Write data to remote process's memory with `process_vm_writev()`.
    /// You should run `is_valid()` after this method to check if the remote process and corresponding syscall
    /// is still alive.
    ///
    /// # Examples
    /// ```ignore
    /// let buf = [0u8; 256];
    /// remote.write_mem(&buf, path as usize).unwrap();
    /// ```
    pub fn write_mem(&self, local_buffer: &[u8], remote_addr: usize) -> Result<usize, Errno> {
        static FORCE_PROC: Lazy<bool> =
            Lazy::new(|| std::env::var(NO_CROSS_MEMORY_ATTACH_ENVIRON).is_ok());
        if *FORCE_PROC {
            return self
                .write_mem_proc(local_buffer, remote_addr)
                .map_err(|_| Errno::last());
        }

        let len = local_buffer.len();
        match process_vm_writev(
            self.pid,
            &[IoSlice::new(local_buffer)],
            &[RemoteIoVec {
                len,
                base: remote_addr,
            }],
        ) {
            Ok(n) => Ok(n),
            Err(e) => match e as i32 {
                nix::libc::ENOSYS | nix::libc::EPERM => self
                    .write_mem_proc(local_buffer, remote_addr)
                    .map_err(|_| Errno::last()),
                _ => Err(Errno::last()),
            },
        }
    }

    /// Fallback method to read data from `/proc/$pid/mem` when `process_vm_readv()` is unavailable.
    pub fn read_mem_proc(&self, local_buffer: &mut [u8], remote_addr: usize) -> IOResult<usize> {
        let path = format!("/proc/{}/mem", self.pid);
        let mut file = File::open(path)?;
        file.seek(SeekFrom::Start(remote_addr as u64))?;
        file.read(local_buffer)
    }

    /// Fallback method to write data to `/proc/$pid/mem` when `process_vm_writev()` is unavailable.
    fn write_mem_proc(&self, local_buffer: &[u8], remote_addr: usize) -> IOResult<usize> {
        let path = format!("/proc/{}/mem", self.pid);
        let mut file = OpenOptions::new().write(true).open(path)?;
        file.seek(SeekFrom::Start(remote_addr as u64))?;
        file.write(local_buffer)
    }

    /// Read path from the given system call argument with the given request.
    fn read_path(&self, req: &ScmpNotifReq, arg: &SyscallPathArgument) -> Result<String, Errno> {
        let path = match arg.path {
            Some(idx) => Some(remote_path_n!(self, req, idx)?),
            None => None,
        };
        let path = if let Some(path) = path {
            if path.to_bytes().first() == Some(&b'/') {
                path.to_string_lossy().to_string()
            } else {
                let dir = self.read_directory(req, arg)?;
                let path = path.to_string_lossy().to_string();
                format!("{dir}/{path}")
            }
        } else {
            self.read_directory(req, arg)?
        };

        match crate::fs::canonicalize(&path, arg.follow, crate::fs::MissingHandling::Missing) {
            Ok(path) => Ok(path.as_os_str().to_string_lossy().to_string()),
            Err(error) => {
                error!("ctx": "path_canonicalize", "path": path, "err": format!("{error}"));
                Err(error)
            }
        }
    }

    /// Read directory from the given system call argument with the given request.
    fn read_directory(
        &self,
        req: &ScmpNotifReq,
        arg: &SyscallPathArgument,
    ) -> Result<String, Errno> {
        let dir = match arg.dirfd {
            Some(idx) => Self::remote_dirfd(req.pid, Self::remote_fd(req, idx)),
            None => Self::remote_dirfd(req.pid, None),
        };
        match crate::fs::canonicalize(dir, true, crate::fs::MissingHandling::Missing) {
            Ok(dir) => Ok(dir.to_string_lossy().to_string()),
            Err(error) => {
                error!("ctx": "path_canonicalize", "dir": arg.dirfd.unwrap_or(-nix::libc::AT_FDCWD as usize), "err": format!("{error}"));
                Err(error)
            }
        }
    }

    /// Convert the file descriptor argument into a `RawFd`.
    /// If the argument refers to the current working directory,
    /// this function returns `None`.
    pub fn remote_fd(req: &ScmpNotifReq, idx: usize) -> Option<RawFd> {
        #[allow(clippy::cast_possible_truncation)]
        let fd = req.data.args[idx] as i32;
        if fd == nix::libc::AT_FDCWD {
            None
        } else {
            Some(fd as RawFd)
        }
    }

    /// Returns the file descriptor or current working directory path for the given `Pid`.
    pub fn remote_dirfd(pid: u32, fd: Option<RawFd>) -> String {
        if let Some(fd) = fd {
            format!("/proc/{pid}/fd/{fd}")
        } else {
            format!("/proc/{pid}/cwd")
        }
    }

    /// Read the `nix::libc::open_how` struct from process memory
    /// at the given address and size.
    pub fn remote_ohow(&self, addr: usize, size: usize) -> Result<nix::libc::open_how, Errno> {
        if size != std::mem::size_of::<nix::libc::open_how>() {
            return Err(Errno::EINVAL);
        }
        let mut buf = [0u8; std::mem::size_of::<nix::libc::open_how>()];
        if let Err(error) = self.read_mem(&mut buf, addr) {
            // For EFAULT we assume NULL, and return a struct with all zeros.
            if error != Errno::EFAULT {
                return Err(error);
            }
        }
        // SAFETY: The following unsafe block assumes that:
        // 1. The memory layout of open_how in our Rust environment matches that of the target process.
        // 2. The proc.read_mem call has populated buf with valid data of the appropriate size (ensured by the size check above).
        // 3. The buffer is appropriately aligned for reading an open_how struct. If the remote process's representation of open_how
        //    was correctly aligned, our local buffer should be too, since it's an array on the stack.
        Ok(unsafe { std::ptr::read_unaligned(buf.as_ptr() as *const _) })
    }

    /// Read the path from memory of the process with the given `Pid` with the given address.
    fn remote_path(&self, pid: Pid, addr: usize) -> Result<Box<CStr>, Errno> {
        let mut buf = [0u8; nix::libc::PATH_MAX as usize];
        if let Err(err) = self.read_mem(&mut buf, addr) {
            // For EFAULT we assume NULL, and return an empty string.
            if err != Errno::EFAULT {
                return Err(Errno::last());
            }
        }

        // Handle /proc/self.
        let _ = crate::proc_self(pid, &mut buf);

        Ok(Box::from(
            CStr::from_bytes_until_nul(&buf)
                .map_err(|_| Errno::EINVAL)?
                .to_owned(),
        ))
    }
}

impl Drop for RemoteProcess {
    fn drop(&mut self) {
        let _ = close(self.fd);
    }
}

type ChildHandle = JoinHandle<Result<(), Errno>>;
type Child = (Pid, ChildHandle, ThreadPool);
type UserHookFunc = Box<dyn Fn(&UNotifyEventRequest) -> libseccomp::ScmpNotifResp + Send + Sync>;

/// The main component of greenhook.
pub struct Supervisor {
    export: Option<ExportMode>,
    handlers: HashMap<ScmpSyscall, Arc<UserHookFunc>>,
    sysallow: HashSet<ScmpSyscall>,
    socket_pair: SocketPair,
    thread_pool: ThreadPool,
    sandbox: Arc<RwLock<Sandbox>>,
}

macro_rules! loop_while_eintr {
    ($poll_expr:expr) => {
        loop {
            match $poll_expr {
                Ok(nfds) => break Ok(nfds),
                Err(Errno::EINTR) => (),
                Err(e) => break Err(e),
            }
        }
    };
}

impl Supervisor {
    /// Create a new `Supervisor` object. You can specify the number of threads in the thread pool.
    /// This function will also check your kernel version and show warning or return error if necessary.
    ///
    /// # Examples
    /// ```
    /// use syd::{hook::Supervisor, sandbox::Sandbox};
    /// let supervisor = Supervisor::new(Sandbox::default(), num_cpus::get(), None).unwrap();
    /// ```
    pub fn new(
        sandbox: Sandbox,
        thread_num: usize,
        export_mode: Option<ExportMode>,
    ) -> Result<Self, io::Error> {
        if thread_num == 0 {
            return Err(io::Error::new(
                io::ErrorKind::InvalidInput,
                "thread_num should be greater than 0",
            ));
        }
        // detect kernel version and show warning
        let version = uname().map_err(|e| io::Error::from_raw_os_error(e as i32))?;
        let version = version.release();

        macro_rules! parse_error {
            () => {
                io::Error::new(io::ErrorKind::Other, "unknown version")
            };
        }

        let (major, minor) = {
            let mut iter = version.to_str().ok_or_else(|| parse_error!())?.split('.');
            let major = iter
                .next()
                .ok_or_else(|| parse_error!())?
                .parse::<u32>()
                .map_err(|_| parse_error!())?;
            let minor = iter
                .next()
                .ok_or_else(|| parse_error!())?
                .parse::<u32>()
                .map_err(|_| parse_error!())?;
            (major, minor)
        };
        if major < 5 {
            error!("ctx": "check_kernel", "err": "Your kernel version is too old.");
            return Err(io::Error::new(io::ErrorKind::Other, "kernel too old"));
        } else if major == 5 && minor < 5 {
            error!("ctx": "check_kernel", "err": "Your kernel version is too old (Does not support SECCOMP_USER_NOTIF_FLAG_CONTINUE, etc.).");
            return Err(io::Error::new(io::ErrorKind::Other, "kernel too old"));
        } else if major == 5 && minor < 6 {
            error!("ctx": "check_kernel", "err": "Your kernel version is too old (Does not support pidfd_getfd() and SECCOMP_IOCTL_NOTIF_ADDFD).");
            return Err(io::Error::new(io::ErrorKind::Other, "kernel too old"));
        } /* else if major == 5 && minor < 9 {
              error!("ctx": "check_kernel", "err": "Your kernel version is too old (Does not support SECCOMP_IOCTL_NOTIF_ADDFD).");
              return Err(io::Error::new(io::ErrorKind::Other, "kernel too old"));
          } */
        Ok(Supervisor {
            export: export_mode,
            socket_pair: SocketPair::init()?,
            handlers: HashMap::new(),
            sysallow: HashSet::new(),
            thread_pool: ThreadPool::with_name(module_path!().to_string(), thread_num),
            sandbox: Arc::new(RwLock::new(sandbox)),
        })
    }

    /// Initialize the environment for the sandbox.
    /// Call this before `init`.
    pub fn init_env() -> anyhow::Result<()> {
        // SAFETY: The nix::sys::signal::signal function is unsafe because it affects the global state
        // of the program by changing how a signal (SIGPIPE in this case) is handled. It's safe to call
        // here because changing the SIGPIPE signal to its default behavior will not interfere with any
        // other part of this program that could be relying on a custom SIGPIPE signal handler.
        unsafe {
            nix::sys::signal::signal(
                nix::sys::signal::Signal::SIGPIPE,
                nix::sys::signal::SigHandler::SigDfl,
            )
        }
        .context("Failed to set SIGPIPE signal handler to default.")?;

        let _ = nix::sys::stat::umask(
            Mode::from_bits(0o077).context("Failed to set file mode creation mask.")?,
        );

        JsonLinesLogger::init().context("Failed to initialize logging.")?;

        Ok(())
    }

    /// Initilizes the supervisor by adding the system call handlers.
    pub fn init(&mut self) -> &mut Self {
        // network sandboxing
        self.insert_handler(ScmpSyscall::new("bind"), sys_bind);
        self.insert_handler(ScmpSyscall::new("connect"), sys_connect);
        self.insert_handler(ScmpSyscall::new("recvfrom"), sys_recvfrom);
        self.insert_handler(ScmpSyscall::new("sendto"), sys_sendto);
        self.insert_handler(ScmpSyscall::new("getsockname"), sys_getsockname);

        // exec sandboxing
        self.insert_handler(ScmpSyscall::new("execve"), sys_execve);
        self.insert_handler(ScmpSyscall::new("execveat"), sys_execveat);

        // stat sandboxing
        self.insert_handler(ScmpSyscall::new("chdir"), sys_chdir);
        self.insert_handler(ScmpSyscall::new("fchdir"), sys_fchdir);
        self.insert_handler(ScmpSyscall::new("getdents"), sys_getdents);
        self.insert_handler(ScmpSyscall::new("getdents64"), sys_getdents);
        self.insert_handler(ScmpSyscall::new("stat"), sys_stat);
        self.insert_handler(ScmpSyscall::new("fstat"), sys_fstat);
        self.insert_handler(ScmpSyscall::new("lstat"), sys_lstat);
        self.insert_handler(ScmpSyscall::new("statx"), sys_statx);
        self.insert_handler(ScmpSyscall::new("newfstatat"), sys_newfstatat);

        // read/write sandboxing
        self.insert_handler(ScmpSyscall::new("access"), sys_access);
        self.insert_handler(ScmpSyscall::new("acct"), sys_acct);
        self.insert_handler(ScmpSyscall::new("faccessat"), sys_faccessat);
        self.insert_handler(ScmpSyscall::new("faccessat2"), sys_faccessat2);
        self.insert_handler(ScmpSyscall::new("chmod"), sys_chmod);
        self.insert_handler(ScmpSyscall::new("fchmodat"), sys_fchmodat);
        self.insert_handler(ScmpSyscall::new("chown"), sys_chown);
        self.insert_handler(ScmpSyscall::new("fchownat"), sys_fchownat);
        self.insert_handler(ScmpSyscall::new("creat"), sys_creat);
        self.insert_handler(ScmpSyscall::new("link"), sys_link);
        self.insert_handler(ScmpSyscall::new("symlink"), sys_symlink);
        self.insert_handler(ScmpSyscall::new("unlink"), sys_unlink);
        self.insert_handler(ScmpSyscall::new("linkat"), sys_linkat);
        self.insert_handler(ScmpSyscall::new("symlinkat"), sys_symlinkat);
        self.insert_handler(ScmpSyscall::new("unlinkat"), sys_unlinkat);
        self.insert_handler(ScmpSyscall::new("mkdir"), sys_mkdir);
        self.insert_handler(ScmpSyscall::new("rmdir"), sys_rmdir);
        self.insert_handler(ScmpSyscall::new("mkdirat"), sys_mkdirat);
        self.insert_handler(ScmpSyscall::new("mknod"), sys_mknod);
        self.insert_handler(ScmpSyscall::new("mknodat"), sys_mknodat);
        self.insert_handler(ScmpSyscall::new("mount"), sys_mount);
        self.insert_handler(ScmpSyscall::new("umount"), sys_umount);
        self.insert_handler(ScmpSyscall::new("umount2"), sys_umount2);
        self.insert_handler(ScmpSyscall::new("open"), sys_open);
        self.insert_handler(ScmpSyscall::new("openat"), sys_openat);
        self.insert_handler(ScmpSyscall::new("openat2"), sys_openat2);
        self.insert_handler(ScmpSyscall::new("rename"), sys_rename);
        self.insert_handler(ScmpSyscall::new("renameat"), sys_renameat);
        self.insert_handler(ScmpSyscall::new("renameat2"), sys_renameat2);
        self.insert_handler(ScmpSyscall::new("utime"), sys_utime);
        self.insert_handler(ScmpSyscall::new("utimes"), sys_utimes);
        self.insert_handler(ScmpSyscall::new("futimesat"), sys_futimesat);
        self.insert_handler(ScmpSyscall::new("utimensat"), sys_utimensat);
        self.insert_handler(ScmpSyscall::new("truncate"), sys_truncate);
        self.insert_handler(ScmpSyscall::new("truncate64"), sys_truncate);
        self.insert_handler(ScmpSyscall::new("getxattr"), sys_getxattr);
        self.insert_handler(ScmpSyscall::new("setxattr"), sys_setxattr);
        self.insert_handler(ScmpSyscall::new("fsetxattr"), sys_fsetxattr);
        self.insert_handler(ScmpSyscall::new("lsetxattr"), sys_lsetxattr);
        self.insert_handler(ScmpSyscall::new("listxattr"), sys_listxattr);
        self.insert_handler(ScmpSyscall::new("flistxattr"), sys_flistxattr);
        self.insert_handler(ScmpSyscall::new("llistxattr"), sys_llistxattr);
        self.insert_handler(ScmpSyscall::new("removexattr"), sys_removexattr);
        self.insert_handler(ScmpSyscall::new("fremovexattr"), sys_fremovexattr);
        self.insert_handler(ScmpSyscall::new("lremovexattr"), sys_lremovexattr);

        // Allowlist safe system calls.
        for sysname in SAFE_SYSCALLS.iter().take(SAFE_SYSCALLS_SIZE) {
            let syscall = ScmpSyscall::new(sysname);
            if i32::from(syscall) == libseccomp_sys::__NR_SCMP_ERROR {
                error!("ctx": "allowlist_safe_syscall", "err": "scmp_error", "sys": sysname);
                continue;
            }
            self.allow_syscall(syscall);
        }

        self
    }

    /// Insert this system call to the list of allowed system calls.
    /// No filtering is done one these system calls and they're allowed at the kernel level.
    pub fn allow_syscall(&mut self, syscall: ScmpSyscall) {
        self.sysallow.insert(syscall);
    }

    ///
    /// # Examples
    ///
    /// ```no_run
    /// use libseccomp::ScmpSyscall;
    /// use syd::{
    ///     hook::{Supervisor, UNotifyEventRequest},
    ///     sandbox::Sandbox,
    /// };
    ///
    /// fn close_handler(req: &UNotifyEventRequest) -> libseccomp::ScmpNotifResp {
    ///     println!("close");
    ///     unsafe { req.continue_syscall() }
    /// }
    ///
    /// let mut supervisor = Supervisor::new(Sandbox::default(), num_cpus::get(), None).unwrap();
    /// supervisor.insert_handler(ScmpSyscall::new("open"), |req| {
    ///     println!("open: {}", req.get_request().data.args[0]);
    ///     unsafe { req.continue_syscall() }
    /// });
    /// supervisor.insert_handler(ScmpSyscall::new("close"), close_handler);
    /// ```
    pub fn insert_handler(
        &mut self,
        syscall: ScmpSyscall,
        handler: impl Fn(&UNotifyEventRequest) -> libseccomp::ScmpNotifResp + Send + Sync + 'static,
    ) {
        self.handlers.insert(syscall, Arc::new(Box::new(handler)));
    }

    /// Run a command with seccomp filter.
    /// This method will fork a child process, do some preparations and run the command in it.
    /// It returns a `Pid`, a `JoinHandle` of supervising thread, and a `ThreadPool` handle of syscall user functions.
    /// It's recommended to use `Supervisor::wait()` to wait for the child process.
    pub fn spawn(self, command: &mut Command) -> Result<Child, Errno> {
        let (sender, receiver) = self.socket_pair.channel();

        let pid = match unsafe { nix::unistd::fork() }.map_err(|_| Errno::last())? {
            ForkResult::Parent { child, .. } => {
                // Ignore some signals to ensure uniterrupted tracing.
                let _ = crate::ignore_signal(SIGTSTP);
                let _ = crate::ignore_signal(SIGTTIN);
                let _ = crate::ignore_signal(SIGTTOU);
                let _ = crate::ignore_signal(SIGHUP);
                child
            }
            ForkResult::Child => {
                let _ = close(self.socket_pair.receiver);
                let result = self.exec_command(command, &sender);
                let _ = close(self.socket_pair.sender);
                let lerrno = Errno::last();
                if let Err(error) = result {
                    eprintln!("exec: {error}");
                }
                // SAFETY: In libc, we trust.
                unsafe { nix::libc::_exit(lerrno as i32) };
            }
        };
        let _ = close(self.socket_pair.sender);
        let fd = match receiver.recvfd() {
            Ok(fd) => fd,
            Err(_) => {
                // We have to wait for the child here and get the correct return code,
                // otherwise we'll return a bogus error like EBADF which is hard to
                // understand in the context of execution of a command. This way we
                // also don't leave zombies behind to be reaped by init which is good.
                let mut status = 0;
                loop {
                    match unsafe { nix::libc::waitpid(pid.into(), &mut status, 0) } {
                        -1 if nix::errno::errno() == nix::libc::EINTR => continue,
                        -1 => return Err(Errno::last()),
                        _ => break,
                    }
                }
                return Err(Errno::from_i32(
                    ExitStatus::from_raw(status)
                        .code()
                        .unwrap_or(nix::libc::EFAULT),
                ));
            }
        };
        debug!("ctx": "spawn_recvfd", "fd": fd);

        let mut sandbox = self.sandbox.write();
        sandbox.set_child_pid(pid);
        drop(sandbox);
        debug!("ctx": "set_child_pid", "pid": pid.as_raw());

        self.supervise(pid, fd)
    }

    fn exec_command(&self, command: &mut Command, sender: &Sender) -> IOResult<()> {
        let ctx = self.setup_seccomp()?;
        ctx.load().map_err(|error| {
            io::Error::new(
                io::ErrorKind::Other,
                format!(
                    "failed to load seccomp filter: {}, {}.",
                    error,
                    Errno::last(),
                ),
            )
        })?;

        let ufd = ctx.get_notify_fd().map_err(|e| {
            io::Error::new(
                io::ErrorKind::Other,
                format!("failed to get notify fd: {}", e),
            )
        })?;
        sender.sendfd(ufd)?;
        close(ufd)?;

        Err(command
            .stderr(Stdio::inherit())
            .stdin(Stdio::inherit())
            .stdout(Stdio::inherit())
            .exec())
    }

    fn setup_seccomp(&self) -> IOResult<ScmpFilterContext> {
        let mut ctx =
            ScmpFilterContext::new_filter(ScmpAction::Errno(nix::libc::EACCES)).map_err(|e| {
                io::Error::new(
                    io::ErrorKind::Other,
                    format!("failed to create seccomp filter: {}", e),
                )
            })?;
        // We don't want ECANCELED, we want actual errnos.
        let _ = ctx.set_api_sysrawrc(true);
        // We deny with EACCES for bad system call, and ENOSYS for bad arch.
        let _ = ctx.set_act_badarch(ScmpAction::Errno(nix::libc::ENOSYS));
        // We log all filter actions other than Allow to kernel log if the log level is <=Debug.
        if log_enabled!(Level::Debug) {
            let _ = ctx.set_ctl_log(true);
        }

        // Fakeroot
        let sandbox = self.sandbox.read();
        let fakeroot = sandbox.get_root();
        drop(sandbox);
        let id_action = if fakeroot {
            ScmpAction::Errno(0)
        } else {
            ScmpAction::Allow
        };
        for sysname in ID_SYSCALLS.iter().take(ID_SYSCALLS_SIZE) {
            let syscall = ScmpSyscall::new(sysname);
            ctx.add_rule(id_action, syscall).map_err(|e| {
                io::Error::new(
                    io::ErrorKind::Other,
                    format!(
                        "failed to add rule for system call {} ( {} ): {}",
                        syscall, sysname, e
                    ),
                )
            })?;
        }

        // Add notify rules for system calls with handlers.
        let syscall_notif: Vec<_> = self.handlers.keys().copied().collect();
        for syscall in &syscall_notif {
            // This is a verified handler, the name must exist, calling unwrap is OK.
            #[allow(clippy::disallowed_methods)]
            let name = syscall.get_name().unwrap();
            let _ = ctx.set_syscall_priority(*syscall, crate::syscall_priority(&name));
            ctx.add_rule(ScmpAction::Notify, *syscall).map_err(|e| {
                io::Error::new(
                    io::ErrorKind::Other,
                    format!(
                        "failed to add rule for system call {} ( {} ): {}",
                        *syscall,
                        ScmpSyscall::get_name(*syscall).unwrap_or("?".to_string()),
                        e
                    ),
                )
            })?;
        }

        // Add allow rules for system calls in the default allow list.
        let syscall_allow: Vec<_> = self.sysallow.iter().copied().collect();
        for syscall in &syscall_allow {
            ctx.add_rule(ScmpAction::Allow, *syscall).map_err(|e| {
                io::Error::new(
                    io::ErrorKind::Other,
                    format!(
                        "failed to add rule for system call {} ( {} ): {}",
                        *syscall,
                        ScmpSyscall::get_name(*syscall).unwrap_or("?".to_string()),
                        e
                    ),
                )
            })?;
        }

        // Export seccomp rules if requested.
        if let Some(mode) = self.export {
            match mode {
                ExportMode::BerkeleyPacketFilter => ctx.export_bpf(&mut io::stdout()),
                ExportMode::PseudoFiltercode => ctx.export_pfc(&mut io::stdout()),
            }
            .map_err(|error| {
                io::Error::new(
                    io::ErrorKind::Other,
                    format!("failed to export rules in {mode:?} format: {error}"),
                )
            })?;
        }

        Ok(ctx)
    }

    /// Logic for the supervise child thread.
    fn supervise(self, pid: Pid, fd: i32) -> Result<Child, Errno> {
        let pool_handle = self.thread_pool.clone();
        let thread_handle = Builder::new().name("syd-box".into()).spawn(move || {
            loop {
                self.poll_fd(fd)?;
                let req = ScmpNotifReq::receive(fd);
                if let Ok(req) = req {
                    if !self.syscall(fd, req) {
                        // Handling system call failed, break.
                        return Ok(())
                    }
                } else {
                    let errno = Errno::last();
                    error!("ctx": "seccomp_notif_recv", "errno": errno as i32, "err": format!("{:?}", req.err()));
                    return Err(errno);
                }
            }
        }).map_err(|_| Errno::last())?;

        Ok((pid, thread_handle, pool_handle))
    }

    fn poll_fd(&self, fd: i32) -> Result<(), Errno> {
        let mut fd = [PollFd::new(fd, PollFlags::POLLIN)];
        let result = loop_while_eintr!(nix::poll::poll(&mut fd, -1));

        if let Err(err) = result {
            error!("ctx": "poll", "err": format!("{err}"));
            return Err(err);
        }

        match fd[0].revents() {
            None => {
                error!("ctx": "poll", "err": "unknown poll event");
                Err(Errno::EINVAL)
            }
            Some(events) if events.contains(PollFlags::POLLHUP) => Err(Errno::EINTR),
            _ => Ok(()),
        }
    }

    fn syscall(&self, fd: i32, req: ScmpNotifReq) -> bool {
        let event_req = UNotifyEventRequest::new(req, fd, Arc::clone(&self.sandbox));
        let syscall_id = event_req.get_request().data.syscall;

        match self.handlers.get(&syscall_id) {
            Some(handler) => {
                let handler_in_thread = Arc::clone(handler);
                self.thread_pool.execute(move || {
                    let response = handler_in_thread(&event_req);

                    if event_req.is_valid() {
                        if let Err(error) = response.respond(fd) {
                            let errno = Errno::last();
                            error!("ctx": "spawn_response", "err": format!("failed to send response: {error}, {errno}"));
                        }
                    } else {
                        info!("ctx": "spawn_response", "err": "failed to send response: invalid request");
                    }
                });
                true
            }
            None => {
                error!("ctx": "syscall_handle", "err": format!("got unknown syscall to handle: {syscall_id}"));
                match event_req.fail_syscall(libc::ENOSYS).respond(fd) {
                    Ok(_) => true,
                    Err(error) => {
                        let errno = Errno::last();
                        error!("ctx": "syscall_handle", "err": format!("failed to send response: {error}, {errno}"));
                        false
                    }
                }
            }
        }
    }

    /// Wait for the child process to exit and cleanup the supervisor thread and thread pool.
    /// It returns `WaitStatus` of the child process.
    ///
    /// # Examples
    ///
    /// ```ignore
    /// let status = Supervisor::wait(pid, thread_handle, pool).unwrap();
    /// ```
    pub fn wait(
        pid: Pid,
        thread_handle: ChildHandle,
        pool_handle: ThreadPool,
    ) -> Result<ExitStatus, Errno> {
        let mut status = 0;
        loop {
            match unsafe { nix::libc::waitpid(pid.into(), &mut status, 0) } {
                -1 if nix::errno::errno() == nix::libc::EINTR => continue,
                -1 => return Err(Errno::last()),
                _ => break,
            }
        }
        let _ = thread_handle.join().map_err(|_| Errno::EPIPE)?;
        pool_handle.join();
        Ok(ExitStatus::from_raw(status))
    }
}

/// Processes the address family of a `SockaddrStorage` object and performs logging or other
/// required operations specific to the syscall being handled.
///
/// This helper function isolates the logic involved in dealing with different address families
/// and reduces code duplication across different syscall handler functions.
///
/// # Parameters
///
/// - `addr`: Reference to a `SockaddrStorage`, representing the socket address involved in the syscall.
/// - `syscall_name`: A string slice holding the name of the syscall being handled, used for logging purposes.
///
/// # Safety
///
/// The function contains unsafe blocks due to potential TOCTOU (Time-of-Check Time-of-Use)
/// vulnerabilities. Each unsafe block within this function has been annotated with a detailed
/// safety comment to ensure that unsafe operations are used correctly and securely.
///
/// # Errors
///
/// The function returns an `io::Error` in cases where:
/// - The conversion from `SockaddrStorage` to a specific address family representation fails.
/// - Any other unexpected error condition occurs during the processing of the address family.
///
/// # Returns
///
/// Returns an `Result<(), Errno>`:
/// - `Ok(())` if the processing is successful.
/// - `Err(Errno)` containing a description of the error, if any error occurs during processing.
pub fn sandbox_addr(
    proc: &RemoteProcess,
    request: &UNotifyEventRequest,
    req: &ScmpNotifReq,
    addr: &SockaddrStorage,
    syscall_name: &str,
    caps: Capability,
) -> Result<(), Errno> {
    match addr.family() {
        Some(AddressFamily::Unix) => {
            sandbox_addr_unix(proc, request, req, addr, syscall_name, caps)
        }
        Some(AddressFamily::Inet) => {
            sandbox_addr_inet(proc, request, req, addr, syscall_name, caps)
        }
        Some(AddressFamily::Inet6) => {
            sandbox_addr_inet6(proc, request, req, addr, syscall_name, caps)
        }
        Some(_) | None => sandbox_addr_notsup(proc, request),
    }
}

/// Process a `AddressFamily::Unix` socket address.
pub fn sandbox_addr_unix(
    proc: &RemoteProcess,
    request: &UNotifyEventRequest,
    req: &ScmpNotifReq,
    addr: &SockaddrStorage,
    syscall_name: &str,
    caps: Capability,
) -> Result<(), Errno> {
    let addr = addr.as_unix_addr().ok_or(Errno::EINVAL)?;
    let (path, abs) = match (addr.path(), addr.as_abstract()) {
        (Some(path), _) => {
            let path = path.as_os_str().as_bytes();
            let null = path.iter().position(|&b| b == 0).unwrap_or(path.len());
            let path = String::from_utf8_lossy(&path[..null]);
            debug!("ctx": "syscall", "sys": syscall_name, "addr": format!("{path}"), "caps": caps.bits());
            (path, false)
        }
        (_, Some(path)) => {
            let null = path.iter().position(|&b| b == 0).unwrap_or(path.len());
            let path = String::from_utf8_lossy(&path[..null]);
            debug!("ctx": "syscall", "sys": syscall_name, "addr": format!("{path}"), "caps": caps.bits());
            (path, true)
        }
        _ => {
            // unnamed unix socket
            return Ok(());
        }
    };

    let path = if !path.starts_with('/') {
        let arg = SyscallPathArgument {
            dirfd: None,
            path: None,
            follow: true,
        };
        let mut dir = proc.read_directory(req, &arg)?;
        if !dir.ends_with('/') {
            dir.push('/');
        }
        Cow::Owned(format!("{dir}{path}"))
    } else {
        path
    };

    // Check for access.
    let sandbox = request.get_sandbox(false);
    let action = sandbox.check_unix(caps, path.as_ref());
    drop(sandbox);

    if action == Action::Deny {
        // Report access violation.
        warn!("ctx": "access", "caps": caps.bits(), "unix": path, "abs": abs, "pid": req.pid, "sys": syscall_name);
    }
    if action == Action::Allow {
        Ok(())
    } else {
        // Deny or Filter.
        Err(Errno::EPERM)
    }
}

/// Process an `AddressFamily::Inet` socket address.
pub fn sandbox_addr_inet(
    proc: &RemoteProcess,
    request: &UNotifyEventRequest,
    req: &ScmpNotifReq,
    addr: &SockaddrStorage,
    syscall_name: &str,
    caps: Capability,
) -> Result<(), Errno> {
    let addr = addr.as_sockaddr_in().ok_or(Errno::EINVAL)?;
    let port = addr.port();
    let addr = IpAddr::V4(Ipv4Addr::from(addr.ip()));
    debug!("ctx": "syscall", "sys": syscall_name, "addr": format!("{addr}@{port}"), "caps": caps.bits());

    // Check for access.
    let sandbox = request.get_sandbox(false);
    let action = sandbox.check_ip(caps, addr, port);
    drop(sandbox);

    if action == Action::Deny {
        // Report access violation.
        warn!("ctx": "access", "caps": caps.bits(), "ipv4": format!("{addr}"), "port": port, "pid": req.pid, "sys": syscall_name);
    }
    if action == Action::Allow {
        // allowlist/successful_bind
        if caps.contains(Capability::CAP_BIND) && port == 0 {
            let mut sandbox = request.get_sandbox(true);
            if sandbox.allow_successful_bind() {
                sandbox.insert_bind0(proc.get_pid(), addr);
            }
        }
        Ok(())
    } else {
        // Deny or Filter.
        Err(Errno::EPERM)
    }
}

/// Process an `AddressFamily::Inet6` socket address.
pub fn sandbox_addr_inet6(
    proc: &RemoteProcess,
    request: &UNotifyEventRequest,
    req: &ScmpNotifReq,
    addr: &SockaddrStorage,
    syscall_name: &str,
    caps: Capability,
) -> Result<(), Errno> {
    let addr = addr.as_sockaddr_in6().ok_or(Errno::EINVAL)?;
    let port = addr.port();
    let addr = IpAddr::V6(addr.ip());
    debug!("ctx": "syscall", "sys": syscall_name, "addr": format!("{addr}@{port}"), "caps": caps.bits());

    // Check for access.
    let sandbox = request.get_sandbox(false);
    let action = sandbox.check_ip(caps, addr, port);
    drop(sandbox);

    if action == Action::Deny {
        // Report access violation.
        warn!("ctx": "access", "caps": caps.bits(), "ipv6": format!("{addr}"), "port": port, "pid": req.pid, "sys": syscall_name);
    }
    if action == Action::Allow {
        // allowlist/successful_bind
        if caps.contains(Capability::CAP_BIND) && port == 0 {
            let mut sandbox = request.get_sandbox(true);
            if sandbox.allow_successful_bind() {
                sandbox.insert_bind0(proc.get_pid(), addr);
            }
        }
        Ok(())
    } else {
        // Deny or Filter.
        Err(Errno::EPERM)
    }
}

/// Process a socket address of an unsupported socket family.
pub fn sandbox_addr_notsup(
    _proc: &RemoteProcess,
    request: &UNotifyEventRequest,
) -> Result<(), Errno> {
    let sandbox = request.get_sandbox(false);
    let ok = sandbox.allow_unsupported_socket_families();
    drop(sandbox);

    if ok {
        Ok(())
    } else {
        Err(Errno::EAFNOSUPPORT)
    }
}

/// Process the first path argument.
pub fn sandbox_path_1(
    paths: NonEmpty<String>,
    caps: Capability,
    syscall_name: &str,
    request: &UNotifyEventRequest,
    req: &ScmpNotifReq,
    _: &RemoteProcess,
) -> Result<Option<ScmpNotifResp>, Errno> {
    let path = paths.first(); // paths are NonEmpty, so first always exists.
    debug!("ctx": "syscall", "sys": syscall_name, "caps": caps.bits(), "path": path);

    if caps.is_empty() {
        return Err(Errno::EINVAL);
    }

    // Special case for system calls that must create the path argument.
    // Note: dangling symbolic links are considered existing.
    if (syscall_name.starts_with("mkdir") || syscall_name.starts_with("mknod"))
        && lstat(Path::new(path)).is_ok()
    {
        debug!("ctx": "sandbox_path_1", "err": "EEXIST", "path": path);
        return Err(Errno::EEXIST);
    }

    let sandbox = request.get_sandbox(false);
    let mut action = Action::Allow;

    // Sandboxing
    if caps.contains(Capability::CAP_READ) {
        action = sandbox.check_path(Capability::CAP_READ, path);
    }
    if action == Action::Allow && caps.contains(Capability::CAP_STAT) {
        action = sandbox.check_path(Capability::CAP_STAT, path);
    }
    if action == Action::Allow && caps.contains(Capability::CAP_WRITE) {
        action = sandbox.check_path(Capability::CAP_WRITE, path);
    }
    if action == Action::Allow && caps.contains(Capability::CAP_EXEC) {
        action = sandbox.check_path(Capability::CAP_EXEC, path)
    }

    // exec/kill
    if caps.contains(Capability::CAP_EXEC) && sandbox.check_exec(path) == Action::Kill {
        warn!("ctx": "exec/kill", "caps": caps.bits(), "path": path, "pid": req.pid, "sys": syscall_name);
        #[allow(clippy::cast_possible_wrap)]
        let _ = kill(Pid::from_raw(req.pid as i32), Some(SIGKILL));
        action = Action::Kill;
    }

    match action {
        Action::Allow => Ok(None),
        _ if caps.contains(Capability::CAP_STAT) => {
            // We do not report violations for Stat capability because we are essentially hiding paths.
            Err(Errno::ENOENT)
        }
        Action::Filter | Action::Kill => Err(Errno::EPERM),
        Action::Deny => {
            // Report access violation if the path exists, otherwise drop an info level log.
            if Path::new(path).exists() {
                warn!("ctx": "access", "caps": caps.bits(), "path": path, "pid": req.pid, "sys": syscall_name, "x": true);
            } else {
                info!("ctx": "access", "caps": caps.bits(), "path": path, "pid": req.pid, "sys": syscall_name, "x": false);
            }
            Err(Errno::EPERM)
        }
    }
}

/// Process both the first and the second path argument.
pub fn sandbox_path_2(
    paths: NonEmpty<String>,
    caps: Capability,
    syscall_name: &str,
    request: &UNotifyEventRequest,
    req: &ScmpNotifReq,
    proc: &RemoteProcess,
) -> Result<Option<ScmpNotifResp>, Errno> {
    let (source, target) = (paths.first(), paths.last()); // paths are NonEmpty, so first/last always exists.
    debug!("ctx": "syscall", "sys": syscall_name, "caps": caps.bits(), "source": source, "target": target);

    sandbox_path_1(
        nonempty![source.to_string()],
        caps,
        syscall_name,
        request,
        req,
        proc,
    )?;
    sandbox_path_1(
        nonempty![target.to_string()],
        caps,
        syscall_name,
        request,
        req,
        proc,
    )?;
    Ok(None)
}

/*
 * System call handlers
 */
fn sys_bind(request: &UNotifyEventRequest) -> ScmpNotifResp {
    syscall_network_handler(request, 1, 2, false, "bind", sandbox_addr)
}

fn sys_connect(request: &UNotifyEventRequest) -> ScmpNotifResp {
    syscall_network_handler(request, 1, 2, false, "connect", sandbox_addr)
}

fn sys_recvfrom(request: &UNotifyEventRequest) -> ScmpNotifResp {
    syscall_network_handler(request, 4, 5, true, "recvfrom", sandbox_addr)
}

fn sys_sendto(request: &UNotifyEventRequest) -> ScmpNotifResp {
    syscall_network_handler(request, 4, 5, false, "sendto", sandbox_addr)
}

fn sys_getsockname(request: &UNotifyEventRequest) -> ScmpNotifResp {
    syscall_handler!(request, |req: &ScmpNotifReq, proc: &RemoteProcess| {
        let fd = proc.get_fd(req.data.args[0] as RawFd)?;
        let addr: SockaddrStorage = getsockname(fd)?;
        let _ = nix::unistd::close(fd);

        let addrlen = match addr.family() {
            Some(AddressFamily::Unix) => {
                let addr = addr.as_unix_addr().ok_or(Errno::EINVAL)?;
                #[cfg(debug_assertions)]
                match (addr.path(), addr.as_abstract()) {
                    (Some(path), _) => {
                        let path = path.as_os_str().as_bytes();
                        let null = path.iter().position(|&b| b == 0).unwrap_or(path.len());
                        let path = String::from_utf8_lossy(&path[..null]);
                        debug!("ctx": "syscall", "sys": "getsockname", "unix": path, "abs": false);
                    }
                    (_, Some(path)) => {
                        let null = path.iter().position(|&b| b == 0).unwrap_or(path.len());
                        let path = String::from_utf8_lossy(&path[..null]);
                        debug!("ctx": "syscall", "sys": "getsockname", "unix": path, "abs": true);
                    }
                    _ => { // unnamed unix socket
                    }
                }
                addr.len()
            }
            Some(AddressFamily::Inet) => {
                let addr = addr.as_sockaddr_in().ok_or(Errno::EINVAL)?;
                let port = addr.port();
                let alen = addr.len();
                let addr = IpAddr::V4(Ipv4Addr::from(addr.ip()));
                debug!("ctx": "syscall", "sys": "getsockname", "ipv4": format!("{addr}"), "port": port);

                // Handle allowlist/successful_bind
                let mut sandbox = request.get_sandbox(true);
                let bind0 = sandbox.contains_bind0(proc.get_pid(), addr);
                if bind0 {
                    let cmd = format!("allowlist/net/connect+{addr}@{port}");
                    info!("ctx": "allowlist_successful_bind", "cmd": cmd);
                    sandbox.remove_bind0(proc.get_pid(), addr);
                    sandbox.config(&cmd)?;
                }
                drop(sandbox);

                alen
            }
            Some(AddressFamily::Inet6) => {
                let addr = addr.as_sockaddr_in6().ok_or(Errno::EINVAL)?;
                let port = addr.port();
                let alen = addr.len();
                let addr = IpAddr::V6(addr.ip());
                debug!("ctx": "syscall", "sys": "getsockname", "ipv6": format!("{addr}"), "port": port);

                // Handle allowlist/successful_bind
                let mut sandbox = request.get_sandbox(true);
                let bind0 = sandbox.contains_bind0(proc.get_pid(), addr);
                if bind0 {
                    let cmd = format!("allowlist/net/connect+{addr}@{port}");
                    info!("ctx": "allowlist_successful_bind", "cmd": cmd);
                    sandbox.remove_bind0(proc.get_pid(), addr);
                    sandbox.config(&cmd)?;
                }
                drop(sandbox);

                alen
            }
            Some(_) | None => {
                // unsupported socket
                // SAFETY: This is unsafe because it is vulnerable to TOCTOU.
                return unsafe { Ok(request.continue_syscall()) };
            }
        };

        // SAFETY: It is crucial that `addr.as_ptr()` points to a valid memory location of at least `addrlen` bytes.
        // Moreover, the memory should be properly aligned for `*const u8`. The memory at `addr.as_ptr()` must not be
        // modified by any other code while this slice is alive, to avoid data races. Additionally, there should be
        // no concurrent writes to the memory location from other threads while creating this slice.
        let addr =
            unsafe { std::slice::from_raw_parts(addr.as_ptr() as *const u8, addrlen as usize) };
        #[allow(clippy::cast_possible_truncation)]
        proc.write_mem(addr, req.data.args[1] as usize)?;
        if !request.is_valid() {
            return Ok(request.fail_syscall(nix::libc::EACCES));
        }

        // SAFETY: We are using the `any_as_u8_slice` function to obtain a byte slice from
        // `addrlen`. We must ensure that the lifetime of this slice does not exceed `addrlen`,
        // and that the memory representation of `addrlen` is correctly interpreted when accessed
        // as bytes. Moreover, the caller of this surrounding function should ensure that it is
        // safe to write these bytes to the memory location pointed to by `req.data.args[2]`. It is
        // also assumed that no other parts of the program will simultaneously modify `addrlen`.
        let addrlen = unsafe { any_as_u8_slice(&addrlen) };
        #[allow(clippy::cast_possible_truncation)]
        proc.write_mem(addrlen, req.data.args[2] as usize)?;
        if !request.is_valid() {
            return Ok(request.fail_syscall(nix::libc::EACCES));
        }

        Ok(request.return_syscall(0))
    })
}

fn sys_execve(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        follow: true,
    });
    syscall_path_handler(request, "execve", argv, sandbox_path_1)
}

fn sys_execveat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: Some(0),
        path: Some(1),
        follow: true,
    });
    syscall_path_handler(request, "execveat", argv, sandbox_path_1)
}

fn sys_chdir(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        follow: true,
    });
    syscall_path_handler(request, "chdir", argv, sandbox_path_1)
}

fn sys_fchdir(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: Some(0),
        path: None,
        follow: true,
    });
    syscall_path_handler(request, "fchdir", argv, sandbox_path_1)
}

fn sys_getdents(request: &UNotifyEventRequest) -> ScmpNotifResp {
    syscall_handler!(request, |req: &ScmpNotifReq, proc: &RemoteProcess| {
        // If sandboxing for List capability is off, return immediately.
        let sandbox = request.get_sandbox(false);
        let check = sandbox.enabled(Capability::CAP_STAT);
        drop(sandbox); // release the read lock.
        if !check {
            // SAFETY: This is unsafe due to vulnerability to TOCTOU.
            return unsafe { Ok(request.continue_syscall()) };
        }

        let fd = proc.get_fd(req.data.args[0] as RawFd)?;
        let mut dir = match crate::fs::canonicalize(
            format!("/proc/self/fd/{fd}"),
            true,
            crate::fs::MissingHandling::Missing,
        ) {
            Ok(dir) => dir.to_string_lossy().to_string(),
            Err(error) => {
                error!("ctx": "path_canonicalize", "fd": fd, "err": format!("{error}"));
                let _ = nix::unistd::close(fd);
                return Err(Errno::ENOENT);
            }
        };
        if !dir.ends_with('/') {
            dir.push('/');
        }
        #[allow(clippy::cast_possible_truncation)]
        let count = req.data.args[2] as usize;

        let mut entries = Vec::new();
        loop {
            match getdents(fd, count) {
                Err(error) => {
                    let _ = nix::unistd::close(fd);
                    return Err(error);
                }
                Ok(None) => {
                    let _ = nix::unistd::close(fd);
                    // getdents returned None, there are no more entries.
                    return Ok(request.return_syscall(0));
                }
                Ok(Some(e)) => {
                    for entry in e.iter() {
                        let name = entry.name().to_string_lossy().to_string();
                        let path = format!("{dir}{name}");
                        match sandbox_path_1(
                            nonempty![path.clone()],
                            Capability::CAP_STAT,
                            "getdents",
                            request,
                            req,
                            proc,
                        ) {
                            Ok(None) /*allow*/ => {
                                trace!("ctx": "check_stat_allow", "path": path);
                                entries.push(entry.clone());
                            },
                            Ok(_) if name == "." /*deny the whole dir */ => {
                                trace!("ctx": "check_stat_deny", "path": path);
                                let _ = nix::unistd::close(fd);
                                return Ok(request.fail_syscall(nix::libc::ENOENT))
                            }
                            _ => {
                                /* this entry is denied, skip it. */
                                trace!("ctx": "check_stat_hide", "path": path);
                            },
                        };
                    }
                    if !entries.is_empty() {
                        break; // exit the loop once we have allowed entries
                    }
                }
            };
        }

        let _ = nix::unistd::close(fd);
        let mut buffer = Vec::new();

        for entry in &entries {
            let bytes = &entry.dirent;

            // Ensure we don't append more bytes than the buffer can hold.
            if buffer.len().saturating_add(bytes.len()) > count {
                break;
            }

            buffer.extend_from_slice(bytes);
        }

        #[allow(clippy::cast_possible_truncation)]
        proc.write_mem(&buffer, req.data.args[1] as usize)?;

        #[allow(clippy::cast_possible_wrap)]
        Ok(request.return_syscall(buffer.len() as i64))
    })
}

fn sys_access(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        follow: true,
    });
    syscall_path_handler(request, "access", argv, sandbox_path_1)
}

fn sys_acct(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        follow: true,
    });
    syscall_path_handler(request, "acct", argv, sandbox_path_1)
}

fn sys_faccessat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let req = request.get_request();
    #[allow(clippy::cast_possible_truncation)]
    let follow = req.data.args[3] as i32 & nix::libc::AT_SYMLINK_NOFOLLOW == 0;
    let argv = NonEmpty::new(SyscallPathArgument {
        follow,
        dirfd: Some(0),
        path: Some(1),
    });
    syscall_path_handler(request, "faccessat", argv, sandbox_path_1)
}

fn sys_faccessat2(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let req = request.get_request();
    #[allow(clippy::cast_possible_truncation)]
    let follow = req.data.args[3] as i32 & nix::libc::AT_SYMLINK_NOFOLLOW == 0;
    let argv = NonEmpty::new(SyscallPathArgument {
        follow,
        dirfd: Some(0),
        path: Some(1),
    });
    syscall_path_handler(request, "faccessat2", argv, sandbox_path_1)
}

fn sys_chmod(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        follow: true,
    });
    syscall_path_handler(request, "chmod", argv, sandbox_path_1)
}

fn sys_fchmodat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let req = request.get_request();
    #[allow(clippy::cast_possible_truncation)]
    let follow = req.data.args[3] as i32 & nix::libc::AT_SYMLINK_NOFOLLOW == 0;
    let argv = NonEmpty::new(SyscallPathArgument {
        follow,
        dirfd: Some(0),
        path: Some(1),
    });
    syscall_path_handler(request, "fchmodat", argv, sandbox_path_1)
}

fn sys_chown(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        follow: true,
    });
    syscall_path_handler(request, "chown", argv, sandbox_path_1)
}

fn sys_fchownat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let req = request.get_request();
    #[allow(clippy::cast_possible_truncation)]
    let follow = req.data.args[4] as i32 & nix::libc::AT_SYMLINK_NOFOLLOW == 0;
    let argv = NonEmpty::new(SyscallPathArgument {
        follow,
        dirfd: Some(0),
        path: Some(1),
    });
    syscall_path_handler(request, "fchmownat", argv, sandbox_path_1)
}

fn sys_creat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        follow: true,
    });
    syscall_path_handler(request, "creat", argv, sandbox_path_1)
}

fn sys_link(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = nonempty![
        SyscallPathArgument {
            dirfd: None,
            path: Some(0),
            follow: false,
        },
        SyscallPathArgument {
            dirfd: None,
            path: Some(1),
            follow: false,
        }
    ];
    syscall_path_handler(request, "link", argv, sandbox_path_2)
}

fn sys_symlink(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: None,
        path: Some(1),
        follow: false,
    });
    syscall_path_handler(request, "symlink", argv, sandbox_path_1)
}

fn sys_unlink(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        follow: false,
    });
    syscall_path_handler(request, "unlink", argv, sandbox_path_1)
}

fn sys_linkat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let req = request.get_request();
    #[allow(clippy::cast_possible_truncation)]
    let follow = req.data.args[4] as i32 & nix::libc::AT_SYMLINK_FOLLOW != 0;
    let argv = nonempty![
        SyscallPathArgument {
            follow,
            dirfd: Some(0),
            path: Some(2),
        },
        SyscallPathArgument {
            dirfd: Some(1),
            path: Some(3),
            follow: false,
        }
    ];
    syscall_path_handler(request, "linkat", argv, sandbox_path_2)
}

fn sys_symlinkat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: Some(1),
        path: Some(2),
        follow: false,
    });
    syscall_path_handler(request, "symlinkat", argv, sandbox_path_1)
}

fn sys_unlinkat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: Some(0),
        path: Some(1),
        follow: false,
    });
    syscall_path_handler(request, "unlinkat", argv, sandbox_path_1)
}

fn sys_mkdir(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        follow: true,
    });
    syscall_path_handler(request, "mkdir", argv, sandbox_path_1)
}

fn sys_rmdir(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        follow: true,
    });
    syscall_path_handler(request, "rmdir", argv, sandbox_path_1)
}

fn sys_mkdirat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: Some(0),
        path: Some(1),
        follow: true,
    });
    syscall_path_handler(request, "mkdirat", argv, sandbox_path_1)
}

fn sys_mknod(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        follow: true,
    });
    syscall_path_handler(request, "mknod", argv, sandbox_path_1)
}

fn sys_mknodat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: Some(0),
        path: Some(1),
        follow: true,
    });
    syscall_path_handler(request, "mknodat", argv, sandbox_path_1)
}

fn sys_mount(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = nonempty![
        SyscallPathArgument {
            dirfd: None,
            path: Some(0),
            follow: false,
        },
        SyscallPathArgument {
            dirfd: None,
            path: Some(1),
            follow: false,
        }
    ];
    syscall_path_handler(request, "mount", argv, sandbox_path_2)
}

fn sys_umount(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        follow: true,
    });
    syscall_path_handler(request, "umount", argv, sandbox_path_1)
}

fn sys_umount2(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let req = request.get_request();
    #[allow(clippy::cast_possible_truncation)]
    let follow = req.data.args[1] as i32 & nix::libc::UMOUNT_NOFOLLOW == 0;
    let argv = NonEmpty::new(SyscallPathArgument {
        follow,
        dirfd: None,
        path: Some(0),
    });
    syscall_path_handler(request, "umount2", argv, sandbox_path_1)
}

fn sys_open(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        follow: true,
    });
    syscall_path_handler(request, "open", argv, sandbox_path_1)
}

fn sys_openat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let req = request.get_request();
    #[allow(clippy::cast_possible_truncation)]
    let flags = OFlag::from_bits_truncate(req.data.args[2] as nix::libc::c_int);
    let follow = !flags.contains(OFlag::O_NOFOLLOW);
    let argv = NonEmpty::new(SyscallPathArgument {
        follow,
        dirfd: Some(0),
        path: Some(1),
    });
    syscall_path_handler(request, "openat", argv, sandbox_path_1)
}

fn sys_openat2(request: &UNotifyEventRequest) -> ScmpNotifResp {
    // TODO: set follow based on the open_how struct.
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: Some(0),
        path: Some(1),
        follow: true,
    });
    syscall_path_handler(request, "openat2", argv, sandbox_path_1)
}

fn sys_rename(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = nonempty![
        SyscallPathArgument {
            dirfd: None,
            path: Some(0),
            follow: false,
        },
        SyscallPathArgument {
            dirfd: None,
            path: Some(1),
            follow: false,
        }
    ];
    syscall_path_handler(request, "rename", argv, sandbox_path_2)
}

fn sys_renameat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = nonempty![
        SyscallPathArgument {
            dirfd: Some(0),
            path: Some(2),
            follow: false,
        },
        SyscallPathArgument {
            dirfd: Some(1),
            path: Some(3),
            follow: false,
        }
    ];
    syscall_path_handler(request, "renameat", argv, sandbox_path_2)
}

fn sys_renameat2(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = nonempty![
        SyscallPathArgument {
            dirfd: Some(0),
            path: Some(2),
            follow: false,
        },
        SyscallPathArgument {
            dirfd: Some(1),
            path: Some(3),
            follow: false,
        }
    ];
    syscall_path_handler(request, "renameat2", argv, sandbox_path_2)
}

fn sys_stat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        follow: true,
    });
    syscall_stat_handler(request, "stat", argv)
}

fn sys_fstat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: Some(0),
        path: None,
        follow: true,
    });
    syscall_stat_handler(request, "fstat", argv)
}

fn sys_lstat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        follow: false,
    });
    syscall_stat_handler(request, "lstat", argv)
}

fn sys_statx(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: Some(0),
        path: Some(1),
        follow: true,
    });
    syscall_stat_handler(request, "statx", argv)
}

fn sys_newfstatat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let req = request.get_request();
    #[allow(clippy::cast_possible_truncation)]
    let follow = req.data.args[3] as i32 & nix::libc::AT_SYMLINK_NOFOLLOW == 0;
    let argv = NonEmpty::new(SyscallPathArgument {
        follow,
        dirfd: Some(0),
        path: Some(1),
    });
    syscall_stat_handler(request, "newfstatat", argv)
}

fn sys_utime(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        follow: true,
    });
    syscall_path_handler(request, "utime", argv, sandbox_path_1)
}

fn sys_utimes(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        follow: true,
    });
    syscall_path_handler(request, "utimes", argv, sandbox_path_1)
}

fn sys_futimesat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: Some(0),
        path: Some(1),
        follow: true,
    });
    syscall_path_handler(request, "futimesat", argv, sandbox_path_1)
}

fn sys_utimensat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let req = request.get_request();
    #[allow(clippy::cast_possible_truncation)]
    let follow = req.data.args[3] as i32 & nix::libc::AT_SYMLINK_NOFOLLOW == 0;
    let argv = NonEmpty::new(SyscallPathArgument {
        follow,
        dirfd: Some(0),
        path: Some(1),
    });
    syscall_path_handler(request, "utimensat", argv, sandbox_path_1)
}

fn sys_truncate(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        follow: true,
    });
    syscall_path_handler(request, "truncate", argv, sandbox_path_1)
}

fn sys_getxattr(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        follow: true,
    });
    syscall_path_handler(request, "getxattr", argv, sandbox_path_1)
}

fn sys_setxattr(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        follow: true,
    });
    syscall_path_handler(request, "setxattr", argv, sandbox_path_1)
}

fn sys_lsetxattr(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        follow: false,
    });
    syscall_path_handler(request, "lsetxattr", argv, sandbox_path_1)
}

fn sys_fsetxattr(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: Some(0),
        path: None,
        follow: true,
    });
    syscall_path_handler(request, "fsetxattr", argv, sandbox_path_1)
}

fn sys_listxattr(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        follow: true,
    });
    syscall_path_handler(request, "listxattr", argv, sandbox_path_1)
}

fn sys_flistxattr(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: Some(0),
        path: None,
        follow: true,
    });
    syscall_path_handler(request, "flistxattr", argv, sandbox_path_1)
}

fn sys_llistxattr(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        follow: false,
    });
    syscall_path_handler(request, "llistxattr", argv, sandbox_path_1)
}

fn sys_removexattr(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        follow: true,
    });
    syscall_path_handler(request, "removexattr", argv, sandbox_path_1)
}

fn sys_fremovexattr(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: Some(0),
        path: None,
        follow: true,
    });
    syscall_path_handler(request, "fremovexattr", argv, sandbox_path_1)
}

fn sys_lremovexattr(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let argv = NonEmpty::new(SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        follow: false,
    });
    syscall_path_handler(request, "lremovexattr", argv, sandbox_path_1)
}

/// Handles syscalls related to paths, reducing code redundancy and ensuring a uniform way of dealing with paths.
///
/// # Parameters
///
/// - `request`: User notification request from seccomp.
/// - `syscall_name`: The name of the syscall being handled, used for logging and error reporting.
/// - `arg_mappings`: Non-empty list of argument mappings containing dirfd and path indexes, if applicable.
/// - `handler`: Closure that processes the constructed canonical paths and performs additional syscall-specific operations.
///
/// # Returns
///
/// - `ScmpNotifResp`: Response indicating the result of the syscall handling.
fn syscall_path_handler<F>(
    request: &UNotifyEventRequest,
    syscall_name: &str,
    path_argv: NonEmpty<SyscallPathArgument>,
    handler: F,
) -> ScmpNotifResp
where
    F: Fn(
        NonEmpty<String>,
        Capability,
        &str,
        &UNotifyEventRequest,
        &ScmpNotifReq,
        &RemoteProcess,
    ) -> Result<Option<ScmpNotifResp>, Errno>,
{
    syscall_handler!(request, |req: &ScmpNotifReq, proc: &RemoteProcess| {
        let mut paths: Vec<String> = Vec::new();

        // Determine system call capabilities.
        let caps = Capability::try_from((proc, req, syscall_name))?;

        // If sandboxing for all the selected capabilities is off, return immediately.
        // Exception: CAP_EXEC is available and we have patterns in exec/kill list.
        let mut check = false;
        let sandbox = request.get_sandbox(false);
        if caps.contains(Capability::CAP_EXEC) && sandbox.has_exec_kill() {
            check = true;
        } else {
            for cap in [
                Capability::CAP_READ,
                Capability::CAP_STAT,
                Capability::CAP_WRITE,
                Capability::CAP_EXEC,
                Capability::CAP_CONNECT,
                Capability::CAP_BIND,
            ] {
                if sandbox.enabled(cap) {
                    check = true;
                    break;
                }
            }
        }
        drop(sandbox); // release the read lock.
        if !check {
            // SAFETY: This is unsafe due to vulnerability to TOCTOU.
            return unsafe { Ok(request.continue_syscall()) };
        }

        for arg in path_argv.iter() {
            paths.push(proc.read_path(req, arg)?);
        }

        if !request.is_valid() {
            return Ok(request.fail_syscall(nix::libc::EACCES));
        }

        // At this point, we're absolutely sure the paths vector has at least a single element.
        // Hence, using `unwrap` is fine here.
        #[allow(clippy::disallowed_methods)]
        let non_empty_paths = NonEmpty::from_vec(paths).unwrap();
        if let Some(response) = handler(non_empty_paths, caps, syscall_name, request, req, proc)? {
            Ok(response)
        } else {
            // SAFETY: This is unsafe due to vulnerability to TOCTOU.
            unsafe { Ok(request.continue_syscall()) }
        }
    })
}

fn syscall_stat_handler(
    request: &UNotifyEventRequest,
    syscall_name: &str,
    path_argv: NonEmpty<SyscallPathArgument>,
) -> ScmpNotifResp {
    syscall_handler!(request, |req: &ScmpNotifReq, proc: &RemoteProcess| {
        let mut paths: Vec<String> = Vec::new();

        // If sandboxing for CAP_STAT is off, and magic lock is set, return immediately.
        let sandbox = request.get_sandbox(false);
        #[allow(clippy::cast_possible_wrap)]
        let is_lock = sandbox.locked_for_pid(req.pid as nix::libc::pid_t);
        let is_stat = sandbox.enabled(Capability::CAP_STAT);
        if is_lock && !is_stat {
            // SAFETY: This is unsafe due to vulnerability to TOCTOU.
            return unsafe { Ok(request.continue_syscall()) };
        }
        drop(sandbox); // release the read lock.

        for arg in path_argv.iter() {
            paths.push(proc.read_path(req, arg)?);
        }

        if !request.is_valid() {
            return Ok(request.fail_syscall(nix::libc::EACCES));
        }
        #[allow(clippy::disallowed_methods)]
        let non_empty_paths = NonEmpty::from_vec(paths).unwrap();
        debug!("ctx": "syscall", "sys": syscall_name, "caps": Capability::CAP_STAT.bits(), "path": non_empty_paths.first());

        // Handle magic prefix (ie /dev/syd)
        if let Some(path) = non_empty_paths.first().strip_prefix(MAGIC_PREFIX) {
            let sandbox = request.get_sandbox(false);
            #[allow(clippy::cast_possible_wrap)]
            if sandbox.locked_for_pid(req.pid as nix::libc::pid_t) {
                // Magic commands locked, continue system call normally.
                // SAFETY: This is unsafe due to vulnerability to TOCTOU.
                return unsafe { Ok(request.continue_syscall()) };
            }
            drop(sandbox);

            // Execute magic command.
            let mut sandbox = request.get_sandbox(true);
            if path.is_empty() {
                sandbox.config(path)?;
            } else if let Some(path) = path.strip_prefix('/') {
                sandbox.config(path)?;
            } else {
                // Invalid magic command.
                // SAFETY: This is unsafe due to vulnerability to TOCTOU.
                return unsafe { Ok(request.continue_syscall()) };
            }
            drop(sandbox);

            // Handle special sydbox case
            match syscall_name {
                "stat" | "fstat" | "lstat" | "newfstatat" => {
                    let mut stat = MaybeUninit::<nix::libc::stat>::uninit();
                    // SAFETY: In libc we trust.
                    if unsafe {
                        let file = CString::from_vec_unchecked(MAGIC_FILE.into());
                        nix::libc::stat(file.as_ptr(), stat.as_mut_ptr())
                    } != 0
                    {
                        return Err(Errno::last());
                    }
                    // SAFETY: stat returned success, stat struct is properly populated.
                    unsafe { stat.assume_init() };
                    // SAFETY: The following block creates an immutable byte slice representing the memory of `stat`.
                    // We ensure that the slice covers the entire memory of `stat` using `std::mem::size_of_val`.
                    // Since `stat` is a stack variable and we're only borrowing its memory for the duration of the slice,
                    // there's no risk of `stat` being deallocated while the slice exists.
                    // Additionally, we ensure that the slice is not used outside of its valid lifetime.
                    let stat = unsafe {
                        std::slice::from_raw_parts(
                            std::ptr::addr_of!(stat) as *const u8,
                            std::mem::size_of_val(&stat),
                        )
                    };
                    #[allow(clippy::cast_possible_truncation)]
                    proc.write_mem(
                        stat,
                        req.data.args[if syscall_name == "newfstatat" { 2 } else { 1 }] as usize,
                    )?;
                }
                "statx" => {
                    let mut statx = MaybeUninit::<crate::compat::statx>::uninit();

                    // SAFETY: The libc function statx is invoked directly.
                    // - `path.as_ptr()` provides a pointer to the
                    // null-terminated path string. The lifetime of `path` extends
                    // beyond the syscall, so it's guaranteed not to be dangled
                    // here.
                    // - `statx.as_mut_ptr()` is a pointer to the `statx`
                    // struct. Similar to `path`, the lifetime of `statx` ensures the
                    // pointer won't be dangling during the syscall.
                    // The syscall can still fail (e.g., if the path does not
                    // exist), but these failures will be captured by checking the
                    // syscall's return value.
                    match unsafe {
                        let file = CString::from_vec_unchecked(MAGIC_FILE.into());
                        #[allow(clippy::arithmetic_side_effects)]
                        #[allow(clippy::cast_possible_truncation)]
                        nix::libc::syscall(
                            nix::libc::SYS_statx,
                            nix::libc::AT_FDCWD,
                            file.as_ptr() as *const _,
                            req.data.args[2] as nix::libc::c_int,
                            req.data.args[3] as nix::libc::c_uint,
                            statx.as_mut_ptr(),
                        )
                    } {
                        n if n < 0 => {
                            #[allow(clippy::arithmetic_side_effects)]
                            #[allow(clippy::cast_possible_truncation)]
                            return Err(Errno::from_i32(-n as i32));
                        }
                        _ => {}
                    };

                    // SAFETY: The following block creates an immutable byte slice representing the memory of `statx`.
                    // We ensure that the slice covers the entire memory of `statx` using `std::mem::size_of_val`.
                    // Since `statx` is a stack variable and we're only borrowing its memory for the duration of the slice,
                    // there's no risk of `statx` being deallocated while the slice exists.
                    // Additionally, we ensure that the slice is not used outside of its valid lifetime.
                    let statx = unsafe {
                        std::slice::from_raw_parts(
                            statx.as_ptr() as *const u8,
                            std::mem::size_of_val(&statx),
                        )
                    };
                    #[allow(clippy::cast_possible_truncation)]
                    proc.write_mem(statx, req.data.args[4] as usize)?;
                }
                _ => unreachable!(
                    "syscall_stat_handler called with invalid system call name `{syscall_name}'."
                ),
            }
            // stat system call successfully emulated.
            return Ok(request.return_syscall(0));
        } else {
            let sandbox = request.get_sandbox(false);
            if sandbox.enabled(Capability::CAP_STAT) {
                if let Some(response) = sandbox_path_1(
                    non_empty_paths,
                    Capability::CAP_STAT,
                    syscall_name,
                    request,
                    req,
                    proc,
                )? {
                    return Ok(response);
                }
            }
            drop(sandbox);
        }

        // Continue system call normally.
        // SAFETY: This is unsafe due to vulnerability to TOCTOU.
        unsafe { Ok(request.continue_syscall()) }
    })
}

/// A helper function to handle network-related syscalls.
///
/// This function abstracts the common logic involved in handling network syscalls such as `bind`,
/// `connect`, `recvfrom`, and `sendto` in a seccomp-based sandboxing environment. It reduces code
/// duplication across different syscall handler functions.
///
/// # Parameters
///
/// - `request`: Reference to `UNotifyEventRequest`, the user notification request from seccomp.
/// - `arg_addr`: Index of the argument representing the address in the syscall arguments.
/// - `arg_len`: Index of the argument representing the length of the address in the syscall arguments.
/// - `syscall_name`: A string slice holding the name of the syscall being handled, used for error reporting.
/// - `handler`: A closure that handles the processing of the `SockaddrStorage` object and is responsible
///   for logging or any other required operations, specific to the syscall being handled.
///
/// # Safety
///
/// This function contains unsafe blocks due to direct memory access and potential TOCTOU (Time-of-Check
/// Time-of-Use) vulnerabilities, which are acceptable in this context as per requirements. The invoked
/// unsafe blocks are commented with detailed explanations of safety considerations.
///
/// # Errors
///
/// This function returns an error if there is an issue reading memory, converting the address types,
/// or any error returned by the passed `handler` closure. All errors are wrapped in `ScmpNotifResp`
/// and are returned as syscall failures.
///
/// # Returns
///
/// Returns `ScmpNotifResp` indicating the result of the syscall handling:
/// - If successful, it contains a continued syscall.
/// - If an error occurs, it contains a failed syscall with an `EACCES` error code.
fn syscall_network_handler<F>(
    request: &UNotifyEventRequest,
    arg_addr: usize,
    arg_len: usize,
    len_ptr: bool,
    syscall_name: &str,
    handler: F,
) -> ScmpNotifResp
where
    F: Fn(
        &RemoteProcess,
        &UNotifyEventRequest,
        &ScmpNotifReq,
        &SockaddrStorage,
        &str,
        Capability,
    ) -> Result<(), Errno>,
{
    syscall_handler!(request, |req: &ScmpNotifReq, proc: &RemoteProcess| {
        let cap = if syscall_name == "bind" {
            Capability::CAP_BIND
        } else {
            Capability::CAP_CONNECT
        };
        // Return immediately if sandboxing is not enabled for current capability,
        let sandbox = request.get_sandbox(false);
        let check = sandbox.enabled(cap);
        let bind0 = sandbox.allow_successful_bind();
        drop(sandbox);
        if !check && (cap != Capability::CAP_BIND || !bind0) {
            // SAFETY: This is unsafe due to vulnerability to TOCTOU.
            return unsafe { Ok(request.continue_syscall()) };
        }

        let addr_remote = req.data.args[arg_addr];
        let addr_len: nix::libc::socklen_t = if !len_ptr {
            req.data.args[arg_len]
                .try_into()
                .map_err(|_| Errno::EINVAL)?
        } else {
            let mut l: nix::libc::socklen_t = 0;
            // SAFETY:
            // We are using the `any_as_u8_mut_slice` function to obtain a mutable byte slice from
            // `l`. We must ensure that the lifetime of this slice does not exceed `l`, and that
            // the memory representation of `l` is correctly interpreted when accessed as bytes.
            // Also, as we're getting a mutable slice, we need to guarantee that no other parts of
            // the program will simultaneously read or write to `l` or the memory area we're
            // interfacing with to prevent data races and undefined behavior.
            let l_slice = unsafe { any_as_u8_mut_slice(&mut l) };
            // Careful here:
            // recvfrom with NULL address length is required for connection-mode sockets.
            #[allow(clippy::cast_possible_truncation)]
            if let Err(errno) = proc.read_mem(l_slice, req.data.args[arg_len] as usize) {
                if errno == Errno::EFAULT {
                    // SAFETY: This is unsafe because it is vulnerable to TOCTOU.
                    return unsafe { Ok(request.continue_syscall()) };
                }
                return Err(errno);
            }
            if !request.is_valid() {
                return Ok(request.fail_syscall(nix::libc::EACCES));
            }
            l
        };

        if addr_len == 0 {
            if syscall_name == "recvfrom" || syscall_name == "sendto" {
                // Careful here:
                // recvfrom/sendto with NULL address is required for connection-mode sockets.
                // SAFETY: This is unsafe because it is vulnerable to TOCTOU.
                return unsafe { Ok(request.continue_syscall()) };
            } else {
                // Call to network system call with invalid address size, fail with EINVAL.
                return Ok(request.fail_syscall(nix::libc::EINVAL));
            }
        }

        let mut addr = vec![0u8; addr_len as usize];
        #[allow(clippy::cast_possible_truncation)]
        if let Err(errno) = proc.read_mem(&mut addr, addr_remote as usize) {
            // Careful here:
            // recvfrom/sendto with NULL address is required for connection-mode sockets.
            if errno == Errno::EFAULT && (syscall_name == "recvfrom" || syscall_name == "sendto") {
                // SAFETY: This is unsafe because it is vulnerable to TOCTOU.
                return unsafe { Ok(request.continue_syscall()) };
            } else {
                // Call to network system call with invalid address, fail with EFAULT.
                return Ok(request.fail_syscall(nix::libc::EFAULT));
            }
        }
        if !request.is_valid() {
            return Ok(request.fail_syscall(nix::libc::EACCES));
        }

        // SAFETY: Invoking `SockaddrStorage::from_raw` is safe because:
        // 1. The memory location of `sockaddr_ptr` is valid, correctly aligned,
        //    and readable for `addr_len` bytes, preventing segmentation faults.
        // 2. The memory is allocated based on a valid `sockaddr` structure.
        // 3. The `addr_len` is correctly representing the size of the actual `sockaddr` structure.
        // 4. There are no concurrent writes to the memory location while reading.
        let addr = match unsafe {
            #[allow(clippy::cast_ptr_alignment)]
            #[allow(clippy::cast_possible_truncation)]
            SockaddrStorage::from_raw(
                addr.as_ptr() as *const nix::libc::sockaddr,
                Some(addr_len as u32),
            )
        } {
            Some(a) => a,
            None => {
                // Invalid socket address of length `addr_len`.
                return Err(Errno::EINVAL);
            }
        };

        handler(proc, request, req, &addr, syscall_name, cap)?;

        if len_ptr {
            let addrlen = match addr.family() {
                Some(AddressFamily::Inet) => std::mem::size_of::<nix::sys::socket::sockaddr_in>(),
                Some(AddressFamily::Inet6) => std::mem::size_of::<nix::sys::socket::sockaddr_in6>(),
                Some(AddressFamily::Unix) => std::mem::size_of::<nix::sys::socket::sockaddr_un>(),
                _ => 0,
            };
            if addrlen != 0 {
                debug!("ctx": "socket_address_length_rewrite", "from": addr_len.to_string(), "to": addrlen.to_string());

                // recvfrom(..., socklen_t *addrlen)
                // We need to write the correct length of address or child
                // may access invalid memory (e.g. on free()'ing addr).
                // SAFETY: TODO
                let addrlen = unsafe { any_as_u8_slice(&addrlen) };
                #[allow(clippy::cast_possible_truncation)]
                proc.write_mem(addrlen, req.data.args[arg_len] as usize)?;
                if !request.is_valid() {
                    return Ok(request.fail_syscall(nix::libc::EACCES));
                }
            }
        }

        // SAFETY: This is unsafe because it is vulnerable to TOCTOU.
        unsafe { Ok(request.continue_syscall()) }
    })
}

/*
#[cfg(test)]
mod tests {
    use std::{ffi::CStr, time::Duration};

    use nix::sys::signal::{kill, Signal::SIGKILL};

    use super::*;

    #[test]
    fn smoke_test_sleep() {
        fn openat_handler(req: &UNotifyEventRequest) -> libseccomp::ScmpNotifResp {
            let path = req.get_request().data.args[1];
            let remote = RemoteProcess::new(Pid::from_raw(req.request.pid as i32)).unwrap();
            let mut buf = [0u8; 256];
            remote.read_mem(&mut buf, path as usize).unwrap();
            eprintln!("open (read from remote): {:?}", buf);
            let path = CStr::from_bytes_until_nul(&buf).unwrap();
            if !req.is_valid() {
                return req.fail_syscall(libc::EACCES);
            }
            eprintln!("open (path CStr): {:?}", path);
            unsafe { req.continue_syscall() }
        }

        let mut supervisor = Supervisor::new(2).unwrap();
        supervisor.insert_handler(ScmpSyscall::new("openat"), openat_handler);
        let mut cmd = Command::new("/bin/sleep");
        let cmd = cmd.arg("1");
        let (pid, thread_handle, pool) = supervisor.spawn(cmd).unwrap();
        let status = Supervisor::wait(pid, thread_handle, pool).unwrap();
        assert!(status.success());
    }

    #[test]
    fn smoke_test_whoami() {
        fn geteuid_handler(req: &UNotifyEventRequest) -> libseccomp::ScmpNotifResp {
            req.return_syscall(0)
        }

        let mut supervisor = Supervisor::new(2).unwrap();
        supervisor.insert_handler(ScmpSyscall::new("geteuid"), geteuid_handler);
        let mut cmd = Command::new("/usr/bin/whoami");
        let cmd = cmd.stdout(Stdio::piped());
        let (pid, thread_handle, pool) = supervisor.spawn(cmd).unwrap();
        let status = Supervisor::wait(pid, thread_handle, pool).unwrap();
        assert!(status.success());
        let whoami_stdout = child.stdout.as_mut().unwrap();
        let mut buf = String::new();
        whoami_stdout.read_to_string(&mut buf).unwrap();
        assert_eq!(buf.trim(), "root");
    }

    #[test]
    fn test_sleep_blocking_syscall() {
        fn clock_nanosleep_handler(req: &UNotifyEventRequest) -> libseccomp::ScmpNotifResp {
            // sleep for extra 60s
            // Please note that it may bring A LOT OF PROBLEMS if you try using pthread_cancel
            // So here we just use the easy way: check valid in the loop
            let (tx, rx) = std::sync::mpsc::channel();
            let handler = std::thread::spawn(move || {
                for _ in 0..60 {
                    if rx.try_recv().is_ok() {
                        break;
                    }
                    std::thread::sleep(Duration::from_secs(1));
                }
            });
            // while handler is running, check valid in the loop
            loop {
                if !req.is_valid() {
                    // cancel the thread
                    eprintln!("canceling thread as req is invalid now");
                    tx.send(()).unwrap();
                    break;
                }
                std::thread::sleep(Duration::from_millis(100));
            }
            handler.join().unwrap();
            unsafe { req.continue_syscall() }
        }

        let mut supervisor = Supervisor::new(2).unwrap();
        supervisor.insert_handler(ScmpSyscall::new("clock_nanosleep"), clock_nanosleep_handler);
        let mut cmd = Command::new("/bin/sleep");
        let cmd = cmd.arg("120");
        let (pid, thread_handle, pool) = supervisor.spawn(cmd).unwrap();
        std::thread::spawn(move || {
            std::thread::sleep(Duration::from_secs(1));
            // kill the child process
            kill(pid, SIGKILL).unwrap();
        });
        let _ = Supervisor::wait(pid, thread_handle, pool).unwrap();
    }

    #[test]
    fn test_new_fd() {
        fn openat_handler(req: &UNotifyEventRequest) -> libseccomp::ScmpNotifResp {
            let path = req.get_request().data.args[1];
            let remote = RemoteProcess::new(Pid::from_raw(req.request.pid as i32)).unwrap();
            let mut buf = [0u8; 256];
            remote.read_mem(&mut buf, path as usize).unwrap();
            eprintln!("open (read from remote): {:?}", buf);
            let path = CStr::from_bytes_until_nul(&buf).unwrap();
            if !req.is_valid() {
                return req.fail_syscall(libc::EACCES);
            }
            eprintln!("open (path CStr): {:?}", path);
            if path.to_str().unwrap() == "/etc/passwd" {
                // open /etc/resolv.conf instead
                let file = File::open("/etc/resolv.conf").unwrap();
                let fd = file.as_raw_fd();
                let remote_fd = req.add_fd(fd).unwrap();
                req.return_syscall(remote_fd as i64)
            } else {
                unsafe { req.continue_syscall() }
            }
        }

        let mut supervisor = Supervisor::new(2).unwrap();
        supervisor.insert_handler(ScmpSyscall::new("openat"), openat_handler);
        let mut cmd = Command::new("/bin/cat");
        let cmd = cmd.arg("/etc/passwd").stdout(Stdio::piped());
        let (pid, thread_handle, pool) = supervisor.spawn(cmd).unwrap();
        let status = Supervisor::wait(pid, thread_handle, pool).unwrap();
        assert!(status.success());
        let cat_stdout = child.stdout.as_mut().unwrap();
        let mut buf = String::new();
        cat_stdout.read_to_string(&mut buf).unwrap();
        assert!(buf.contains("nameserver"));
    }
}
*/
