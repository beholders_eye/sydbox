//
// syd: application sandbox
// src/config.rs: Static configuration, edit & recompile!
//
// Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

/// The environment variable to read the log level from.
pub const LOG_ENVIRON: &str = "SYD_LOG";

/// The environment variable to set to disable logging to syslog.
pub const NO_SYSLOG_ENVIRON: &str = "SYD_NO_SYSLOG";

/// The environment variable to set to disable cross memory attach, and fallback to /proc/pid/mem.
pub const NO_CROSS_MEMORY_ATTACH_ENVIRON: &str = "SYD_NO_CROSS_MEMORY_ATTACH";

/// Path prefix for magic stat commands.
pub const MAGIC_PREFIX: &str = "/dev/syd";

/// Path to the file which will be used as file status for magic stat commands.
pub const MAGIC_FILE: &str = "/dev/null";

/// Api version of the configuration.
pub const API_VERSION: &str = env!("CARGO_PKG_VERSION_MAJOR");

/// Cache size of the bind0 `HashSet`.
pub const BIND0_CACHE_SIZE: usize = 64;

/// Default buffer size used in `compat::get_directory_entries` function.
pub const GETDENTS_BUFFER_SIZE: usize = 8192;

// Sandboxing profiles

/// No ipv4 sandboxing profile.
pub const PROFILE_NOIPV4: [&str; PROFILE_NOIPV4_SIZE] =
    ["sandbox/net:on", "allowlist/net/connect+::/0@0-65535"];
/// Size of the no ipv4 sandboxing profile.
pub const PROFILE_NOIPV4_SIZE: usize = 2;

/// No ipv6 sandboxing profile.
pub const PROFILE_NOIPV6: [&str; PROFILE_NOIPV6_SIZE] =
    ["sandbox/net:on", "allowlist/net/connect+0.0.0.0/0@0-65535"];
/// Size of the no ipv6 sandboxing profile.
pub const PROFILE_NOIPV6_SIZE: usize = 2;

/// Paludis sandboxing profile, activated with:
/// `syd --profile paludis`.
pub const PROFILE_PALUDIS: [&str; PROFILE_PALUDIS_SIZE] = [
    "sandbox/read:off",
    "sandbox/write:on",
    "sandbox/exec:off",
    "sandbox/net:on",
    "trace/allow_successful_bind:true",
    "trace/allow_unsupported_socket_families:true",
    "allowlist/write+/dev/stdout",
    "allowlist/write+/dev/stderr",
    "allowlist/write+/dev/zero",
    "allowlist/write+/dev/null",
    "allowlist/write+/dev/full",
    "allowlist/write+/dev/console",
    "allowlist/write+/dev/random",
    "allowlist/write+/dev/urandom",
    "allowlist/write+/dev/ptmx",
    "allowlist/write+/dev/fd/**",
    "allowlist/write+/dev/tty*",
    "allowlist/write+/dev/pty*",
    "allowlist/write+/dev/tts",
    "allowlist/write+/dev/pts/**",
    "allowlist/write+/dev/shm/**",
    "allowlist/write+/selinux/context/**",
    "allowlist/write+/proc/[0-9]*/attr/***",
    "allowlist/write+/proc/[0-9]*/fd/***",
    "allowlist/write+/proc/[0-9]*/task/***",
    "allowlist/write+/tmp/**",
    "allowlist/write+/var/tmp/**",
    "allowlist/write+/var/cache/**",
    "allowlist/net/bind+LOOPBACK@0",
    "allowlist/net/bind+LOOPBACK@1024-65535",
    "allowlist/net/bind+LOOPBACK6@0",
    "allowlist/net/bind+LOOPBACK6@1024-65535",
    "allowlist/net/connect+/var/run/nscd/socket",
    "allowlist/net/connect+/run/nscd/socket",
    "allowlist/net/connect+/var/lib/sss/pipes/nss",
    // allow getaddrinfo() with AI_ADDRCONFIG on musl systems.
    "allowlist/net/connect+LOOPBACK@65535",
    "allowlist/net/connect+LOOPBACK6@65535",
];
/// Size of the paludis sandboxing profile.
pub const PROFILE_PALUDIS_SIZE: usize = 37;

/// User sandboxing profile, activated with:
/// `syd --profile user`.
pub const PROFILE_USER: [&str; PROFILE_USER_SIZE] = [
    "sandbox/read:on",
    "sandbox/stat:on",
    "sandbox/write:on",
    "sandbox/exec:on",
    "sandbox/net:on",
    "trace/allow_successful_bind:true",
    "trace/allow_unsupported_socket_families:true",
    "allowlist/read+/",
    "allowlist/read+/home",
    "allowlist/read+/etc/***",
    "allowlist/read+/bin/***",
    "allowlist/read+/dev/***",
    "allowlist/read+/lib*/***",
    "allowlist/read+/media/***",
    "allowlist/read+/mnt/***",
    "allowlist/read+/opt/***",
    "allowlist/read+/proc/***",
    "allowlist/read+/run/***",
    "allowlist/read+/sbin/***",
    "allowlist/read+/snap/***",
    "allowlist/read+/sys/***",
    "allowlist/read+/tmp/***",
    "allowlist/read+/usr/***",
    "allowlist/read+/var/***",
    "allowlist/stat+/",
    "allowlist/stat+/home",
    "allowlist/stat+/etc/***",
    "allowlist/stat+/bin/***",
    "allowlist/stat+/dev/***",
    "allowlist/stat+/lib*/***",
    "allowlist/stat+/media/***",
    "allowlist/stat+/mnt/***",
    "allowlist/stat+/opt/***",
    "allowlist/stat+/proc/***",
    "allowlist/stat+/run/***",
    "allowlist/stat+/sbin/***",
    "allowlist/stat+/snap/***",
    "allowlist/stat+/sys/***",
    "allowlist/stat+/tmp/***",
    "allowlist/stat+/usr/***",
    "allowlist/stat+/var/***",
    "allowlist/exec+/bin/**",
    "allowlist/exec+/lib*/**",
    "allowlist/exec+/sbin/**",
    "allowlist/exec+/snap/**",
    "allowlist/exec+/usr/**/bin/**",
    "allowlist/exec+/usr/**/lib*/**",
    "allowlist/exec+/usr/**/sbin/**",
    "allowlist/write+/dev/stdout",
    "allowlist/write+/dev/stderr",
    "allowlist/write+/dev/zero",
    "allowlist/write+/dev/null",
    "allowlist/write+/dev/full",
    "allowlist/write+/dev/console",
    "allowlist/write+/dev/random",
    "allowlist/write+/dev/urandom",
    "allowlist/write+/dev/ptmx",
    "allowlist/write+/dev/fd/**",
    "allowlist/write+/dev/tty*",
    "allowlist/write+/dev/pty*",
    "allowlist/write+/dev/tts",
    "allowlist/write+/dev/pts/**",
    "allowlist/write+/dev/shm/**",
    "allowlist/write+/selinux/context/**",
    "allowlist/write+/proc/[0-9]*/attr/***",
    "allowlist/write+/proc/[0-9]*/fd/***",
    "allowlist/write+/proc/[0-9]*/task/***",
    "allowlist/write+/run/**",
    "allowlist/write+/tmp/**",
    "allowlist/write+/var/tmp/**",
    "allowlist/write+/var/cache/**",
    "allowlist/net/bind+LOOPBACK@0",
    "allowlist/net/bind+LOOPBACK@1024-65535",
    "allowlist/net/bind+LOOPBACK6@0",
    "allowlist/net/bind+LOOPBACK6@1024-65535",
    "allowlist/net/connect+/var/run/nscd/socket",
    "allowlist/net/connect+/run/nscd/socket",
    "allowlist/net/connect+/var/lib/sss/pipes/nss",
    // allow getaddrinfo() with AI_ADDRCONFIG on musl systems.
    "allowlist/net/connect+LOOPBACK@65535",
    "allowlist/net/connect+LOOPBACK6@65535",
    // Allow UNIX socket access to run & /tmp (think X, tmux, screen et a.)
    "allowlist/net/bind+/run/**",
    "allowlist/net/connect+/run/**",
    "allowlist/net/bind+/tmp/**",
    "allowlist/net/connect+/tmp/**",
    // Allow access to syslog
    "allowlist/net/connect+/dev/log",
];
/// Size of the paludis sandboxing profile.
pub const PROFILE_USER_SIZE: usize = 85;

/// The list of system calls which are of the id family.
pub const ID_SYSCALLS: [&str; ID_SYSCALLS_SIZE] = ["getuid", "getgid", "geteuid", "getegid"];
/// The size of id syscalls array
pub const ID_SYSCALLS_SIZE: usize = 4;

/// The size of safe syscalls array
pub const SAFE_SYSCALLS_SIZE: usize = 247;
/// The list of system calls which are allowlisted without any filtering.
pub const SAFE_SYSCALLS: [&str; SAFE_SYSCALLS_SIZE] = [
    "_sysctl",
    "accept",
    "accept4",
    //add_key
    //adjtimex
    //afs_syscall
    "alarm",
    "arch_prctl",
    "bind",
    "bpf",
    "brk",
    "capget",
    "capset",
    "chdir",
    //"chroot",
    //clock_adjtime
    "clock_getres",
    "clock_gettime",
    "clock_nanosleep",
    "clock_settime",
    "clone",
    "clone3",
    "close",
    "close_range",
    "connect",
    "copy_file_range",
    //create_module
    //delete_module
    "dup",
    "dup2",
    "dup3",
    "epoll_create",
    "epoll_create1",
    "epoll_ctl",
    "epoll_ctl_old",
    "epoll_pwait",
    "epoll_pwait2",
    "epoll_wait",
    "epoll_wait_old",
    "eventfd",
    "eventfd2",
    "exit",
    "exit_group",
    "fadvise64",
    "fallocate",
    "fanotify_init",
    "fanotify_mark",
    "fchdir",
    "fchown",
    "fchmod",
    "fcntl",
    "fdatasync",
    //finit_module
    "flock",
    "fork",
    //fsconfig
    //fsmount
    //fsopen
    //fspick
    "fstatfs",
    "fsync",
    "ftruncate",
    "futex",
    //get_kernel_syms
    "get_mempolicy",
    "get_robust_list",
    "get_thread_area",
    "getcpu",
    "getcwd",
    "getdents",
    "getdents64",
    "getegid",
    "geteuid",
    "getgid",
    "getgroups",
    "getitimer",
    "getpeername",
    "getpgid",
    "getpgrp",
    "getpid",
    "getpmsg",
    "getppid",
    "getpriority",
    "getrandom",
    "getresgid",
    "getresuid",
    "getrlimit",
    "getrusage",
    "getsid",
    "getuid",
    "getsockopt",
    "gettid",
    "gettimeofday",
    //init_module
    "inotify_add_watch",
    "inotify_init",
    "inotify_init1",
    "inotify_rm_watch",
    /*FIXME: urine
    "io_cancel",
    "io_destroy",
    "io_getevents",
    "io_pgetevents",
    "io_setup",
    "io_submit",
    "io_uring_enter",
    "io_uring_register",
    "io_uring_setup",
    */
    "ioctl",
    "ioperm",
    "iopl",
    "ioprio_get",
    "ioprio_set",
    "kcmp",
    //kexec_file_load,
    //kexec_load,
    //keyctl,
    "kill",
    "landlock_add_rule",
    "landlock_create_ruleset",
    "landlock_restrict_self",
    "listen",
    "lookup_dcookie",
    "lseek",
    "madvise",
    "mbind",
    "membarrier",
    "memfd_create",
    //memfd_secret
    "migrate_pages",
    "mincore",
    "mlock",
    "mlock2",
    "mlockall",
    "mmap",
    "modify_ldt",
    //mount_setattr
    "mprotect",
    "mq_getsetattr",
    "mq_notify",
    "mq_open",
    "mq_timedreceive",
    "mq_timedsend",
    "mq_unlink",
    "mremap",
    "msgctl",
    "msgget",
    "msgrcv",
    "msgsnd",
    "msync",
    "munlock",
    "munlockall",
    "munmap",
    //TODO:name_to_handle_at
    "nanosleep",
    //nfsservctl,
    "open_by_handle_at",
    //open_tree
    "pause",
    "perf_event_open",
    "personality",
    "pidfd_getfd",
    //TODO:pidfd_open,
    "pidfd_send_signal",
    "pipe",
    "pipe2",
    //pivot_root,
    "pkey_alloc",
    "pkey_free",
    "pkey_mprotect",
    "poll",
    "ppoll",
    "prctl",
    "pread64",
    "preadv",
    "preadv2",
    "prlimit64",
    "process_madvise",
    "process_mrelease",
    //process_vm_readv
    //process_vm_writev
    "pselect6",
    //ptrace
    //"putpmsg",
    "pwrite64",
    "pwritev",
    "pwritev2",
    //query_module,
    //quotactl
    //quotactl_fd
    "read",
    "readahead",
    "readlink",
    "readlinkat",
    "readv",
    //reboot
    "recvfrom",
    "recvmmsg",
    "recvmsg",
    "remap_file_pages",
    //request_key
    "restart_syscall",
    "rseq",
    "rt_sigaction",
    "rt_sigpending",
    "rt_sigprocmask",
    "rt_sigqueueinfo",
    "rt_sigreturn",
    "rt_sigsuspend",
    "rt_sigtimedwait",
    "rt_tgsigqueueinfo",
    "sched_get_priority_max",
    "sched_get_priority_min",
    "sched_getaffinity",
    "sched_getattr",
    "sched_getparam",
    "sched_getscheduler",
    "sched_setaffinity",
    "sched_setattr",
    "sched_setscheduler",
    "sched_yield",
    "seccomp",
    //security
    "select",
    "semctl",
    "semget",
    "semop",
    "semtimedop",
    "sendfile",
    "sendmmsg",
    "sendmsg",
    "sendto",
    "set_mempolicy",
    "set_robust_list",
    "set_thread_area",
    "set_tid_address",
    //setdomainname
    "setfsgid",
    "setfsuid",
    "setgid",
    "setgroups",
    //sethostname
    "setitimer",
    "setns",
    "setpgid",
    "setpriority",
    "setregid",
    "setresgid",
    "setresuid",
    "setreuid",
    "setrlimit",
    "setsid",
    "setsockopt",
    //"settimeofday"
    "setuid",
    "shmat",
    "shmctl",
    "shmdt",
    "shmget",
    "shutdown",
    "sigaltstack",
    "signalfd",
    "socket",
    "socketpair",
    "splice",
    "statfs",
    //swapoff
    //swapon
    "sync",
    "sync_file_range",
    "syncfs",
    "sysinfo",
    "tee",
    "tgkill",
    "time",
    "timer_create",
    "timer_delete",
    "timer_getoverrun",
    "timer_gettime",
    "timer_settime",
    "timerfd_create",
    "timerfd_gettime",
    "timerfd_settime",
    "times",
    "tkill",
    //tuxcall
    "umask",
    "uname",
    //"unshare",
    //"uselib",
    "userfaultfd",
    "ustat",
    "vfork",
    "vhangup",
    "vmsplice",
    //vserver
    "wait4",
    "waitid",
    "write",
    "writev",
];
