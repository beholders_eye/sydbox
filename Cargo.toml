[package]
name = "syd"
version = "3.0.0"
edition = "2021"
authors = ["Ali Polatel <alip@chesswob.org>"]
description = "seccomp-bpf and seccomp-notify based application sandbox"
readme = "README.md"
license = "GPL-3.0-or-later"
categories = ["command-line-utilities", "development-tools::debugging", "os::linux-apis"]
keywords = ["security", "sandbox"]
homepage = "https://todo.sr.ht/~alip/syd"
repository = "https://git.sr.ht/~alip/syd"
rust-version = "1.70" # remember to update README.md

[dependencies]
anyhow = { version = "1.0", default-features = false, features = ["std"] }
argv = { version = "0.1", default-features = false }
bitflags = { version = "1.3", default-features = false }
env_logger = { version = "0.10", default-features = false }
getargs = { version = "0.5", default-features = false, features = ["std"] }
glob-match = { version = "0.2", default-features = false }
ipnetwork = { version = "0.20", default-features = false }
libseccomp = { version = "0.3", default-features = false, features = ["const-syscall"] }
libseccomp-sys = "0.2"
log = { version = "0.4", default-features = false }
nix = { version = "=0.26.4", default-features = false, features = ["fs", "net", "poll", "signal", "uio", "user"] }
nonempty = { version = "0.8", default-features = false }
num_cpus = { version = "1.16", default-features = false }
once_cell = { version = "1.18", default-features = false, features = ["std"] }
parking_lot = { version = "0.12", default-features = false }
regex-lite = { version = "0.1", default-features = false, features = ["std", "string"] }
serde_json = { version = "1.0", default-features = false, features = ["std"] }
threadpool = { version = "1.8", default-features = false }
ttl_cache = { version = "0.5", default-features = false }

[[bin]]
name = "syd"
path = "src/main.rs"

[[bin]]
name = "syd-test"
path = "src/t/main.rs"

[[bin]]
name = "syd-test-do"
path = "src/t/do.rs"

[[test]]
name = "tests"

[profile.release]
lto = true
codegen-units = 1

# This can hurt profiling but reduces size.
debug = false

# The default optimization level is 3 for release mode builds.
# 0 means disable optimization and is the default for debug mode buids.
# (Setting opt-level=1 for debug builds is a good way of speeding them up a bit.)
# "s" means optimize for size, "z" reduces size even more.
opt-level = "z"

# Setting panic strategy to 'abort':
# - May reduce binary size since unwinding code is excluded.
# - Potentially improves performance due to the absence of unwinding.
# - Ensures predictable behavior: program aborts on panic.
# - Increases compatibility with platforms/configurations that might not support unwinding.
# Trade-offs:
# - Destructors won't run, which might cause resource leaks (e.g., unclosed files or network connections).
# - May lose detailed information about the panic, making debugging hard
panic = "abort"
